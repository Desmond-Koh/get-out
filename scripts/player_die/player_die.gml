if(global.isHolyNecklacePickedUp = false){
	obj_player.sprite_index = spr_player_dead;	
	instance_create_layer(obj_player.x, obj_player.y, "Inst_PlayerDie", obj_player_die);
	global.PlayerDie = true;
}
else{
	if(audio_is_playing(snd_lootItem) == false)
	{ 
		audio_play_sound(snd_lootItem, 10, false);
	}
	instance_create_layer(obj_player.x, obj_player.y, "Inst_PlayerDie", obj_blackscreen);
	global.annaSpawned = false;
	instance_destroy(obj_anna);
	global.isHolyNecklacePickedUp = false;
	global.gameState = 8;
	instance_activate_object(obj_dialoguebox);
	global.array_inv[global.HolyNecklaceInvNum] = -1;
	obj_player.x = 2850;
	obj_player.y = 1635;
}
