/// @param {real} Select Inventory Number

hotbarNum = argument0;

if(global.PlayerDie == false)
{
	if(global.array_inv[hotbarNum] == 0 && global.isCameraPanning == false) //Book
	{
		//Turn off torchlight
		instance_destroy(global.Torchlight);
		global.torchlightValue = 0;
		global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);
		
		//Turn off EMF Reader
		global.emfReaderValue = 0;
		
		if(Book = noone && global.isCupboardOpen == false)
		{
			//Display Book
			Book = instance_create_layer(x, y, "Inst_ItemOverlay", obj_book);
		}
		else
		{
			//Close Book
			instance_destroy(Book);
		}
	}
	else if(global.array_inv[hotbarNum] == 1){ //Torchlight
		if(global.moveState == true){
			//Close book
			instance_destroy(Book);
		
			//Turn on torchlight
			instance_destroy(global.Torchlight);
			if(global.torchlightValue == 0){
				global.torchlightValue = 1;
			}
			else{
				global.torchlightValue = 0;
			}
			global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);
			audio_play_sound(snd_torchlight, 10, false);
		}
	}
	else if(global.array_inv[hotbarNum] == 2){ //EMF Reader
		//Turn off torchlight
		instance_destroy(global.Torchlight);
		global.torchlightValue = 0;
		global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);
		
		//Close book
		instance_destroy(Book);
		
		//Turn on EMF Reader
		if(global.emfReaderValue == 0){
			global.emfReaderValue = 1;
		}
		else{
			global.emfReaderValue = 0;
		}
		audio_play_sound(snd_emfToggle, 10, false);
	}
	else if(global.array_inv[hotbarNum] == 3){ //Holy Necklace
		//Do nothing
	}
	else if(global.array_inv[hotbarNum] == 4 && global.isCameraPanning == false){ //Code Cupboard
		//Turn off torchlight
		instance_destroy(global.Torchlight);
		global.torchlightValue = 0;
		global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);
		
		//Turn off EMF Reader
		global.emfReaderValue = 0;
		
		//Close book
		instance_destroy(Book);
		
		if(global.CodePaper == noone && global.isCupboardOpen == false )
		{
			//Display Code Paper
			global.CodePaper = instance_create_layer(x, y, "Inst_ItemOverlay", obj_code_paper);
		}
		else
		{
			//Close Book
			instance_destroy(global.CodePaper);
		}
	}
	else if(global.array_inv[hotbarNum] == 5){ //Battery
		//Do nothing
	}
	else if(global.array_inv[hotbarNum] == 6 && global.isCameraPanning == false){ //Code Library
		//Turn off torchlight
		instance_destroy(global.Torchlight);
		global.torchlightValue = 0;
		global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);
		
		//Turn off EMF Reader
		global.emfReaderValue = 0;
		
		//Close book
		instance_destroy(Book);
		
		if(global.CodeLibrary == noone && global.isCupboardOpen == false )
		{
			//Display Code Paper
			global.CodeLibrary = instance_create_layer(x, y, "Inst_ItemOverlay", obj_code_library);
		}
		else
		{
			//Close Book
			instance_destroy(global.CodeLibrary);
		}
	}
}