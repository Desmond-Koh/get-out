/// @function pickup(item, itemInstanceID)
/// @description Add an item to inventory
/// @param {real} item the item to be picked up.
/// @param {real} itemInstance the ID of the item instance.

item = argument0;
itemInstanceID = argument1;

if(itemInstanceID == inst_battery1 || itemInstanceID == inst_battery2 || itemInstanceID == inst_battery3){
	if(global.battery < 50){
		global.battery += 50
	}
	else{
		global.battery = 100;
	}
	global.itemNotificationNum = "foundBattery";
	
	//Destroy Item
	instance_destroy(itemInstanceID);	
}
else{
	for (var inv = 0; inv < obj_player.maxInvSlots; inv++){
		//Check Inventory for empty slot
		if(global.array_inv[inv] == -1){
			//Add Item to empty slot
			global.array_inv[inv] = item;	
		
			if(itemInstanceID == holynecklace_instance){
				global.isHolyNecklacePickedUp = true;
				global.HolyNecklaceInvNum = inv;
				global.itemNotificationNum = "foundHolynecklace";
			}
			else if(itemInstanceID == torchlight_Instance)
			{
				global.isTorchlightPickedUp = true;
				global.itemNotificationNum = "foundTorchlight";
			
			}
			else if(item == 0){
				global.isBookPickedUp = true;
				global.itemNotificationNum = "foundBook";
			}
			else if(item == 2){
				global.isEMFReaderPickedUp = true;
				global.itemNotificationNum = "foundEMFReader";
			}
			else if(itemInstanceID == codeLibrary_instance){
				global.isEMFReaderPickedUp = true;
				global.itemNotificationNum = "foundCode";
			}
			
			//Destroy Item
			instance_destroy(itemInstanceID);	
			break;
		}
	}
}

instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_itemNotification)
