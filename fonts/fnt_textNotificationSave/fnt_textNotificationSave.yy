{
    "id": "84895c09-2be8-4808-b426-49f93a3998ec",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textNotificationSave",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans Unicode",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6d30551a-f9e8-4fc6-864c-1cf4a588eff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "90260efc-9fd6-405f-8c72-7ef8c0c78676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 70,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "51d12e1e-3392-4c4e-ae63-ac1741247527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 62,
                "y": 107
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "41f3d5e9-9f44-4fc7-ba89-b65dff3a2676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 46,
                "y": 107
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "67196f39-bedb-4ac4-829a-a6d830765117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 35,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7067da04-b268-4afd-b37c-1de3fd8e70bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b5fbbce8-a0a1-442f-ac6b-89e12256e4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8b21f3c9-0217-4fd7-a200-130c56e18944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 248,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "302affae-5e4d-4e61-a274-04d464d5fe66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 240,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "27fd7d90-6c7d-4a50-b857-7d0059b99ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 232,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f1bdecb0-6038-4cd1-b7f8-feb7a9ede586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 75,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3ce07618-eff1-4666-80a5-9bafe8d031e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 217,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "728a8cfc-2694-484b-85ab-5b0094c565da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 201,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6fcc1262-bce5-4759-a4b8-7fc007bde3b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 189,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6cbeeea3-f241-4d00-94fb-eff0bb3bf46b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 184,
                "y": 72
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f5d443a0-d388-495a-b233-517a46679b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 175,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7fa94afb-c050-42ca-af17-e05441c48362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 161,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "01e477b8-abc7-4571-9098-456a2102126d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 3,
                "shift": 13,
                "w": 5,
                "x": 154,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "981fc1c1-5594-4210-84bf-3153e2a3e828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 141,
                "y": 72
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6f7463c3-fabe-4eab-819b-285e139feeab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 129,
                "y": 72
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2b36be17-af9a-425f-b83b-7a670ed3e452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 115,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "25222055-8295-4fd3-a8c7-e07b28e4d70e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 206,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3332560e-263a-443c-96c3-b70474ec7eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 86,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "815c151c-ca6a-40df-873b-9aa4981721bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 99,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0571fa7a-1493-4ad2-b01b-4685735c7e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 107
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "52173e27-0e8a-421d-aa24-5f0da28b8f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 149,
                "y": 142
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6aee9670-31a0-45e1-8ea7-5ee6e78e8251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 144,
                "y": 142
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d7b239c5-feb7-448a-9f3b-2093dbe7425d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 139,
                "y": 142
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "948fe2fd-000f-417d-bf0b-04f14cc1f80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 124,
                "y": 142
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e94fc503-bd39-40ed-973c-272a94963257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 109,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d7d1f748-7dfd-475b-b14c-f85d8bffdaa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 94,
                "y": 142
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c5046272-bc73-499c-9bee-6a766c9fae0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 142
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ad7ce9e1-0b11-4304-b066-ebff7f2fc57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 63,
                "y": 142
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ab7f1d37-0247-4048-8813-be926874f0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 46,
                "y": 142
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f823aa83-7a59-4e78-ac4d-85d5a24775b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 33,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dada737c-127e-4457-9cfe-ccab6689f5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 142
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1f39d250-730b-4856-8d62-c57b439d175d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e337f517-f7f3-437c-b0c6-ee607b05aeed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "51ef5cfe-0b34-4721-b96a-b30bbd2a62fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 217,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e20ef5f5-3de4-4d53-9c26-4e2929e03495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 202,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cad52658-4c10-414a-83a7-d36b71c26134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 187,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "06728864-6738-4e38-8a5b-5f881b1375c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 181,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cf8832d1-4378-4ea9-a020-4f38d6b5538e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": -2,
                "shift": 7,
                "w": 7,
                "x": 172,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dfbe37e1-7bb7-431d-8db4-0bf99e1cb318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 157,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e8419f17-4123-48c3-ac63-37089fa4dda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 144,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "17435678-8e03-410e-9f39-e4c4dd0a692a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 126,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "67e150e8-d7fe-4f53-876b-d94c08d24a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 100,
                "y": 72
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c5dd3b49-16d0-4ee6-928c-f5f48f983548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 83,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1c3a4449-7c4f-4f7c-a6c6-154d2501abb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 70,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "726b10b3-5da0-4973-ac25-76bc1e18bf67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "47d1a7af-fbfb-4ddb-bc42-5932165d2591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 14,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "77063691-ec71-421f-812e-b52a951d2a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bb7cd1a6-d3c0-45ef-88d1-809ee9d9aa82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ed3b7732-88d8-4a90-95fb-a1d53b8b3649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a9c0bbf-efa5-4f16-aa9b-4c251f1fb907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "32bf1958-bf58-4209-99f8-2a1a5e03451c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1bbcd796-3d77-4a47-b65a-1f00718bc0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0f825681-c249-463a-9537-c4e8e7c8f275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0f3730fd-5980-4cad-b5b9-deb1624d23bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7dbb1012-2e7c-4b13-9507-6c467f78ffe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2f2fedc7-4de5-4c61-a722-5f2a5bc9984a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3d2fae74-fd1b-4653-9f8f-f815fb980828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e2049134-d5f4-4f4c-9c3e-94d1a55eb563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7319a6fc-d060-45b5-9127-672676f0b261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "db7e333c-e5b8-4d42-993d-f43d1f22d99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9f10b0c6-3e72-4b6f-89b7-96bf746574d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4205f177-4a41-4f01-9f01-65ea07c4cd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e11de83e-7386-41a4-8d72-4f1a8215afbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "16521231-120d-40b7-8690-6e3f966d2d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6e513b7d-0455-4deb-91ca-125e661c73d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8eeef9a9-9566-4b10-ac9d-141f3f57558a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1134e89c-f4c6-41a7-ab6d-b7489d7813d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1721e920-0d87-4d74-91e7-666c3ca136da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 179,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ca3ffe06-fb2f-4d97-948a-6c1b54b68e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 68,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "39280107-83a2-4d71-8311-8386b4d90c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5fd1a493-be89-4b07-b732-3823ce1a4416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "817d707e-78b0-43d3-b47b-862f96692464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 32,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "27a4a82d-8d2a-416c-ae7a-74c872b83b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 14,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "13cd97ba-0f9d-4c6b-96ef-606a0249b3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0380fbf0-388b-45ac-aa70-78314badf5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 236,
                "y": 37
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d357ced7-f5ac-4274-a344-435583d981fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 37
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9a09dc50-0716-4faa-aa4f-4c3e78c73f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 37
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f6fe8301-0ca3-4b8c-9434-faae776d41dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 201,
                "y": 37
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a51afe7a-744c-4696-a386-04436098e3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 59,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a86046fb-8b14-4e35-8a9e-1f8490a1716b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 37
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d29ebda8-beed-4d90-8e59-b8c71bf4c016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 166,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "689e95dd-bc72-4696-a419-3ea9def19647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 153,
                "y": 37
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fb30ab24-93d2-4850-b754-fac19ed4a33e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 134,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0e82ccb2-e1c9-4de0-95b4-3b740fb60e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 119,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7ef9cc87-2c88-48fa-80e0-f7046f495f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 105,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "19bde5a6-a8fb-4c78-a783-a09703f78801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 93,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4f742fa3-9555-4a16-b476-3964c98184f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6bf44fb-0057-4c17-a439-a452027e803e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 81,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0fabc1b3-e838-4282-9af4-1574fa99dcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 73,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9aed622b-82ce-4c36-94fb-ca180ce4a37c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 163,
                "y": 142
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "501ce9bc-d49d-4c1c-b9fe-465bb8f7877f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 177,
                "y": 142
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}