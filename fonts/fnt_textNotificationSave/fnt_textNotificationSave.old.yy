{
    "id": "84895c09-2be8-4808-b426-49f93a3998ec",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textNotificationSave",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans Unicode",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a47aeb57-debf-4578-a51c-e5749b370ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4ebcfa7f-9276-4121-803d-c000f2dc6ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 113,
                "y": 113
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "33298771-52e6-4c3e-a4fb-f2862ead37d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 104,
                "y": 113
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3d2449ae-bf13-40cd-9f71-89bf4331606c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 87,
                "y": 113
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "45257671-2418-41b5-bfb4-423e09d806f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 75,
                "y": 113
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c5b478c2-81a9-4dfd-8e59-0995c890d2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 57,
                "y": 113
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8c28fdb3-5914-4321-a217-2793216b492d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 40,
                "y": 113
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a7383b92-e8d8-4fa7-abb3-c820df857470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 35,
                "y": 113
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "db99a0f2-8f3b-476a-8958-3d8384287e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 113
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0f998a98-51d7-4267-8674-12be0b6e2642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 18,
                "y": 113
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a4b0d948-442e-41d7-8f59-a44c937c3342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 118,
                "y": 113
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7cf8525c-531e-40d3-ac52-a4eaeac3c546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7db281a5-74c3-4474-a079-d0ff3cfd3169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 236,
                "y": 76
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "64d6c3fd-3014-46a4-89eb-55afa9543315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 76
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "81731e84-1008-40ef-ac31-0cc9b9361933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 218,
                "y": 76
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4e96b126-a05f-45db-905c-b96d14f1470c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 208,
                "y": 76
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c359c8e3-56b4-4153-851e-3777ad91c7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 194,
                "y": 76
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b66a7702-20f0-4d51-8b51-ab897480b819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 3,
                "shift": 14,
                "w": 6,
                "x": 186,
                "y": 76
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ae7c5ebe-c75f-46dc-b14d-d1da6e6f59e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 173,
                "y": 76
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "136ed225-0e52-43fd-b6ee-70c69a9512ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 161,
                "y": 76
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a5fdca69-07f1-4ee8-8f12-c8fb2d2eae87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 147,
                "y": 76
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6fc04b88-e5da-4f63-8433-f97a0e14792c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 241,
                "y": 76
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "60037279-ad13-4ab0-82d6-64ae3fc9ac80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 129,
                "y": 113
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2f8a3252-e85a-4ffc-ab27-1bd31d2c5966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 143,
                "y": 113
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1838760f-e933-4439-8b09-d5efe1be1775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 113
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c033420a-aaf5-454a-a15b-ec6f808b929c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 195,
                "y": 150
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "df97067d-e748-40aa-a72b-c86bae1d0310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 190,
                "y": 150
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7249bca5-3cb1-455c-9902-1833aa8724e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 185,
                "y": 150
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "241a2fa9-e2cc-4747-bf31-944940c174b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 169,
                "y": 150
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f59f0e23-05bf-4428-be22-4304e5d37f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 153,
                "y": 150
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c62c4f4f-75e1-4714-8b3b-d8a86fe31078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 137,
                "y": 150
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4247a249-bebe-4bda-a63f-7308de8fb488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 150
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9d15998f-9241-406d-a700-44eb9fca7685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 105,
                "y": 150
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0b33ac27-9c93-47aa-8e97-a1ea698989b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 87,
                "y": 150
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "95f0e1b4-629b-473f-ae57-d6c0478c3ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 74,
                "y": 150
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "311e9b8d-f119-47b6-9860-e151e8d9ffda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 58,
                "y": 150
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fbf641b2-f518-4eb3-8f97-483988d2a65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 42,
                "y": 150
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "49b162d3-eeee-48fc-a2a1-e3412a341dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 30,
                "y": 150
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9b9b926a-6474-42ec-9b45-2dbbeed8fecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 18,
                "y": 150
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5e8809be-e568-4e95-a1df-28af93a9c8d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 150
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "291f9c02-42ff-4947-9764-3ad389a3569b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 231,
                "y": 113
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a06fb8b8-184e-4beb-af46-ec3b6fbb19ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 226,
                "y": 113
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "40577131-cd96-40be-85b5-d9656be91d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": -3,
                "shift": 7,
                "w": 9,
                "x": 215,
                "y": 113
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6a217a5c-2699-481a-9d9e-77631c618d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 200,
                "y": 113
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9048a629-08e1-4f7b-aeff-995561c20f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 188,
                "y": 113
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5a4f2362-6ffe-4664-8190-3647d517460a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 170,
                "y": 113
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9e8b2aae-3da5-44e2-8cab-ac88c1e62b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 132,
                "y": 76
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "91cfb29b-0e9f-45cb-a251-733eb6df23d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 114,
                "y": 76
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4eb5f543-d7e7-4beb-9a4a-775bfa642ec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 101,
                "y": 76
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2df229c3-6608-44af-ab22-64e7dc6a460e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 54,
                "y": 39
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7e379aca-066a-47f8-a7c5-fa4e9f677b51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 32,
                "y": 39
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "dffd7a3e-5c49-4fda-8905-7ee0a55e627e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 19,
                "y": 39
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e5ec3801-52ff-4922-8136-547fa6ff8c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "31e8a963-9377-4862-9755-510545225ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "74da23bc-c1f2-4abc-a939-840274d41dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fc6e42be-e425-4c37-9fdc-a1e512acdfaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4e437dcc-cc9f-4d43-aec0-c23053df97cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d3a2fddd-4447-427f-9f4d-3d5385f9ecd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "51f1ea60-461d-44ed-b31d-c2a65030f456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fd1741af-cbce-426f-83e8-e1e3dbac446d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 47,
                "y": 39
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c76c9d3d-f1ae-4efd-98a7-a868ace98dba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c356f5fd-7170-408b-a3ed-6ca6d6f5e06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6450dd67-8dbc-468a-be40-cdd9dc0ba4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "68c456c6-fccd-47b9-b289-86640e04acb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1defa42a-4db5-4eca-a014-000ae35721df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1905594d-25f3-45a9-9402-6cada3c202b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a57bc127-bf3f-4f1c-b140-c4f1d24d7295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8cf45fee-8ad8-4972-98ff-d328d2a7866d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "40e8fccf-c41a-44b9-896a-9563be3eb686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8e464ab2-b3bc-484f-8fe0-76684a644f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c6af0e8e-4077-4cb8-937b-f7d16bde5ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "54f34c75-1124-472c-9e70-6b27240e8796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 74,
                "y": 39
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d7fef38c-7d0f-47c2-a1a9-dc3691e1b4b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 202,
                "y": 39
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d601f468-8a90-447c-a20e-a0482eef9baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 88,
                "y": 39
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fb284067-6e2b-4839-a67a-76867ae337ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": -2,
                "shift": 7,
                "w": 7,
                "x": 81,
                "y": 76
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f88c7530-a7c5-40a2-b175-02ec2b465772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 68,
                "y": 76
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c6ff6678-fd48-47d1-9d25-74f4b35eac3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 63,
                "y": 76
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "38dff550-0d2f-43c7-82cf-12417dbf806b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 43,
                "y": 76
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "82064b93-cd4d-4f51-ab34-812bdc38fd62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 30,
                "y": 76
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5046b453-542c-49ac-b0ff-f740e967c229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 76
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "695b7fd3-f1cc-4061-b3d0-c7336ad4ba50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e48c17d6-c65f-43e3-9eed-dc6370011ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 234,
                "y": 39
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ebecb469-6050-4980-b785-ffecacc531a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 225,
                "y": 39
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "536333da-4a67-4aa7-a282-eb9ed5f042a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 90,
                "y": 76
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e1130249-b7a1-4016-84e0-4f237f75fe26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 215,
                "y": 39
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8ab01d6c-bca8-471a-8a75-9fd664870442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 39
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4d858901-1a41-4ab0-a397-99d9a9e62302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 176,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2afb50b6-4e08-4946-9bb4-abd1d50bcccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 156,
                "y": 39
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f3c2b943-e86d-466b-bfc9-0bd7b5ebaac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 141,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "312e286b-47e2-46d4-b518-2baa0a18e308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 127,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "25fe5883-951b-49ea-837a-52499c40e6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 114,
                "y": 39
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7efdc1c2-b548-41c4-b83a-ad81ca96923c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 39
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e3f0b4b7-bee7-40e7-b858-15dc014d5543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 102,
                "y": 39
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4d70b4cf-8321-43e4-aac7-168f8b4662f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 39
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d3706ba0-4e06-47e7-854e-bf224394d5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 209,
                "y": 150
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "964275ec-bd50-4dc9-a335-9403846c2f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 35,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 224,
                "y": 150
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 17,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}