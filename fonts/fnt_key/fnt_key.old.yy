{
    "id": "e7f9efa7-8ae1-4c62-b12c-a2c414a7f59c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_size16",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1b41c94c-b09f-40a0-8777-3f49c101fe4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8a9d4ed6-0f3d-41f9-a5ed-3037a13913da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 73,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "80f98358-d1ff-444e-a414-031a904a8ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "45f4fa66-5f9a-4a8f-9c08-75bbca6510df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 50,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "596bbf23-2653-4316-8d39-966ce0d09593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "60444a53-bbed-44c5-96bb-1daec95a6d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 18,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "26b776ec-15cd-4c85-a0e2-e697e2e3b99d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2a499fee-527b-4a54-b733-0ba9d6b7c2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "eb20d68d-aa82-42b2-9ced-fd5823c29fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 237,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d1b53e53-0e8e-4fce-bf4c-7cf52b03db10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e1a50a8a-9905-4f92-87a5-9eb1751e883c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "972a8dbd-cff0-4068-b29a-b1ac0129e72d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 216,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a1a59f73-026f-4633-84fb-2be72d0fe960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 198,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ae101b24-d9a6-4a4e-9e0b-c822bbf9b6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a9f3469b-b69c-48ae-ab2b-3a7b3727561d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "756ca71d-c310-41fc-b578-eae75a6852c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ee283303-7b9f-46d3-9a76-543225411bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9a8c4129-8edc-408a-8f0e-cd09054d57d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "57ee280d-3e41-4c1b-867b-799f148b2005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "06ca9089-16c8-4886-bfd9-515da7e60195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 128,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "753a11c2-af70-4154-bc40-f5c22001ec72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1b4cefcc-a11a-4aee-bc13-f749b3b277d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "521de57b-ce10-4c09-981d-76a617d21c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 89,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d8909ada-156d-484b-a45f-ef8fbeb3a4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6deb348d-be54-4a1f-a734-8286f498545f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "53c2d16b-b7cc-4c30-95d0-4ab82ccffb75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 150,
                "y": 110
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "041a6a80-facf-493e-87c7-b25d82631b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 145,
                "y": 110
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f9a9a5ee-15ec-4c34-a2b9-605520dbd3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d9902b56-1532-4e00-87a4-75a1331efe8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 127,
                "y": 110
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ca5439a8-8a7f-4dde-9be6-cd67bca19fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 114,
                "y": 110
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6df520ff-3388-4db7-aefe-13363e9f9b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 110
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "781628aa-4892-4888-9dcb-77ef1a60d5de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 88,
                "y": 110
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d217f3c1-1ebf-45b5-b331-7d8678d2b5f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 66,
                "y": 110
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "65ded1e0-eaf7-4a45-9db2-cce3dbd91aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 48,
                "y": 110
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1a5eadca-ce3f-43df-9306-1217ee987f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 110
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4f19a477-3e29-4058-b340-ebf07e011531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 110
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "278037c2-01e9-4bf7-9984-5f5aa004d2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d71ef9e3-89f9-4467-bcaf-2647604e8122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 83
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "aa322a9c-5857-4791-a584-29da0e18be03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 219,
                "y": 83
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b758e59d-cb2c-4350-a166-02724631eff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 202,
                "y": 83
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4b584a35-9d88-496f-9058-5436f578541c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 187,
                "y": 83
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2ff40616-66f0-4ba0-b1b0-a34643d756bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 182,
                "y": 83
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c8713e81-bac8-4eaa-bcb9-992361aa82c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "85aaaafe-5fbb-48da-97d1-02fc3c2b84f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 156,
                "y": 83
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ff685e19-93f4-4794-8f04-130e2fde98a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8930ee96-e22f-4ed4-9492-42404892c95b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "75690e97-b03d-45d5-9fe7-dee82442820a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d1dd61ac-c2ab-4200-8598-5f30b62dc1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 83,
                "y": 56
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b3882ac1-05a8-4286-b8ab-8f3344766842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 68,
                "y": 56
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "65efc42a-a23d-4b00-b430-98e507122a9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 29
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "94eb6fdb-eb3f-446c-a2a9-2cbb7ecda7a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 17,
                "y": 29
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c1f2323f-b9d4-4bcc-8ca3-6afed07cdc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7a271e5c-15ca-4d4a-9784-16b89500d361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fd41814f-bb71-4f62-bcd4-d8a7c9951860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "794319a4-4356-47f5-b804-3de61094acc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "40c44279-ba11-4d92-8de6-96d5499b0928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b37f22d3-46d2-4680-acfe-36c813dde125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a340cae1-f967-4a6c-a804-21c818eed3ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "89a97412-75fc-470e-994b-c2136deb3e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0340eec8-960e-4e9b-89a9-627e6de5f7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 33,
                "y": 29
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "61d2e79f-48e2-431f-9dd6-edc08dbc29a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bf1974d6-0a61-4dbe-93b0-18f84a803767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d6115f94-f2a1-42f2-952e-2677b05305a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a639a81b-f83a-4e01-9d74-d279fb25c47b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a484ed67-aa9e-4066-b646-cc7e8f434fdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ebd4481c-ad53-475e-b4c8-c35b375f3428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "86499b35-a4a1-41e1-beb1-69cbd7a42a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "09f8e8ec-f974-4347-8bd5-caeb2e541a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cb8a6b4c-ca0b-462f-aa31-f3245bd3295f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "62166703-b7ed-490e-a790-2cddbf6fb04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4fdd3c45-dfe7-4f69-a933-888aa2ab155f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "357351f8-e799-4289-af9f-6f2db228cdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 58,
                "y": 29
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9fb2cf26-c7f3-4c89-96fd-8fb40d299880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 180,
                "y": 29
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "faef923f-d565-4f9b-9463-43a83fc3aa4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 71,
                "y": 29
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bd2e30ad-9b6c-4af6-b9cd-201dd0cd9f2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 49,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a3289efb-eee8-4e9a-b009-8f042578f77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "eaedd289-ae1b-486e-9b6e-a97e3c843470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 32,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d4131396-c09e-4b04-b923-dc4f100d6f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dff4efa0-9cea-4dc3-9fe8-2ef317781039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "04476620-72d2-44e1-8682-7239a8ac395e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 29
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c1994181-c5a4-4607-8494-2d1025dd22b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 222,
                "y": 29
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c351fb97-0027-4382-8d7a-8b87c54afcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 209,
                "y": 29
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "cb910069-07c4-45df-93fb-78669b90dc4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1e905472-3546-4f55-af17-fd678f49d66a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "481ca052-f8f5-4fc1-aa72-61d392d80816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 29
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "71b94f86-6f7d-4092-b450-0a0b6a482f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 29
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2883de4a-1275-454d-905d-b8789e6fb97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 155,
                "y": 29
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "02e2bdca-a634-4f22-b19b-a59707e7d320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 138,
                "y": 29
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3e1155e0-fe46-4e77-b706-431e651a7124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 29
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8cb13d82-6211-4ae1-8f0a-427e39c9e479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 29
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "add2b09c-d4fb-4c77-9e60-c7c72f39b00d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 29
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cd294651-e0f3-436c-89e4-977022af70ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 29
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "312c1b87-69a6-4c18-af4a-5387874973df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 29
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6d3a570b-56f8-48fa-bdf6-d40a2c1036fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 29
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "258be1cc-0463-4e28-8e22-46a7d615ae42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 163,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1329efb0-ea8f-47d7-8818-2106e80db398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 177,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ca29c052-abe7-41e9-8bb2-24520c2c5a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "c2a9e45e-2b87-417e-9b81-5757049ce59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "e2da9a55-9ba1-4718-ba0a-48e239c7ac34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "f7a01948-bc95-4315-b663-086fb2a2bce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "0a59cdff-8d0f-49d7-b518-a7962de54cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "0c229a46-c5ec-4529-9c92-c82026313808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "bf56b428-67f3-49ca-9609-3fe27ce276b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "9a0a8b88-1ef6-4aa6-90b2-61f41c94353d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "87847d93-44e4-4495-aa18-d44166f70720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "44585b16-918a-44d2-832f-057ae0b40ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "65d14813-c6ad-4ce0-9e12-5a485be7afa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "ad139502-f372-4f35-8a18-74c6ffab432e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "5d83ecae-4e62-45f8-9ae8-ebcade65fcf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "3e9be166-a6d2-4963-bb65-6516a43e222b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "e7ebef21-b0d2-4bfd-ae31-cbf89cae8386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "f65791b1-679e-4981-8cc4-402a7fbb7854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "cf54af8c-6f3c-41f8-a5ec-2999b12366fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "48b8a9f7-5cc9-4a1b-8eb9-faf68845d924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "045e3bbe-a89b-4d80-9208-9f7dde8d3ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "d3bdee33-2212-4abf-9612-987c1de4f313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "77161b8b-f3ff-46b9-924d-46b8ec169f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "9e249f5b-266b-44b0-a1f3-b80079335db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "c0c8ff60-8c0c-4caa-a33d-26cc3855b4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "75cf7cc2-631d-4e63-8b93-71ce6dc3fd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "15abf28d-50cb-4df9-9ba2-64bfd29a135c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "7f5c0f43-2a42-44cd-a861-0296552458de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "82b7d88f-a6bb-47f0-b306-542b67c9f130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "2d9e2669-7a98-4c93-b566-95df8e49490e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "9c37eaf0-66dc-4778-808c-24e978cb440a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "aa7e4649-5999-408b-96b3-0a82d200fd42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "2cfa416e-b3ef-473d-8830-7cb0e0857012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "e85b56b1-3608-439c-9544-0bfea3d74f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "1c0cd131-20b5-4e6b-aedd-8c36de57c6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "781f89ed-7807-4300-ada5-496191569e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "4b4ac8bb-9c48-476c-b468-2bcc291a7bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "7c85909a-5d48-4cfc-8b1e-986204c26521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "9333997f-4760-4246-a5ac-ed9208bc7708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "d95b2e6b-e3cf-429e-8aa9-fe2aa515f9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "b8d7402c-93d2-4969-9e70-33515363cc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "1f1fe5c9-ce69-4602-aa8b-15a57c30636b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "85790936-dd6a-463e-8df0-853cb3edee0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "e3f84628-afc8-42e8-967f-5d3cf0ad89ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "9857605e-256f-4676-b090-39f5f4fc914e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "6a7a25f6-79f6-464a-8c77-76c99b9de1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "17406efa-c914-4087-999c-23a3b7a25722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "8451edbf-ab06-4743-a578-d4347b260d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "4ee1d9c4-2ac8-4c37-bfff-685f927493c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "882668dc-d6ca-44f1-b11a-89b9a17578d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "404cc35f-317b-440e-8514-573512dac8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "a19b77cb-6bd4-4f07-8331-776532ac260f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "57cc7e04-d57a-4517-ba52-490e59a611fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "333f1b40-b6af-4916-933a-8a1a14310853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "598c7887-feda-4d56-a1dc-53e20d8a4838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "9295b856-4877-483c-aacb-731fb1f3751a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "3e8a68e7-13d8-486d-99f8-cc101144062d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "a50034b1-b917-4f29-aad6-62069baa3294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "bb89e89d-9539-49f3-814e-10f447fbf291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "a452aa10-33aa-45e2-a850-31e4d4250af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "e24836a5-301c-4524-a07c-caf4ce4593c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "7d91f80d-4b40-4ad9-b313-f7e055c724ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a077e4af-f13d-4a3f-b90a-20f68eab6a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "8cf8b91b-9816-439d-ac7b-f9e8fea5703c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "da5a045c-a79b-4781-8124-015111dabba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "47c95eff-3cc8-4ea4-a0c8-9ea188ba4380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "45c9cbdd-55d7-4d79-b1a8-42b69d76dc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "9e8b217d-da4f-44f1-9932-1f065a321565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "b87e111f-c4a8-4a17-bd5f-ccb8fe2b1987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "91036c6c-2ac7-4ff9-ba21-1e4b303f4c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "79b21bcb-be54-46d1-9291-1a227f1c93a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "7508c55e-0280-4818-8fd9-b58841bea1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "66477ffd-191e-4077-b6e0-687dc445c04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "067f5756-1965-4e47-b029-fdae3322aa47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "231ed81e-d42c-43bd-825f-304958f3b99e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "b95f89c1-ca5a-4af9-9520-0fb753cdc8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "9a627eba-dc1c-499e-92c3-8f00677512df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "3e12cb07-1c34-4a5c-b371-47da69ac661b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "18a73353-db02-4c41-b9a6-a9fad8f2fa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "6b67944b-e05a-40e1-9574-9d29b5fd958a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "53e92db3-307b-4821-94d9-972e408ba838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "0db6b01e-cb57-48f0-b0bc-5bd186e2c7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "5b96e12e-3cd9-400e-97ba-c19e48378cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "45902f51-ffff-4f3d-af68-9bba54e34cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "1c4f455f-20d0-4555-81b2-d771299c134c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "8701a12f-926a-4040-bf2e-4ed278dc9263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "407d7e1c-71f8-42b0-8ad0-e2e5913e7290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "10b1dd8f-4682-4bf3-9dff-7caeeb5d73ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "b7622177-08ff-4d1d-b5bf-1b8b61b9185e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "4a0521ec-978e-408b-b3b5-2a5d408c6797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}