{
    "id": "e7f9efa7-8ae1-4c62-b12c-a2c414a7f59c",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_key",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bahnschrift SemiBold",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "10755e18-6a9b-4180-b785-7798c86f432b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "64ee24ce-0daa-4d0e-aa82-2be095ee3f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 25,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "23f55bfb-810a-450a-ab04-22d5c24a271f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 17,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8bc46172-d3c6-46a1-bcfe-9eb1f56f29dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8757a6da-5aff-486e-92a2-6cfc311a9e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 242,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9ac182a5-2ce7-40f7-9418-79cae5d014d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 227,
                "y": 58
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "38d3600f-cac7-4436-93c1-dff24ac8aaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 212,
                "y": 58
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "afb898b8-3122-42b0-a7c9-42c78e361135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 207,
                "y": 58
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5f957079-b76d-4c8e-97aa-be22b78f91c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 199,
                "y": 58
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ac40ae97-43f7-4aa5-a8bf-37dce152aa68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 191,
                "y": 58
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cada10f5-4374-4c1b-ba6c-3cf1201a172f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f0138843-83b3-4886-9476-49149424d667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 180,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2a057676-f5d7-4da8-a51c-3ed879e3802e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 164,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a87b28e9-6359-4cea-96e3-6d1aeac5775a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "027239ce-aef3-4284-a81d-fd73f93a4bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 149,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f6846c8b-97f0-4bfd-a2cc-cb37c4dc0829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 137,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2ba66384-3576-4a27-854c-4a69ab28fbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 125,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7187017e-4aee-48bb-9cb4-39a1bc0ba387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ed50dd4a-cbe7-4b9f-9a4d-e968ea83be27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "93bf5b4a-4baf-4491-82cc-540178e98529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 94,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "17263536-8f17-4c73-9d59-dab11b40e205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7c1ec3a4-a43d-4f90-864e-94866a3d5bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 169,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9c06015f-f094-42c9-adbc-e6d8ff641cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "03183ada-a295-4db3-84c0-78f901e70f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 52,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cb2cee98-691c-48dd-b5df-6544b0e9d770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 63,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fa10f281-77fc-4524-800c-dc9dd60e21de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 72,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "091df8a3-8dbf-4fe3-9ce7-39084c9b01f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 67,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d4b25546-0792-47da-a30a-c898b7454bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 62,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cc044d07-5d4e-43e4-ad66-99bbdbb280df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "70ea2c98-ac5c-468e-8834-53ad310d200a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bcd88de9-efeb-4eab-8c59-f25c04b67b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0bda2834-4a17-40d1-9ea7-d67e8df0ac2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 21,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4c65d4ba-7ea6-481b-9744-e62cb0ae739b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "24c0340e-7c60-4669-a39b-309b8b185bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 230,
                "y": 86
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6b359e98-fdf9-4d2d-8882-c89addd95ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 216,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6ba861b9-1e88-4131-8e2b-1c6c00e59d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 203,
                "y": 86
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f2858d7a-4bbb-451a-9ae3-98a4e3b8e01b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 189,
                "y": 86
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d5747ade-afa0-4a6a-9bf6-749de465aac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 176,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8e467cab-7e44-4b47-b81e-eb329707c687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 163,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8c3a0823-395d-420f-ab39-3e4b1869ec74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 149,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8f994ff1-c91d-4a2b-8f58-7431d4d83301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 135,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a15ac520-fddf-4729-bcc1-70ae8830034d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 130,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d9e18165-b07a-4f61-a2c3-b3b23e91ca27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 119,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "061eb581-0da1-45a6-9337-e4d13fea2dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 104,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ff161597-9013-4df2-89aa-48bb5eb492e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 91,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1c4c785f-2947-471e-821c-6b7e527c65dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 75,
                "y": 86
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8be37aba-12c7-4106-8d41-ed6c6b535f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 68,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "85c8fa29-6048-42dd-b99a-e5b82e7f9779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 54,
                "y": 58
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9ec367c4-c4bd-437b-9b7b-b82c4da764ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 40,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "731936d0-5143-469a-bfb0-cff61a00f954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e60776a9-dca9-4ee9-8612-74408ebcaeb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "144bb863-e8e7-488e-ab16-c068afe28b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7f2fae87-d895-4e6f-8cfe-96ed5c92024f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "325a017f-850b-440a-87c3-9758389cbc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4c394b5d-57db-479b-8f0c-a3061fea4177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b2173937-94b9-48fc-bf44-7ef148fb8581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "747e0fe1-f164-4763-96a5-35da8d4bdbd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "68113e3a-32aa-45ac-85aa-a8e3f96ac913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "64ac03f3-a75d-42c8-8f75-1c34c5adf90b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "48f758b6-0792-480c-a594-2159fd2a2315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 30
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9acbdca1-5944-4b03-a401-cf89ed641d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e9b8bb08-f72f-476c-ba48-0f1178de3ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "bae59add-0fb3-4efc-9832-f42922130d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1b6a456c-c298-42c6-af89-8428a0440b3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "78ddbfb4-9b37-449f-8690-a941c35429c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "caf41e33-9855-438d-997a-7044319fafe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7061c142-3950-425e-9305-34dadc33fcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "12658ec6-ef5f-4314-b4ae-3ed98a47e455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f73c5ace-71c1-4707-b9e3-353499e84b44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "eb3b099b-059d-4025-b8ed-2709c489619b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "abb6ed1c-865c-46d6-930f-0da17e8ccbb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c34540f8-39bb-4ef9-8631-e06c13bb6e52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8f3ab907-5dc9-43f8-a7bd-a7c0f9168d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 156,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "41b7473f-cd77-4a93-9e84-e0f332171c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 49,
                "y": 30
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0d211f0d-67b0-4af7-b433-07f85ff0262f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 21,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1c4ba086-aff6-4c33-a9f1-d4daf864f6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 9,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c5992f1f-87b2-4917-be84-4eff448ebf2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f76ef73b-342f-4f93-8d51-ed4275ac3ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 233,
                "y": 30
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5514feaa-9cd0-432c-a42b-16a84923606a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 221,
                "y": 30
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6374aeba-c8ba-4124-9141-87a2fc4248c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 209,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fc80dbff-01aa-4f88-b5cc-6578abd47b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 197,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b04635dc-c3a0-462a-8b22-18321641de63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 186,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b41df0a0-40da-4449-8c42-43023039452f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 176,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "09ed7eb9-0617-479c-a0ef-20ab49c442ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "992a9685-5109-4da5-b317-0924d734e38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "90eab209-4163-423d-806c-0422421a1316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 144,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7478651c-dbdd-4a48-b505-d5db423adc09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 131,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "863e4298-fe3b-47e0-97a6-1e758a014414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 113,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c9c1fd9c-9f3e-4d42-a38b-7bc8f2470f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 100,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5a65702d-0d74-42d5-beee-6832960cfc9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 88,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "25b7ecbc-41ba-49a3-bf77-a139b56af7cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 77,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "dd496aa1-dbe2-4541-a674-78b91cd494e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8dfb5097-f2d3-4383-9493-f0ed9af657ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 63,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b7326f38-13bb-4b1b-854c-8739a22dcf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 54,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "749f1f49-cc5e-4139-8857-95ed1316fafe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 83,
                "y": 114
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3f4c0535-230b-40e0-a4be-ccfee7b32689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 26,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 94,
                "y": 114
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}