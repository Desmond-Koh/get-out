{
    "id": "4534166c-a208-4adc-b9d6-f545270feb62",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans Unicode",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d60e0473-b87a-436a-9912-92bfdf72d3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "72e67446-13e8-4bd9-840d-76106d58344c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "560e427a-2eca-46b1-a299-b0480c84ece9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "0606ea5c-bae7-4e41-859e-a41dcfe16a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0484705f-3ed7-4c05-b0f6-dc621cd971d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 83,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4970b0ea-54e2-449a-910b-4120b83391a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 74,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9bd1e5aa-bfcf-49d2-826b-96d885bac602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "da9954e4-49af-48de-9760-69b4ad4c1052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 61,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b2fe3e84-f13d-4e06-acec-28612eea6147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 56,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7ab63884-1ee7-4edd-9607-5433b5de8e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 51,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ad7aa030-863e-4cae-a593-8447ac438917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 106,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0fc9b665-00fc-4e16-a7dd-0193df579807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "78b02a19-e6df-44f9-ab93-4598e237e818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4b3be627-8073-4bc5-b5b2-f8b945302a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 25,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5df014e0-9e45-4d1f-a3a0-941658f77c97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "bf9710e4-fd84-4b7a-864a-9531c19fe2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 15,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bea74198-de86-47e3-a69a-544c9854a860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "34e81435-aaab-48d0-beda-d589c4f9ce0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "15c46c4a-31dc-4010-9290-cfc701e0bbbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 118,
                "y": 34
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "24803d4f-9f06-4bbe-a768-4a08e51844b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 111,
                "y": 34
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0042e553-51e5-4770-ae9a-62fd4abc2ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 103,
                "y": 34
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "eaab170c-1596-4a62-8afb-448b6f00d319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8eff019b-1f28-4961-b955-c92895bfce23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "120259fa-1d34-411b-b631-899a1b52ffac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b2f55587-2a28-4729-8bfa-9603414906ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "42be4571-1aae-4346-88fa-8e1f410cb18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0a918d96-82ac-4874-9b86-b9afefd9498f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 29,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "90f22d2a-7e45-4e05-9323-6592d823a10f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 26,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "493c70ab-cf49-4b00-8c38-826646e2569d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bfc54694-1613-4dc5-bbd2-bcdd8a9d4df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "817ae13c-e1d8-4fae-b05a-d21d3adcf158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "17272fae-687f-4804-a251-4205b9f93f89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 66
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "11ae4a6f-e095-4d67-bb70-84a587f1d3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 108,
                "y": 66
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3f295f6e-43d1-4c54-8e95-5059b5b2692d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 99,
                "y": 66
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "64dd52bd-1f05-4094-b4e3-1aa490b90254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 92,
                "y": 66
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "65d52dbb-0b6d-4efc-82b8-f2b736f65d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7363dd68-c868-481d-ad2d-fdda86445fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 66
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "0ce624e9-9251-4b0c-a8a5-f183a847e821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 66
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "72dc2f78-5e15-4de7-a9c3-bf53f36c787a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 61,
                "y": 66
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7a0a73a4-84e1-4213-b0bb-cdf407a06647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 53,
                "y": 66
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "184dcb8f-44eb-4b87-a097-94b1bb4aafdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 45,
                "y": 66
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fb81c0d2-5d25-428e-aefb-0420767708f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 41,
                "y": 66
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "16ae7b0a-58a6-4c6a-bdca-4fcbaffd5ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 35,
                "y": 66
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0f24c3ed-de08-4ecd-baae-03b9f3b3a9aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 66
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ef97a0d3-b5d7-4f1f-906c-f3f961e2592d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 20,
                "y": 66
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1b87800d-a817-436c-a9d7-c155b7ef052a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 10,
                "y": 66
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8ef1e529-05ce-469a-adb2-8bea24d6abe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 94,
                "y": 34
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "517e642d-42f6-4a85-ac10-46438ee092a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 85,
                "y": 34
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "12c7b093-8bb8-41c1-9544-ba55d4e138aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0714a45f-0c86-470a-b010-4af1dae42e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "065eb671-6c4e-471e-ab8d-3c517b8699f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 25,
                "y": 18
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "45e2b9ca-8b3a-4adc-856b-4338d18f6940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 18,
                "y": 18
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "57b587a7-8398-451b-8ed5-553042c33a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 18
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b9f7107b-c703-4f43-bc5b-13472844784a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "694abb77-9059-430b-94f2-d6a3c0467395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bc7d86e1-2ba2-4383-91f6-dcc975e2d871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "245376a8-e0ef-409d-8c64-422207a5cdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b03cda2a-89c4-4a34-ab1b-bc51bee38a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a612ae10-7578-4efb-925c-a2989cdd31b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fbcfe667-99d4-4faf-a0cf-b4a4ded4a1ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 33,
                "y": 18
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ff1afc23-e67d-40f0-b1d5-81b15195cb9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9f3a9724-0408-45f9-a43c-0f37d53b96c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "eba4a4f5-830a-4950-afab-affecd0d2bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1ad19f29-34d5-47f5-98ad-62ffbc51f601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7298aad9-3dc7-4785-b91f-7834589d1356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d6884b3d-0a73-4a26-b2d6-b89d8f2f7b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4f4df60f-8d40-4449-8445-7124ffe39c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d73f5618-c330-4687-9a58-5ef7365b54da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "54092a38-022d-4cc2-b436-9eaae9f976a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "23c928e8-5004-457b-a8c1-baa509c57a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bea6f677-add4-49e5-b2c0-f4bcde372beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ae5aa7ab-ac0f-4bc5-8734-1bd81eae465e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aded8ea6-5acf-4816-b01c-a11a8e0042ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 118,
                "y": 18
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a150634f-ac2a-46e6-b222-c46e4c5892b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 55,
                "y": 18
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9706873b-1a00-45e8-ac4e-de738f91fb70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "60a38b9a-7d46-459c-b89d-d8d8a6b7721b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 58,
                "y": 34
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "376aa5d7-639e-4c75-81f6-bfd8325685dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 34
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "80b26905-6680-4578-beb7-64c1fd967a2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 34
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c84e502c-e5d8-42a5-97f5-c97a608ed000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1014e8fd-a861-4ded-a638-e9cb7673ad2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 34
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3782371e-2e40-4381-9284-51fad54e52e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 21,
                "y": 34
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5524cd96-a6cf-46ae-8a3f-35928dcea888",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "42c5ab52-5233-4dc8-98f5-ddf7fff04e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 34
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b0b5b935-9dfa-4ebd-a8a0-140f85c4e4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 71,
                "y": 34
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cd744168-e338-4e92-91cc-9342bffffc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8448df0d-569f-48e7-879e-13caebc92a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 111,
                "y": 18
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ea5a08d5-b79d-43a6-a27a-3673b8bcdb2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 104,
                "y": 18
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c893e807-a579-4ef2-a0b3-ae1fac519c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 94,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f8b32468-c151-4d45-b6eb-fbd4e6c15e1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 86,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "609cca3c-5ba1-4807-8ff4-0567504de5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 18
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "5ff45bbb-5261-40ef-8294-fc478a9e9e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5a6227c3-536a-43f0-bdaa-1b7dd3219adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 67,
                "y": 18
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "63efd0ef-aaeb-4e56-9bb2-7692d9cfb0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 64,
                "y": 18
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6a5f7d91-5904-4324-a3e3-c944121c8ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 59,
                "y": 18
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3e4e1107-9aa3-4b72-8710-460a056cf8fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4170b661-6103-4bfa-aeca-4c2e522a1949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 48,
                "y": 82
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 7,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}