{
    "id": "4534166c-a208-4adc-b9d6-f545270feb62",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0288a99e-299b-44b5-9219-073ebbb78359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e9145760-bce9-496c-ab39-f1e927368dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 19,
                "y": 35
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a1c1e000-e959-422f-9d50-ddf108148ef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 14,
                "y": 35
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9cb94818-9a50-44f8-b811-d5670f616795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 7,
                "y": 35
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "673588db-c917-4a30-9ea1-4943fb42c14f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8977e26b-6b2d-4a8f-b88a-864e7b7e3719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 116,
                "y": 24
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "881b21c3-a3e2-47db-b53a-251745e03018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "7ff2b638-16ca-493a-af6d-9c0cc5ea2250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 105,
                "y": 24
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3fad8b8d-7350-4ee0-a1d6-67d2081d6b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 101,
                "y": 24
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d3403a16-1896-4032-8cb5-1cd26cfc3f8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 97,
                "y": 24
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "11310ce9-b01e-4e94-91dd-f476750a37f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9880073e-9389-4027-b890-d91bca586e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 90,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e071cc6f-6e84-4e88-8eb2-5b0311cf2e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 81,
                "y": 24
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0db17631-deb1-44bb-90d8-6f927781819a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 77,
                "y": 24
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8f06e7d4-d631-4bd1-b756-7052eb881627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 73,
                "y": 24
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b92b093c-efe8-4c7c-8180-951670746e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 67,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "580ded85-ab9b-4f3c-a327-843a31600f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 61,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8f8cac7e-142d-4b98-ac88-e917bd9745ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 57,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0485599b-4c2a-4994-b60a-d554d9803839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 52,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c1888f4e-ee00-401f-9161-ce8f67ffbef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 47,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1d12774d-7899-4d02-aded-fb199426e78f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 40,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "099bf4e9-a8f1-493b-9419-69835a63e77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 85,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4060a349-092e-4d19-9ed3-cc280332ea33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 28,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e42cb399-9f80-48b4-9fa4-684daa6d9cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a5b1faab-fe96-4fdb-9bed-391867b21b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 39,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6a939d04-1c44-4213-bb09-051fe7b18eb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 45,
                "y": 46
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3224f266-5bff-470f-9f47-ce20b596ac5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fd40b9cd-bbec-4ddb-b29a-6a22baa4f845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 37,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b18b250d-e670-4936-9354-4df13efc9a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "34251bf1-415d-439d-bcf9-9be5403d65e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9a1fb5ad-e423-4955-91bc-95f4556bc719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b05e6c04-996e-4158-95d0-dc3306ca398b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 11,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8d5600d2-00b4-48e6-9eb2-bde31d9e0809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a7de1dab-e212-448d-8d68-bbe2d35b0f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 114,
                "y": 35
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d6c65315-a287-458d-81a0-31d903789d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 109,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "64eb3230-413b-4548-917a-067d4786794f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 35
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "70717a06-7c1a-4d4f-b02b-1d82b9124278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 95,
                "y": 35
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "273fb212-24e6-46f1-8f15-9f678e51a5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 90,
                "y": 35
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ad987272-6cfa-48c4-b2d0-9cc0784a146f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 85,
                "y": 35
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e76ad549-7684-4ca0-90cc-625db903a931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 35
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e802bd41-5de0-4f02-8d3a-b44ed3137e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5f4271c8-5344-4856-b7f5-5bd4a67701b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 68,
                "y": 35
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f4047693-d50d-4ac9-ab9c-89d4aa1a5cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 63,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "db7fb5ae-6901-4997-aa15-ffec8462d46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4fb9bc89-40e9-4a02-b2d9-e772a53ce8d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 35
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "88e99681-1a04-434a-872c-bd6e26deb877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 45,
                "y": 35
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "89552ade-8bd4-4a03-9366-5ebc87c57185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 34,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "76592081-e3da-4345-a2e7-0a0380dbaabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "33a03562-671d-4a09-bf22-2c89c482e206",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 21,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "51c80710-2ba1-4590-ba77-0649a6915551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 13
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "098061d6-1db8-4c6f-9c0d-f3282850882e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f2b3518a-4504-426a-9ddc-f21b07b1c0c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "264fd08f-60a2-4eb0-95f9-5ad89a73fda7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "219600c4-60f9-457a-ab10-b1bc7466cb2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "517ce3e7-056e-4bfe-a23d-6a4d593a35cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5d6baad0-c7e0-46a1-83d6-01128fef9c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "41d35725-6254-46d1-9dd9-ac400f14f19b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "872be29c-bd48-425a-b30b-008259779179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e647f2fd-6044-4679-8b83-c566890900f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3825c5e2-f262-42bc-926c-3d956b94d5f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 8,
                "y": 13
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9255348b-2d6b-4784-81e3-5b9fffd63617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5f36c8a2-7c24-4bd5-a59d-cdd71c5b32dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2200b5e9-efed-478e-b41f-f8b4c9579ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b2df46d1-ff8c-4ded-88f9-a48b6a3ff7b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c5483ee0-26f7-46ac-b790-4078eb71fa67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "322ff056-d0b2-4349-a66b-5c1ca3fbadbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "289b653e-6007-4d88-acdd-84c81af4bcee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c5e2380d-6ae4-41a9-9ae5-dab6ac7d1d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d09a7362-fd77-4e16-8d97-4863451726ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4e3499be-773b-4ff7-ac0a-ffa6f1a2b1eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dff8adc4-5ea4-4719-963a-eee59ac6a998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1d19b2e7-5ad2-4e84-9f82-7e796fcc543e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 19,
                "y": 13
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3a44f92e-97f5-49fc-bb5d-a8e6042a3b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 79,
                "y": 13
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1168d695-468d-40f9-b647-c973c58cf61a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 25,
                "y": 13
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d23c4b3f-e907-4b85-92b6-d8441aa492ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 12,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a86c91c6-ec24-4d72-a88c-f370604af6a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 6,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "197c79bd-7728-478a-9f9a-dbfa6a87f6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "42360e07-12ad-4900-b743-f02a5f21c230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 116,
                "y": 13
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d8a98acf-0c62-4d1b-82f3-a42feb02f0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 111,
                "y": 13
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c8db67a1-7895-4a5d-8cd0-9552ce959c00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 105,
                "y": 13
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "13c55fd0-7047-4a1d-b96b-1f80c0c26e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 99,
                "y": 13
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8b354a84-fbcf-4e1b-9654-588c76d3bd1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 93,
                "y": 13
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4b9b23d0-02f0-4fd1-a3d9-dab3352eec08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 89,
                "y": 13
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bce1afeb-ddb8-44a5-92a3-081605f372f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 17,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2c1a2f1f-b001-41b0-a276-9570764e629f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 84,
                "y": 13
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "75d59ccd-0ca5-4ecd-a15a-8eb30f646773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 74,
                "y": 13
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7bdf879a-43bf-4290-94f7-6ec4ccdca750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 68,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "60f8f44f-06c2-4bbb-996c-841d7e438454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ec4e9161-48ee-4e66-b2a0-c9d16acc1acc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 53,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "59a9426a-5910-4170-8d77-2bbd6eefe5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 47,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cb66f2fd-3252-4da5-8900-a55fc827f9ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 41,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "18c52ecd-c987-4e61-a707-34925c8c06a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 37,
                "y": 13
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "282f6c83-ee28-4eb5-a58b-2b601d5b118f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 33,
                "y": 13
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2b8f241d-c74a-4f3e-938d-08c24b05a3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 29,
                "y": 13
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f16b905c-5dc3-407f-a68f-c7d3b2ec1c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 46
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "a5b5fd3b-29af-4b09-ae81-862f4e8ad334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 9,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 58,
                "y": 46
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}