{
    "id": "0c917902-a5ff-427c-bab9-aa3ecab2a5be",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textNotification",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans Unicode",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "14499f6e-3c2e-4994-8660-b197423ef6af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f147a266-f561-4963-ac7c-2f9f9f6d7e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 70,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a238fd73-87fe-492a-9f53-78879d4a5865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 62,
                "y": 107
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c0ee5f48-a543-4370-9b21-989eab49fa64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 46,
                "y": 107
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "27d7fcda-5b7e-4334-ac9f-e0ac6c1d0c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 35,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "71c8e8ee-1d27-4dd6-aedf-c4758374f890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1567ae38-8590-4c69-8c79-dfab48f91941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "792e3dbc-5922-4ff1-80f4-c70cdc780538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 248,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1e5388a8-39da-47de-bb7d-6bf936b2341c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 240,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "60eaab03-a145-41b1-820b-6e040fdc8bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 232,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "024af0d7-84e6-4cb1-bbe8-4fe3ee573eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 75,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "df4bf6cf-4584-48d9-872c-abf5020881b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 217,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f7b373cd-546c-4a7c-82d3-8f82f76429f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 201,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "116f88f6-ee2c-4d9a-8819-d1753438446a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 189,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f7c12a0b-f9fd-404b-b6cf-a3387a9a75c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 184,
                "y": 72
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "de3b98ef-f998-47a1-8afd-5155cd4815a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 175,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "5a545a4e-cd0d-45a9-80e1-20b6adf6c556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 161,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0972c907-38a3-46f6-9875-6033928b80ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 3,
                "shift": 13,
                "w": 5,
                "x": 154,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "70f1bc1b-1d35-474b-a218-be87078dd608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 141,
                "y": 72
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "562afcf7-fc2a-4c49-8823-f45aba047313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 129,
                "y": 72
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7adb605a-6ac7-4b62-9e86-cb96a4d34daa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 115,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "72eebcdc-c27f-47f8-869d-8b2b23ca0c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 206,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1c7b6f78-1ecc-4f9d-907e-1acb95fd9ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 86,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2be025d9-997a-43ac-8c5e-1576e78bd16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 99,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6ca1906c-9cd7-4243-af9e-cea6b4ff4c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 107
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5b039fa2-b35e-4f54-b06d-6d049b31b258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 149,
                "y": 142
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7b2c3204-8488-4136-8dbb-19684a1e485c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 144,
                "y": 142
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b5d4d098-bf6d-4cbc-a83c-c35f80596dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 139,
                "y": 142
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9c8f55e8-c2f7-4c57-8add-065d57df6671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 124,
                "y": 142
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2df039a6-70df-47b1-9081-30a1d5efe529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 109,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2d46436d-cba5-4e51-8063-617884553ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 94,
                "y": 142
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "07b638a7-cd7b-4180-918a-756ef236f6a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 142
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5201a9e6-efa5-452f-9198-8ad6afb7f6e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 63,
                "y": 142
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "37e69d81-afae-4967-8a04-f37181296c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 46,
                "y": 142
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bbf8732f-41bc-4f9a-bb2f-57fadea0c36f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 33,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "71db5d83-52b1-43a1-a3d2-df2270f54d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 18,
                "y": 142
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d233bde2-d9a2-443b-a0a2-627ad3dba861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "292158b3-e0ba-4efe-8ae3-0a08e6524825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "89bbc45e-ed7c-41c4-a711-c8dc149f95f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 217,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e064aeea-aabb-4077-8c2d-a1a801950abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 202,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "630571bb-d822-4b11-8b9d-0c519a9bfb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 187,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c545ba28-8c0d-4bed-b613-4588718a2a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 181,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b90382c7-d1e1-40a2-a7df-a7d789cbf3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": -2,
                "shift": 7,
                "w": 7,
                "x": 172,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cb55121f-85e4-4c91-8b99-5f11a8fb0549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 157,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "229e7e00-27f5-4d3f-aca8-5b2040f3fb02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 144,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5b387be8-9e87-41dc-b3ed-1c5ca69a2f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 126,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cb1afdf3-37ec-4b37-b760-fca7872534fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 100,
                "y": 72
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "65f85131-14d1-482c-bf03-2a39ac35b093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 83,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ddf1c3b6-f71f-40d5-9660-1826836e181b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 70,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "94d3beae-ea41-4658-9de3-fad57387feb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0fe68c43-08f2-4219-b96a-2c6fde2e6411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 14,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b081274e-9690-4091-8f55-cb10dff629df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "75fb6f37-00ae-4841-b193-5b6f2e757f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a1b2ca91-6b80-47d2-b7e3-1a28c3d61f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5f31bfd7-1d35-4786-ac53-4865dd8e3c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "04b34c48-d354-47c8-b995-f029c43ed1d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d37ce02d-fa63-41c8-9898-80ef5a5300b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b8a9824a-dfde-40e8-a20e-6ca9642809d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f1b56f2d-17d9-428a-8a7d-009767b50be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d2a3a5c7-135c-480e-b20a-5d710547476d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ade57c3a-616d-4a6d-9486-06ba70a2fb64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0b0021de-a273-4c5c-a1fe-b6e7c9c18a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "af187d38-18fa-4559-80f5-2cef118e0feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "50f6f3a0-9760-4179-9952-fef951d70a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7f91a062-e383-4e8f-bd66-bd19319b943b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3df60c75-a14b-4ff7-b98b-7dd8d7957b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4c70816c-5050-4eff-a9ec-974af239538c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "78a5868f-d595-4f7a-9b08-8bc2002f521d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8a395d00-1bbc-44ee-ae22-93b35329845d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2536f0bd-dbbb-49bc-828c-c29b36e0f9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3c86dfdb-3251-4de3-9078-db7cf2cb88d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4f987fbd-10f8-4b6f-a4cc-90a20f54a25e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 55,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "47cf75c4-57e8-492b-b1fd-3eade7846e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 179,
                "y": 37
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3adc5fd3-73db-476e-ada6-455335648776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 68,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8b85f262-19fa-4eda-8889-373158fe075a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b1ec7bdc-7095-4ee8-ad1d-b16370756390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bc23cc2d-505b-4065-b27d-911b10742fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 32,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5917ef4f-c38d-4d25-8d84-7a5642073ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 14,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a202c142-9a7e-4472-af48-76afa2de92cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a59267b5-8555-4113-8305-9b06f5f8204b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 236,
                "y": 37
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6b40c3e2-1a4f-47ba-a150-8c20f8194439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 223,
                "y": 37
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8945a917-a91b-4386-b442-15def0764112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 37
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2ea3a633-ae15-40bf-ab87-fddf782253a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 201,
                "y": 37
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "085928e3-63a1-453a-88f2-dd4c23b01420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 59,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "424a0701-8134-4677-bf4f-6d511d885b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 37
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "cfbb66f3-5320-496f-af7b-7128255859bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 166,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b1475f5e-0f86-4f29-88d5-6a5510091e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 153,
                "y": 37
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ddc34651-d64c-481d-8fe2-7ad52bf4c0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 134,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "35a1b0e4-b8ec-4a61-b030-e7bf5281b026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 119,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "78bfcdcc-dccc-4d4c-882c-37685086eeac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 105,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ed04915d-52d6-40f0-9425-4e72e47f8a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 93,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "711f4f1e-dfaf-4029-96a6-79ce76c2da3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "febc46f8-9432-4291-95d2-c91af50ef0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 81,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "eede43b7-e9c9-483b-ae16-69e836d726b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 73,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7e85d83a-d4d5-4a3b-8a10-c669975737ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 163,
                "y": 142
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "548b3b61-c623-4b9f-918f-0c9ff5fc67ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 177,
                "y": 142
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}