{
    "id": "0c917902-a5ff-427c-bab9-aa3ecab2a5be",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_textNotification",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Sans Unicode",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8304c9a4-f9d7-412f-bb75-6a3f27711feb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "19390b5c-0a0b-4b16-8b7a-b45515a15e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 52,
                "y": 101
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d89d6212-f3ee-45f3-9ce3-ec3811a32d99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 44,
                "y": 101
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bba9bf88-5f08-4f8a-95fe-28cad54203b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 29,
                "y": 101
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a3ab1196-b998-4795-9d03-a8424037df0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 18,
                "y": 101
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fae0adcb-42a3-45f1-962c-e33411ad3fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f6b56dc9-3606-4634-a7c5-c724ac3921a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 68
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "95b9fd42-1f60-4611-8d39-dc0d2ed9f13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 229,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "36c0a630-11b0-41fc-8631-29bff72b4795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 222,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2d309fb1-4e91-4555-8029-89ed2f0cd425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 214,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d3409752-6c47-416a-a629-24c0508b493a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 57,
                "y": 101
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f68016d1-460e-4fcb-a708-2428b989bf95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 200,
                "y": 68
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5b6e44fd-6a05-445a-92be-a1dbf69c37bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 183,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a64ba1b7-b99c-4a31-aef5-4a0c1db6826c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 171,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "718e22fc-4fe2-4389-ac6a-9caa775e4187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 165,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0e6fb9cf-ac4e-47ec-9a13-f1db8951a94f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 156,
                "y": 68
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0e352d0e-d4e1-49f5-bf45-085bfb54eb5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d9b63ecc-a113-44ce-b83e-d76665cdddc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 13,
                "w": 6,
                "x": 135,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5830f195-0eb6-46d6-bd7c-7ba13fd0ea2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 123,
                "y": 68
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a41e6994-0071-4d10-87f2-3b2c9f9f1eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 112,
                "y": 68
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "83956721-6fe9-4903-a2ec-1deaafe0a6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 99,
                "y": 68
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6a921fb6-2f5a-4093-9554-0c16572df5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 189,
                "y": 68
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2bc3309d-effe-4071-b843-f03868e739bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 67,
                "y": 101
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "44819735-585d-406a-852a-0e1ded0d577a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 80,
                "y": 101
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4c66e728-cfc5-4e17-988c-5c0b89075f9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 92,
                "y": 101
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "01a111db-c917-493d-871e-3dc9436dc23c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 100,
                "y": 134
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b22c58e4-7e42-4412-9c31-e33030b737bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 95,
                "y": 134
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ac1ef190-390a-4357-baea-9b59831add6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 90,
                "y": 134
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e7ea5437-d2ca-40a1-96f7-1afce39972d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 76,
                "y": 134
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "57508da0-1012-4e06-bcb1-520b117d268d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 62,
                "y": 134
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8653c163-366f-42b4-baa2-634b080b53ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 48,
                "y": 134
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "89427a0f-706c-4cc4-a66c-65626814ff5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 37,
                "y": 134
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "606bbc41-0791-4f79-bcec-bfbeed5a584a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 18,
                "y": 134
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "75c8a3a3-8a92-427f-af47-803f48d722b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "aab7ff55-972c-45af-9d58-781a63b1fd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 101
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "44a02dd3-2172-4c84-8070-d7874c8e2511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 228,
                "y": 101
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0e8090ed-42c4-40f0-8396-69b2534f683c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 213,
                "y": 101
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bf7b79b6-b97f-4120-afdc-de7345ae18fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 101
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3cf5fe87-d42f-43af-b5ef-3a3eb9659e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 190,
                "y": 101
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1f412e20-7017-4a43-b2a7-1661b51559aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 176,
                "y": 101
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "21aefff9-f60c-47ed-9b21-0c8eaa6c84cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 162,
                "y": 101
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2276fd80-6fa6-42d4-bc4f-5eacfae038fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 157,
                "y": 101
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a60a6d42-a46f-4bba-a256-f419c6de8906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 148,
                "y": 101
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e9ac6c06-7f06-4ba4-8481-3c252fe527ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 134,
                "y": 101
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "481cc011-72c1-4aac-a6da-6ef43c9ac28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 101
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f3acd9a1-5f85-4f6f-ace0-03986e96195b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 105,
                "y": 101
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bf2367b2-762d-48bf-b034-0d19ccc18160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 85,
                "y": 68
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "73045cc0-d156-474c-9d82-e7d8fda2f305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 69,
                "y": 68
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "96a4d751-4990-4a97-9c2f-74fcbe76e46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 57,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "691cdaf1-7963-4efe-a0fe-174415ebce8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6a889e68-3590-436c-bbe1-e8d2a59f1192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8159be85-3018-4b8c-8816-5369e6a877ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "88a5a04c-4d39-47ec-a69a-53a2b10f7039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "38fdfdd3-2146-49bd-b517-306adce586de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "300f4261-0935-4385-9e1d-5423ca8eaa7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4909c774-d791-4ae4-997c-c672c62aa06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "02f3e562-e571-47c8-9d36-e96dcb5e5261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b5511f4c-0d08-4c63-864e-5da86079b3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9f6d765d-1090-4c55-8abb-a0eab09b88be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "35ced50f-47fc-4bef-9a27-d730e6a9cc1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c4cfe940-f3dc-4888-98c5-c5da1134ff08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "87d6aec8-1163-4c40-8d89-e190bb4bd95d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6273550a-47bf-4917-a961-73230a042c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fb03cfe5-3736-4b0c-a00f-bbb05d3c398a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "31344354-1941-42fa-9d9f-782d3b26c123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "96a3f06a-daed-4145-a54e-ef598488b7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b966073d-b656-4b3e-9910-d1f1861ca7d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ffed4215-cdd6-408f-9d2b-6fc7f194e134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d0a989f5-8bdf-4fc5-9803-2d513b8182ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "eae6fb85-09fd-4e28-b3b8-d46417f8be6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "21f363e9-9471-4c5e-8a72-006a983f6df2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8cd0bbe3-f785-4515-9ff9-b77949ba9cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 40,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f3b8ddd0-98a6-44b8-97e0-754e9078a3be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 159,
                "y": 35
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e7c05f2e-db69-4b86-b281-0bad2866fa3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 52,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b2183233-4f0e-499d-8951-081b7c542ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c91d25b8-da1d-4120-9d7c-2feaebb9c5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 25,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "25c0a664-f5e5-4887-8200-897ad65e2e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 20,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f8d47f7e-7f71-452b-abfd-c3ce0a4e6286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "76053fc8-d2f2-4008-aead-f99d15532e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 227,
                "y": 35
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0dcf5f88-85fe-4130-850e-ded3c887e04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 214,
                "y": 35
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7c4c07fa-90c1-4b07-b174-50f0ba326cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 35
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "af726fee-df80-4d5a-ab1f-93e88df35fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 189,
                "y": 35
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ce3908f0-cc2e-4cb1-be92-242c9835c721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 180,
                "y": 35
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "eb8121c4-7cbc-4400-b1a1-7724ffb94d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 47,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e6d38597-8318-403a-ada8-f864a8ee0954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 171,
                "y": 35
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aff7b373-ebaf-4ccd-86f3-b46c2ec574cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 147,
                "y": 35
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1d9dffd2-895e-446d-a486-6679fe79ddb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 134,
                "y": 35
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e38c3ab7-d5cd-4610-b87e-eead70981502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 116,
                "y": 35
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "39f1cbd4-e8db-46ce-8b76-6fe8997a0889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 102,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "893c7755-a2bb-4b96-9276-08fc921de5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 89,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4fbe88c6-bfdd-4560-adc1-4e7ad0861676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 77,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "886894a3-e45b-40cc-92d2-ded2ede808df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 69,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7701eceb-ca33-4c30-988f-36928f791023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 65,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1928af2f-51ee-4df8-99af-3379832d5fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 57,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "addd553e-8b81-481f-a1d8-de787d758b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 113,
                "y": 134
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c0a3613f-576e-4444-8803-37228c2a5075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 126,
                "y": 134
            }
        }
    ],
    "hinting": 2,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}