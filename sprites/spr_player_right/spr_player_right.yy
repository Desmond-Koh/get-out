{
    "id": "542578ad-a254-4ba7-afec-09609ed6cf0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 21,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2224f3b8-219c-4912-b5a0-09b461a387ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542578ad-a254-4ba7-afec-09609ed6cf0a",
            "compositeImage": {
                "id": "978619f3-bffd-4384-9fe8-1ad331c2a757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2224f3b8-219c-4912-b5a0-09b461a387ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06db9c91-ffa1-406c-8057-3f3d9b157199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2224f3b8-219c-4912-b5a0-09b461a387ac",
                    "LayerId": "b5431eea-8d45-47af-b307-906754134bf9"
                }
            ]
        },
        {
            "id": "68333ee1-ed2a-4b8e-9013-b0749bfc5cd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542578ad-a254-4ba7-afec-09609ed6cf0a",
            "compositeImage": {
                "id": "664782df-0f02-4d16-a4f2-3e29ba22256e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68333ee1-ed2a-4b8e-9013-b0749bfc5cd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67068914-f317-445c-a861-73faaed98592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68333ee1-ed2a-4b8e-9013-b0749bfc5cd5",
                    "LayerId": "b5431eea-8d45-47af-b307-906754134bf9"
                }
            ]
        },
        {
            "id": "ce2af195-5e4f-4ca7-8fb7-fdaba1fae48f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542578ad-a254-4ba7-afec-09609ed6cf0a",
            "compositeImage": {
                "id": "575f7be7-68fd-4d2a-b0b8-81f4164e4e16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce2af195-5e4f-4ca7-8fb7-fdaba1fae48f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8baab12-6470-46df-83ff-917b24d48b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce2af195-5e4f-4ca7-8fb7-fdaba1fae48f",
                    "LayerId": "b5431eea-8d45-47af-b307-906754134bf9"
                }
            ]
        },
        {
            "id": "4fb02c78-34fe-465d-9d5c-960bf1a7460e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542578ad-a254-4ba7-afec-09609ed6cf0a",
            "compositeImage": {
                "id": "a1713ca6-8f28-4f7f-bf65-2e54bada743a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb02c78-34fe-465d-9d5c-960bf1a7460e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dbd7afd-640b-4ee0-9aa9-7aaa2d31dc75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb02c78-34fe-465d-9d5c-960bf1a7460e",
                    "LayerId": "b5431eea-8d45-47af-b307-906754134bf9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "b5431eea-8d45-47af-b307-906754134bf9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "542578ad-a254-4ba7-afec-09609ed6cf0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}