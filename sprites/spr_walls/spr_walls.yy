{
    "id": "e3efacda-b49a-4344-ac58-0c9d6275c556",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e5c023e-6374-4e61-8053-3a5ad13c5a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3efacda-b49a-4344-ac58-0c9d6275c556",
            "compositeImage": {
                "id": "d3222d78-9a8e-4668-99a3-f6a40e706de8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e5c023e-6374-4e61-8053-3a5ad13c5a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29711aba-6db2-46f8-96af-f7741080ef4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e5c023e-6374-4e61-8053-3a5ad13c5a7d",
                    "LayerId": "94ed1367-ecca-4132-a01b-340bc5894f32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "94ed1367-ecca-4132-a01b-340bc5894f32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3efacda-b49a-4344-ac58-0c9d6275c556",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}