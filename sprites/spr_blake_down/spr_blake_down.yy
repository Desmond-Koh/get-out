{
    "id": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blake_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c19c848f-a9fb-4f32-8d58-89aaa1352ada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
            "compositeImage": {
                "id": "8ade89b6-557f-4522-ab5f-c9981038d8a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19c848f-a9fb-4f32-8d58-89aaa1352ada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "948b8331-c509-4a2a-9db5-6d28521f9eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19c848f-a9fb-4f32-8d58-89aaa1352ada",
                    "LayerId": "944865d2-25d3-4697-b71a-374404a21ac2"
                }
            ]
        },
        {
            "id": "9560ede4-bcf3-49f7-9f5d-2bfc90e3df95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
            "compositeImage": {
                "id": "b2bb8683-b6fc-4960-b66a-8082734bf938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9560ede4-bcf3-49f7-9f5d-2bfc90e3df95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98b58a8-c8d1-4ea2-80d9-703311805817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9560ede4-bcf3-49f7-9f5d-2bfc90e3df95",
                    "LayerId": "944865d2-25d3-4697-b71a-374404a21ac2"
                }
            ]
        },
        {
            "id": "7139ff23-4936-4dd8-90d5-ee456c82df0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
            "compositeImage": {
                "id": "46f96930-0692-4f4e-bc98-4266902d88b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7139ff23-4936-4dd8-90d5-ee456c82df0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0c6145-b34c-4669-b56a-187b77ddd0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7139ff23-4936-4dd8-90d5-ee456c82df0a",
                    "LayerId": "944865d2-25d3-4697-b71a-374404a21ac2"
                }
            ]
        },
        {
            "id": "ffb8cc1c-6795-4d3d-83d0-4667c984568a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
            "compositeImage": {
                "id": "7cebc65f-c3c3-4ae1-bec3-a9dc4a0a9e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb8cc1c-6795-4d3d-83d0-4667c984568a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96b48ed-2f72-4746-821d-186a513459c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb8cc1c-6795-4d3d-83d0-4667c984568a",
                    "LayerId": "944865d2-25d3-4697-b71a-374404a21ac2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "944865d2-25d3-4697-b71a-374404a21ac2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}