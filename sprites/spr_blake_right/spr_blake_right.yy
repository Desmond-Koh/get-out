{
    "id": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blake_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72d6f511-8c6d-42ab-8f68-06ec02a219cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
            "compositeImage": {
                "id": "eb9f6fbd-42cc-4cba-9afd-f5db536c1bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72d6f511-8c6d-42ab-8f68-06ec02a219cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0af12294-14ea-41a9-bf79-4356d9367be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72d6f511-8c6d-42ab-8f68-06ec02a219cd",
                    "LayerId": "b884b88c-462e-4b27-a494-6928aba33e07"
                }
            ]
        },
        {
            "id": "0094fbfe-8a0f-4f04-9016-99251a64fff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
            "compositeImage": {
                "id": "5f5fe0f5-50ef-406b-96d9-13875fdd5ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0094fbfe-8a0f-4f04-9016-99251a64fff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88adffd3-ccad-4b48-94d4-01edebfcbc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0094fbfe-8a0f-4f04-9016-99251a64fff4",
                    "LayerId": "b884b88c-462e-4b27-a494-6928aba33e07"
                }
            ]
        },
        {
            "id": "9cd30477-5f58-45f4-ba9a-ddbc884db3a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
            "compositeImage": {
                "id": "cd54a323-91a7-4dd1-91f5-5cea5e28ae7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd30477-5f58-45f4-ba9a-ddbc884db3a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64bbfe37-1f27-48e1-b11b-57f75e5f23fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd30477-5f58-45f4-ba9a-ddbc884db3a7",
                    "LayerId": "b884b88c-462e-4b27-a494-6928aba33e07"
                }
            ]
        },
        {
            "id": "a3f9b6f8-bc2f-401a-a696-17036fd7a817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
            "compositeImage": {
                "id": "d9cedfa5-006e-4f49-9256-0c45138adf08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f9b6f8-bc2f-401a-a696-17036fd7a817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2028e87-bfa5-4206-899a-2acdc64b1be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f9b6f8-bc2f-401a-a696-17036fd7a817",
                    "LayerId": "b884b88c-462e-4b27-a494-6928aba33e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "b884b88c-462e-4b27-a494-6928aba33e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb3a5ba-399b-4d7c-8de7-fbb9aaf620ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}