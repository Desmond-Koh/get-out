{
    "id": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1fc60b9-fe4b-42f3-ab57-8968cb83ba8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
            "compositeImage": {
                "id": "30abd5d8-05bd-4362-9834-7583aba0f963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fc60b9-fe4b-42f3-ab57-8968cb83ba8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eebc65e-1f95-4fd5-b07d-954cfcbb73c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fc60b9-fe4b-42f3-ab57-8968cb83ba8d",
                    "LayerId": "f69978b1-f64f-4524-a7e0-17ec26627a49"
                }
            ]
        },
        {
            "id": "72232350-73c2-4dcf-ab08-c99f9fec978b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
            "compositeImage": {
                "id": "68f5b17a-966f-4fcc-aea5-5822545ad2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72232350-73c2-4dcf-ab08-c99f9fec978b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9d7560d-3118-4be7-a431-12165f615846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72232350-73c2-4dcf-ab08-c99f9fec978b",
                    "LayerId": "f69978b1-f64f-4524-a7e0-17ec26627a49"
                }
            ]
        },
        {
            "id": "beef0dc0-1241-4276-a1ff-6b5b3985d06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
            "compositeImage": {
                "id": "e3819daf-d6c9-4533-9c6e-3bd226c9a8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beef0dc0-1241-4276-a1ff-6b5b3985d06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888c3f9a-a3f8-4842-9fc9-d96b70ded647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beef0dc0-1241-4276-a1ff-6b5b3985d06c",
                    "LayerId": "f69978b1-f64f-4524-a7e0-17ec26627a49"
                }
            ]
        },
        {
            "id": "4acba962-6a13-420b-9973-9f84d22471ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
            "compositeImage": {
                "id": "bb648811-1149-41ba-9529-d1ab8a9215fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4acba962-6a13-420b-9973-9f84d22471ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826f0cdc-c937-4cc8-81ac-919e64dd3b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4acba962-6a13-420b-9973-9f84d22471ef",
                    "LayerId": "f69978b1-f64f-4524-a7e0-17ec26627a49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "f69978b1-f64f-4524-a7e0-17ec26627a49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}