{
    "id": "668ebee7-6039-4069-91b5-be7fe781ca45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ba81df4-ce51-4fa4-abd4-d9c71842b3a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "668ebee7-6039-4069-91b5-be7fe781ca45",
            "compositeImage": {
                "id": "854f8aba-fd09-4829-a2d9-785436c9fda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba81df4-ce51-4fa4-abd4-d9c71842b3a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d84944-815f-4e9a-b0da-2d2e85dd2ddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba81df4-ce51-4fa4-abd4-d9c71842b3a9",
                    "LayerId": "2dd9c3b0-7c76-4140-a269-e2c17ea8a0d2"
                }
            ]
        },
        {
            "id": "2447e1bd-fa2d-4ef7-b6eb-d5b0ab5ce77c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "668ebee7-6039-4069-91b5-be7fe781ca45",
            "compositeImage": {
                "id": "2990c020-a892-4740-b4a8-4df0ae832418",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2447e1bd-fa2d-4ef7-b6eb-d5b0ab5ce77c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b4344d-d5db-458f-b1a4-24aa939cda32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2447e1bd-fa2d-4ef7-b6eb-d5b0ab5ce77c",
                    "LayerId": "2dd9c3b0-7c76-4140-a269-e2c17ea8a0d2"
                }
            ]
        },
        {
            "id": "383c695d-bf3d-400a-9e89-4314a4976951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "668ebee7-6039-4069-91b5-be7fe781ca45",
            "compositeImage": {
                "id": "5c4295de-12d5-42e1-b2d3-4c9131ad1eb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383c695d-bf3d-400a-9e89-4314a4976951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea9dea7-1ccd-4e54-86fa-002d9e6ccf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383c695d-bf3d-400a-9e89-4314a4976951",
                    "LayerId": "2dd9c3b0-7c76-4140-a269-e2c17ea8a0d2"
                }
            ]
        },
        {
            "id": "32b35e05-ad86-4e11-a099-2ebc6cf8cd65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "668ebee7-6039-4069-91b5-be7fe781ca45",
            "compositeImage": {
                "id": "655ed2a5-3277-44ad-86a2-bbf737972fbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b35e05-ad86-4e11-a099-2ebc6cf8cd65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1eb4da4-b4d7-4f4d-9f6e-4e790f43c92d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b35e05-ad86-4e11-a099-2ebc6cf8cd65",
                    "LayerId": "2dd9c3b0-7c76-4140-a269-e2c17ea8a0d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "2dd9c3b0-7c76-4140-a269-e2c17ea8a0d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "668ebee7-6039-4069-91b5-be7fe781ca45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}