{
    "id": "4d754d2d-a4df-42ca-a10c-b67e912fd44f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_staminabarOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 8,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50a6dd36-aceb-40e4-9685-74043ccfc364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d754d2d-a4df-42ca-a10c-b67e912fd44f",
            "compositeImage": {
                "id": "25d20082-5d7d-4945-8354-f107ad5948d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a6dd36-aceb-40e4-9685-74043ccfc364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3918d95-b4e7-45e9-a7d8-288d2afbe7f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a6dd36-aceb-40e4-9685-74043ccfc364",
                    "LayerId": "eefe223a-3b3c-4282-9c65-136215c25ed0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "eefe223a-3b3c-4282-9c65-136215c25ed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d754d2d-a4df-42ca-a10c-b67e912fd44f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 179,
    "yorig": -10
}