{
    "id": "2c01ac20-90bb-43b9-a263-c28c0722bc77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite77",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcde4cab-24a2-4d11-b348-3f2689a3a0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c01ac20-90bb-43b9-a263-c28c0722bc77",
            "compositeImage": {
                "id": "a1b79265-661e-43e2-a702-d4a0212a67ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcde4cab-24a2-4d11-b348-3f2689a3a0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca073322-fa10-429a-bd22-e3ce478e3e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcde4cab-24a2-4d11-b348-3f2689a3a0b9",
                    "LayerId": "25316fca-34e7-4dff-812a-7fb1a9d04a8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "25316fca-34e7-4dff-812a-7fb1a9d04a8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c01ac20-90bb-43b9-a263-c28c0722bc77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}