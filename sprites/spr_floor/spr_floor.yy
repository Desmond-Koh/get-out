{
    "id": "833fe95d-4551-4e33-8b80-be79beced7f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 575,
    "bbox_left": 0,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "871aef07-e67d-4b0d-84e0-01ac606fa97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "833fe95d-4551-4e33-8b80-be79beced7f0",
            "compositeImage": {
                "id": "4fc10d7e-9d34-4a5d-b762-dbaff0da08bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "871aef07-e67d-4b0d-84e0-01ac606fa97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38c63795-7051-4704-b124-689a98f252e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "871aef07-e67d-4b0d-84e0-01ac606fa97d",
                    "LayerId": "bf10fc46-552b-4339-acfd-92a6fb0f3530"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "bf10fc46-552b-4339-acfd-92a6fb0f3530",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "833fe95d-4551-4e33-8b80-be79beced7f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}