{
    "id": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a732026-17ae-4643-8f05-b5091a598b2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
            "compositeImage": {
                "id": "63d7ecc0-8e9b-43ff-a077-5ba208b7d924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a732026-17ae-4643-8f05-b5091a598b2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a8a92a-c5a4-467a-a20a-8655bac27619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a732026-17ae-4643-8f05-b5091a598b2d",
                    "LayerId": "20bb749c-b3c4-479a-a25d-8d2a40ae144c"
                }
            ]
        },
        {
            "id": "84511824-32d4-4b75-90d6-5b7ea9fa3099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
            "compositeImage": {
                "id": "f5448685-094c-4e14-82ec-9689cd7e4e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84511824-32d4-4b75-90d6-5b7ea9fa3099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9708bec-138c-436b-9f08-17bff4f55cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84511824-32d4-4b75-90d6-5b7ea9fa3099",
                    "LayerId": "20bb749c-b3c4-479a-a25d-8d2a40ae144c"
                }
            ]
        },
        {
            "id": "f49f22d3-d2c8-4420-b460-84de9ba26352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
            "compositeImage": {
                "id": "f785f4c9-a430-4a9c-a928-e8437af2c3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49f22d3-d2c8-4420-b460-84de9ba26352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5779fa-03c5-4e4a-9405-0ac5ecbd3289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49f22d3-d2c8-4420-b460-84de9ba26352",
                    "LayerId": "20bb749c-b3c4-479a-a25d-8d2a40ae144c"
                }
            ]
        },
        {
            "id": "2695ddbf-8f9e-492c-bb12-0182e66dba1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
            "compositeImage": {
                "id": "1b8bc868-3a6d-4719-a855-da9046f430d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2695ddbf-8f9e-492c-bb12-0182e66dba1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfac3b7b-3b1e-4bfc-b8d5-9ca2c47f9838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2695ddbf-8f9e-492c-bb12-0182e66dba1b",
                    "LayerId": "20bb749c-b3c4-479a-a25d-8d2a40ae144c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "20bb749c-b3c4-479a-a25d-8d2a40ae144c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "faadb5ef-0047-498e-9659-c67f54b2e3ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}