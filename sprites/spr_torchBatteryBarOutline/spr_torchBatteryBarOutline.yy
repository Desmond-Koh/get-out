{
    "id": "5e9d8ca7-f20b-46ff-bad2-10f017ac7726",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torchBatteryBarOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 14,
    "bbox_right": 204,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c596757e-ddb8-49d4-9733-ecd132fa5229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9d8ca7-f20b-46ff-bad2-10f017ac7726",
            "compositeImage": {
                "id": "dce0ae9b-909f-4c6e-8ad8-166d679e3e6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c596757e-ddb8-49d4-9733-ecd132fa5229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d538d7a-0a4d-4c4c-a328-e5562e8dad27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c596757e-ddb8-49d4-9733-ecd132fa5229",
                    "LayerId": "d4fb53d6-4806-4485-9395-b744a1575c17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "d4fb53d6-4806-4485-9395-b744a1575c17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e9d8ca7-f20b-46ff-bad2-10f017ac7726",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 205,
    "xorig": 0,
    "yorig": 0
}