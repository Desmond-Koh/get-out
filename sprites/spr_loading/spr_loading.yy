{
    "id": "ef8f9f25-b478-4a70-a8e7-782bffe3d645",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_loading",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f02acdd3-27b1-4fc8-a7dc-4c64e176436f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef8f9f25-b478-4a70-a8e7-782bffe3d645",
            "compositeImage": {
                "id": "fab5d36d-07af-4bb6-95e2-541c00b4a8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f02acdd3-27b1-4fc8-a7dc-4c64e176436f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfdbfe40-c25a-474b-9f85-038f27f0ae57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f02acdd3-27b1-4fc8-a7dc-4c64e176436f",
                    "LayerId": "c05ee1c3-931b-4de0-96ee-97202e4a837f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "c05ee1c3-931b-4de0-96ee-97202e4a837f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef8f9f25-b478-4a70-a8e7-782bffe3d645",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 270
}