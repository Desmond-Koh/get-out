{
    "id": "56b05f7e-f428-440e-a25c-ae4a0a17743b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backbatteryR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4c2570a-6c1a-4f3e-9064-ae9a80134c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56b05f7e-f428-440e-a25c-ae4a0a17743b",
            "compositeImage": {
                "id": "077823fb-2357-42f6-89be-437d0e6ee950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c2570a-6c1a-4f3e-9064-ae9a80134c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67daa024-e47d-4c9e-96a1-f7c8415ece3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c2570a-6c1a-4f3e-9064-ae9a80134c58",
                    "LayerId": "4e15493a-6daa-4b67-a83d-1bf3b2adcc75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "4e15493a-6daa-4b67-a83d-1bf3b2adcc75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56b05f7e-f428-440e-a25c-ae4a0a17743b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}