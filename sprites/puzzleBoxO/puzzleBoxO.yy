{
    "id": "adb41484-abed-4298-ae41-d4148d834726",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "puzzleBoxO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59d1b422-1465-4c74-9831-c38a216c2110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb41484-abed-4298-ae41-d4148d834726",
            "compositeImage": {
                "id": "de476532-d452-4f2f-a411-3a27912203cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d1b422-1465-4c74-9831-c38a216c2110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "891fa4b0-14dc-4185-9ee8-f7effca43b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d1b422-1465-4c74-9831-c38a216c2110",
                    "LayerId": "7733d01f-dea1-4d36-8c4e-cbe78cfb2d05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7733d01f-dea1-4d36-8c4e-cbe78cfb2d05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb41484-abed-4298-ae41-d4148d834726",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}