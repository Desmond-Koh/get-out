{
    "id": "816c29b8-7c25-4828-9499-fa792bae87e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialoguebox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 3,
    "bbox_right": 519,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9252e188-e251-4b2c-8e30-59eb58a29b87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "816c29b8-7c25-4828-9499-fa792bae87e9",
            "compositeImage": {
                "id": "b20a81c5-7e21-44c3-a43b-a431be06f00d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9252e188-e251-4b2c-8e30-59eb58a29b87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2521a471-4f53-4e55-93bb-8d502fc33540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9252e188-e251-4b2c-8e30-59eb58a29b87",
                    "LayerId": "8163b27e-11ba-4762-ad3c-f95d11dd8558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 135,
    "layers": [
        {
            "id": "8163b27e-11ba-4762-ad3c-f95d11dd8558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "816c29b8-7c25-4828-9499-fa792bae87e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 520,
    "xorig": 0,
    "yorig": 0
}