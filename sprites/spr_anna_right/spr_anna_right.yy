{
    "id": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anna_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e191180-c9d4-4cb4-8889-2929220af119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
            "compositeImage": {
                "id": "a95a049e-f43b-46a5-8b31-f334d9ee3e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e191180-c9d4-4cb4-8889-2929220af119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9347d65-8b1d-42bb-97f4-4f04a4e9bf22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e191180-c9d4-4cb4-8889-2929220af119",
                    "LayerId": "0d82b80e-b82d-4476-a6e1-b516772b7385"
                }
            ]
        },
        {
            "id": "b629b693-1d0f-4fec-a21f-69f125274d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
            "compositeImage": {
                "id": "def57f7f-fb08-4eaf-986a-fbed8f111feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b629b693-1d0f-4fec-a21f-69f125274d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb5bf39-40dd-4966-b694-cde98edc8b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b629b693-1d0f-4fec-a21f-69f125274d12",
                    "LayerId": "0d82b80e-b82d-4476-a6e1-b516772b7385"
                }
            ]
        },
        {
            "id": "2010f2b1-a2ef-4b2f-a102-c40a50b84d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
            "compositeImage": {
                "id": "e4762478-2583-4633-b868-c5e33b6f7116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2010f2b1-a2ef-4b2f-a102-c40a50b84d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1884de0-3d17-4e4f-b331-32c97ca6d2bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2010f2b1-a2ef-4b2f-a102-c40a50b84d90",
                    "LayerId": "0d82b80e-b82d-4476-a6e1-b516772b7385"
                }
            ]
        },
        {
            "id": "ef0dc61e-ddd8-4b9c-bcb1-b82022a12da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
            "compositeImage": {
                "id": "750f0b07-6463-4c93-a461-74951dc860a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0dc61e-ddd8-4b9c-bcb1-b82022a12da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a9b92ac-a3f7-4395-bcb1-f01bea66e8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0dc61e-ddd8-4b9c-bcb1-b82022a12da1",
                    "LayerId": "0d82b80e-b82d-4476-a6e1-b516772b7385"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "0d82b80e-b82d-4476-a6e1-b516772b7385",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bde2b66c-5f2b-4256-8f41-3580a7a126ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}