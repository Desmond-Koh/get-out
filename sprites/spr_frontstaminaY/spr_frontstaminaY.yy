{
    "id": "9d05c8b0-26f5-4acf-b895-2e4db252a9bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontstaminaY",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4ebe704-ec8d-40c0-8e27-bb5100061e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d05c8b0-26f5-4acf-b895-2e4db252a9bf",
            "compositeImage": {
                "id": "ed1d04e9-89fc-45ca-8bef-4d2dfa74e1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ebe704-ec8d-40c0-8e27-bb5100061e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff1ef73-2d75-425e-8353-c867ac3fd185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ebe704-ec8d-40c0-8e27-bb5100061e0a",
                    "LayerId": "edb726a6-b5c5-437a-b1d4-bb0eac81249a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "edb726a6-b5c5-437a-b1d4-bb0eac81249a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d05c8b0-26f5-4acf-b895-2e4db252a9bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}