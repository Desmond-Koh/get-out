{
    "id": "829c6f8b-d340-4352-8ad9-6df00e8c087f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backbatteryY",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2eb0b88-7aff-4789-b942-5d3afbcdc553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "829c6f8b-d340-4352-8ad9-6df00e8c087f",
            "compositeImage": {
                "id": "c31ac119-99d1-421f-95fc-02def885aaf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2eb0b88-7aff-4789-b942-5d3afbcdc553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2d6f7d-4e19-44f4-85c0-bbf9e8974060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2eb0b88-7aff-4789-b942-5d3afbcdc553",
                    "LayerId": "5f6a99ec-9836-464d-bfd1-14b3774bb26a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "5f6a99ec-9836-464d-bfd1-14b3774bb26a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "829c6f8b-d340-4352-8ad9-6df00e8c087f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}