{
    "id": "1432f848-35d2-4dcf-8002-3c2f1d1fadf6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_book_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1919,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddef5241-cc75-4811-83f8-15c3ade911a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1432f848-35d2-4dcf-8002-3c2f1d1fadf6",
            "compositeImage": {
                "id": "034f02a5-69d5-4ab9-9587-bef50820b153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddef5241-cc75-4811-83f8-15c3ade911a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f0478e-6947-40bb-b5c4-af351c3d8664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddef5241-cc75-4811-83f8-15c3ade911a2",
                    "LayerId": "4c948d67-e9f4-429a-bd96-8a7a5f2907d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1920,
    "layers": [
        {
            "id": "4c948d67-e9f4-429a-bd96-8a7a5f2907d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1432f848-35d2-4dcf-8002-3c2f1d1fadf6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 0
}