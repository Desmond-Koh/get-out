{
    "id": "efb08810-3dee-49f6-9d06-9c8dae863c74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ghostEncounterDialogue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b23beb90-6028-4ee6-ba5f-419f633bee11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efb08810-3dee-49f6-9d06-9c8dae863c74",
            "compositeImage": {
                "id": "93375d3f-6b93-4a59-9ef0-0c7696b41f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b23beb90-6028-4ee6-ba5f-419f633bee11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59344c6b-1dcc-4db7-b1f6-5146a2c2dff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b23beb90-6028-4ee6-ba5f-419f633bee11",
                    "LayerId": "9f6ea0db-2516-426b-b4be-00a2a68aad50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f6ea0db-2516-426b-b4be-00a2a68aad50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efb08810-3dee-49f6-9d06-9c8dae863c74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}