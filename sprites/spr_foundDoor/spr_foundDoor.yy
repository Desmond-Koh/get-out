{
    "id": "6cb23b11-abc3-4b45-bc0d-b0ed49efd27e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_foundDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69171240-0649-4961-acc1-57704d94f7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cb23b11-abc3-4b45-bc0d-b0ed49efd27e",
            "compositeImage": {
                "id": "c02b739f-a94b-4eee-b61f-4aa86bd5d6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69171240-0649-4961-acc1-57704d94f7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74da44d-c720-47e7-8228-79ff033edbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69171240-0649-4961-acc1-57704d94f7d6",
                    "LayerId": "e9afc57d-92e0-41d6-982b-02197c7c7f81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e9afc57d-92e0-41d6-982b-02197c7c7f81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cb23b11-abc3-4b45-bc0d-b0ed49efd27e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}