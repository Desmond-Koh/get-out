{
    "id": "b4fa508b-3d25-416a-bfe9-f0c2907b9002",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unlockDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f4b629a-8136-407f-ab05-aef0a30af761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4fa508b-3d25-416a-bfe9-f0c2907b9002",
            "compositeImage": {
                "id": "09f94df9-9404-4400-96c9-cbc272b4ee5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f4b629a-8136-407f-ab05-aef0a30af761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c74b2994-79be-4fd0-a140-bc224523a08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f4b629a-8136-407f-ab05-aef0a30af761",
                    "LayerId": "ecf2fb66-f9c1-493a-a960-4185d4ccaa67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ecf2fb66-f9c1-493a-a960-4185d4ccaa67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4fa508b-3d25-416a-bfe9-f0c2907b9002",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}