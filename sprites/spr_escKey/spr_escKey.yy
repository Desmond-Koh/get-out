{
    "id": "7d57065e-dedc-4c94-a1ad-b4f82ae7b6ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_escKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7df79c0f-41de-4697-84c7-980ff1a9f5ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d57065e-dedc-4c94-a1ad-b4f82ae7b6ff",
            "compositeImage": {
                "id": "28c0ec0d-e534-45b9-a2e2-28d7664aaf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df79c0f-41de-4697-84c7-980ff1a9f5ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecdc086a-b344-460f-8417-d4fdcf1106fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df79c0f-41de-4697-84c7-980ff1a9f5ee",
                    "LayerId": "9ecee52e-2fc0-41bf-aaad-c651b43db2c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "9ecee52e-2fc0-41bf-aaad-c651b43db2c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d57065e-dedc-4c94-a1ad-b4f82ae7b6ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 270
}