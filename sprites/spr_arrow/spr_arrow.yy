{
    "id": "9e1d1e7f-e4ff-4709-9d69-32218fbd6356",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a266f4a8-ffe5-4335-ae19-66282518739a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e1d1e7f-e4ff-4709-9d69-32218fbd6356",
            "compositeImage": {
                "id": "6de56781-360a-4f42-a9a6-f55ce858e8b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a266f4a8-ffe5-4335-ae19-66282518739a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b52f0f-2092-418e-b00a-29d1d41a1cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a266f4a8-ffe5-4335-ae19-66282518739a",
                    "LayerId": "a79b151e-6212-4972-972e-49ecde35b067"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a79b151e-6212-4972-972e-49ecde35b067",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e1d1e7f-e4ff-4709-9d69-32218fbd6356",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 24
}