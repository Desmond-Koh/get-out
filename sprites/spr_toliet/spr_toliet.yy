{
    "id": "ccfb8b4a-a0e9-4736-8f34-1e15cc77ec54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_toliet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 467,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d93e383c-1f98-4322-917b-d5ddc75fd837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccfb8b4a-a0e9-4736-8f34-1e15cc77ec54",
            "compositeImage": {
                "id": "08f9cb57-8697-4538-919e-93578f1ffef3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93e383c-1f98-4322-917b-d5ddc75fd837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a87bc4d-7e0b-4d27-b165-35acb255f4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93e383c-1f98-4322-917b-d5ddc75fd837",
                    "LayerId": "0d56ccf4-0d0d-4e44-88fa-22919270945b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "0d56ccf4-0d0d-4e44-88fa-22919270945b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccfb8b4a-a0e9-4736-8f34-1e15cc77ec54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}