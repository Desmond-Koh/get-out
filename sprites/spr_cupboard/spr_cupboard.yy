{
    "id": "cd0cb952-c54f-4fbe-bcb2-bf8212d9f10f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 899,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cfb3d3c-9f0d-40d0-a29c-0111a4387006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd0cb952-c54f-4fbe-bcb2-bf8212d9f10f",
            "compositeImage": {
                "id": "cb25ec9a-f668-4f8c-8c66-45c41d3bf104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cfb3d3c-9f0d-40d0-a29c-0111a4387006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0732851-c29d-45f1-afae-00ea29c35c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cfb3d3c-9f0d-40d0-a29c-0111a4387006",
                    "LayerId": "4da383ec-5226-4220-bc83-3e9bfa4e45fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "4da383ec-5226-4220-bc83-3e9bfa4e45fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd0cb952-c54f-4fbe-bcb2-bf8212d9f10f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 250
}