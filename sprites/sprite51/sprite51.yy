{
    "id": "1e63ddcd-8b0a-4747-82f7-6dab0f4085ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite51",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51d4dafa-8115-490d-a5ef-826c50643c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e63ddcd-8b0a-4747-82f7-6dab0f4085ba",
            "compositeImage": {
                "id": "cae6569b-061a-438d-ad8a-45f903794332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d4dafa-8115-490d-a5ef-826c50643c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afb906e1-d18b-483b-a5e9-7c1caec3279f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d4dafa-8115-490d-a5ef-826c50643c8e",
                    "LayerId": "a4f74073-e3ba-4a9a-aa91-fcedb84adac7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4f74073-e3ba-4a9a-aa91-fcedb84adac7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e63ddcd-8b0a-4747-82f7-6dab0f4085ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}