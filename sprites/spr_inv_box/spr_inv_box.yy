{
    "id": "07a19a40-bbd2-43eb-8d21-edc8c34fd399",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dad01abe-c863-4648-ba50-83c0a54fe52c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a19a40-bbd2-43eb-8d21-edc8c34fd399",
            "compositeImage": {
                "id": "a060beab-3c93-43d3-b3cf-003a2c5695a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad01abe-c863-4648-ba50-83c0a54fe52c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62bdae0a-8741-423d-ac86-d25056eb8796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad01abe-c863-4648-ba50-83c0a54fe52c",
                    "LayerId": "b3484002-7527-4047-9ae4-952164799300"
                }
            ]
        },
        {
            "id": "e6f88c54-c55b-4566-8153-7d40dec0bcb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a19a40-bbd2-43eb-8d21-edc8c34fd399",
            "compositeImage": {
                "id": "1ef7318b-8206-4da7-9d1d-a616a01e2e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f88c54-c55b-4566-8153-7d40dec0bcb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30c59ebe-eed2-417f-bf1e-c8025b50723f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f88c54-c55b-4566-8153-7d40dec0bcb9",
                    "LayerId": "b3484002-7527-4047-9ae4-952164799300"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b3484002-7527-4047-9ae4-952164799300",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a19a40-bbd2-43eb-8d21-edc8c34fd399",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -57,
    "yorig": 84
}