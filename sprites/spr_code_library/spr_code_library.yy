{
    "id": "8cdf501f-08a4-4ffa-8a9c-4cc6c8c1b452",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_code_library",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 264,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f73482f-7b05-4b01-bc3f-c6cf7e3e7e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cdf501f-08a4-4ffa-8a9c-4cc6c8c1b452",
            "compositeImage": {
                "id": "c41d6484-6194-4387-9442-159884e462f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f73482f-7b05-4b01-bc3f-c6cf7e3e7e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2807b84-e553-4674-a73e-0ef49c42b15d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f73482f-7b05-4b01-bc3f-c6cf7e3e7e97",
                    "LayerId": "a6006c44-1e77-46ce-9ef0-4698ce17d178"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 265,
    "layers": [
        {
            "id": "a6006c44-1e77-46ce-9ef0-4698ce17d178",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cdf501f-08a4-4ffa-8a9c-4cc6c8c1b452",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 132
}