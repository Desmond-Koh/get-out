{
    "id": "3250325e-f28c-4642-accb-c4132f92b143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bookControls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 6,
    "bbox_right": 194,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc66da1e-9fea-4510-987d-ee651573f0a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3250325e-f28c-4642-accb-c4132f92b143",
            "compositeImage": {
                "id": "ad7555de-1461-4281-a2c3-cae8023e301e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc66da1e-9fea-4510-987d-ee651573f0a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34875f3f-a567-4bb8-a99a-fa5588d7d5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc66da1e-9fea-4510-987d-ee651573f0a1",
                    "LayerId": "d2add272-08b5-47fe-ba1a-e01da6d8bb88"
                }
            ]
        },
        {
            "id": "d63ec188-05b6-4ef3-856f-f1122a00af66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3250325e-f28c-4642-accb-c4132f92b143",
            "compositeImage": {
                "id": "d1fdf102-b440-49a4-9bf2-711c1cd0880b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63ec188-05b6-4ef3-856f-f1122a00af66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "918c0e1a-ddc6-4f83-9b3e-f4b5da58c70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63ec188-05b6-4ef3-856f-f1122a00af66",
                    "LayerId": "d2add272-08b5-47fe-ba1a-e01da6d8bb88"
                }
            ]
        },
        {
            "id": "c803977c-3cad-4990-9d6d-a745f6ed51a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3250325e-f28c-4642-accb-c4132f92b143",
            "compositeImage": {
                "id": "364ec2cf-86d0-425e-8751-20ad5b4513ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c803977c-3cad-4990-9d6d-a745f6ed51a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289f1770-0991-48ad-866a-71c7950a15a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c803977c-3cad-4990-9d6d-a745f6ed51a6",
                    "LayerId": "d2add272-08b5-47fe-ba1a-e01da6d8bb88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "d2add272-08b5-47fe-ba1a-e01da6d8bb88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3250325e-f28c-4642-accb-c4132f92b143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 40
}