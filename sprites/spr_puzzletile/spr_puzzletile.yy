{
    "id": "708ea4fe-2b5d-40f4-ab80-35b6b5456390",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_puzzletile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad010b2d-542a-4c5a-8c0c-c051ec3fdbd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "708ea4fe-2b5d-40f4-ab80-35b6b5456390",
            "compositeImage": {
                "id": "008d1a79-8880-4735-a01c-a75e2266329a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad010b2d-542a-4c5a-8c0c-c051ec3fdbd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e6ae9ad-8d5c-4896-aa91-9b62ee7bf20b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad010b2d-542a-4c5a-8c0c-c051ec3fdbd8",
                    "LayerId": "13e91db8-f190-42fc-a810-e46711ab5127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "13e91db8-f190-42fc-a810-e46711ab5127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "708ea4fe-2b5d-40f4-ab80-35b6b5456390",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}