{
    "id": "ca154095-ba84-4680-97ca-e21f3049cb02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 743,
    "bbox_left": 9,
    "bbox_right": 750,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41ef84ba-1081-493c-94f6-2ff9c4a803dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca154095-ba84-4680-97ca-e21f3049cb02",
            "compositeImage": {
                "id": "f14041b2-0fdc-45bd-8680-04cc62d30120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41ef84ba-1081-493c-94f6-2ff9c4a803dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f7b44d-647c-433b-9cd7-a537c3b0141c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41ef84ba-1081-493c-94f6-2ff9c4a803dd",
                    "LayerId": "de239dce-1ad0-4a95-9856-d7701f8d5541"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "de239dce-1ad0-4a95-9856-d7701f8d5541",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca154095-ba84-4680-97ca-e21f3049cb02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}