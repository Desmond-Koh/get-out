{
    "id": "8d6adb3f-38c5-456e-bd70-d272ed5eb5a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontstamina",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7719f8e-7494-4df8-8300-aff390b3cdde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d6adb3f-38c5-456e-bd70-d272ed5eb5a0",
            "compositeImage": {
                "id": "9a8ef8ce-7fde-4380-bbfc-5335f4193cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7719f8e-7494-4df8-8300-aff390b3cdde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311c460d-1c09-4f39-864b-b29b6a154db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7719f8e-7494-4df8-8300-aff390b3cdde",
                    "LayerId": "7d4d8ff1-3128-4a7f-b283-22860670e35a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "7d4d8ff1-3128-4a7f-b283-22860670e35a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d6adb3f-38c5-456e-bd70-d272ed5eb5a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}