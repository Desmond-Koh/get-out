{
    "id": "f538c9a7-c2fb-4290-9f6c-712b184a5a27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backbattery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01c77538-9a68-4e90-94c4-999f42a5078e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f538c9a7-c2fb-4290-9f6c-712b184a5a27",
            "compositeImage": {
                "id": "e83d7b1a-4d87-4f2c-ab6a-edee83417ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c77538-9a68-4e90-94c4-999f42a5078e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a78829db-1399-4331-8da5-252873be1772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c77538-9a68-4e90-94c4-999f42a5078e",
                    "LayerId": "0f1f4bcf-691f-4a1c-9c9a-c03ad0ec930e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "0f1f4bcf-691f-4a1c-9c9a-c03ad0ec930e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f538c9a7-c2fb-4290-9f6c-712b184a5a27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}