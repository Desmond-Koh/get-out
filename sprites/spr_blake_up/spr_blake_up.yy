{
    "id": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blake_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 2,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c68d0d5-1dcb-4943-b2af-1f243919693b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
            "compositeImage": {
                "id": "5a5f7a38-705f-48f5-be8c-a9eda770f1e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c68d0d5-1dcb-4943-b2af-1f243919693b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1849cd-24c8-48ca-8c53-e0df4e51d24a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c68d0d5-1dcb-4943-b2af-1f243919693b",
                    "LayerId": "f8c8308e-e220-4216-bdf0-8ae0d4f732c8"
                }
            ]
        },
        {
            "id": "76960746-5d4c-4895-a76e-3487a61e377c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
            "compositeImage": {
                "id": "41aa5178-5436-42e3-8aec-c0abdb8a6fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76960746-5d4c-4895-a76e-3487a61e377c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aa4f4f0-beb9-41d2-a216-6806aa6a3561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76960746-5d4c-4895-a76e-3487a61e377c",
                    "LayerId": "f8c8308e-e220-4216-bdf0-8ae0d4f732c8"
                }
            ]
        },
        {
            "id": "3e332d92-ec4a-40c6-bbc7-2d3644271ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
            "compositeImage": {
                "id": "89ea8202-20f9-4b6b-8f94-282e908d719d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e332d92-ec4a-40c6-bbc7-2d3644271ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30373e2-ec77-40ce-b2b4-d60d125bcbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e332d92-ec4a-40c6-bbc7-2d3644271ed4",
                    "LayerId": "f8c8308e-e220-4216-bdf0-8ae0d4f732c8"
                }
            ]
        },
        {
            "id": "293d4072-65a1-4303-aa98-3783029d5da8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
            "compositeImage": {
                "id": "821345f2-df87-49c5-88c9-6a9179f65591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "293d4072-65a1-4303-aa98-3783029d5da8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476aefc3-285f-4a88-8b1f-1ba109e87442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "293d4072-65a1-4303-aa98-3783029d5da8",
                    "LayerId": "f8c8308e-e220-4216-bdf0-8ae0d4f732c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "f8c8308e-e220-4216-bdf0-8ae0d4f732c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd0702be-b4a2-4b65-99bd-6030c13aca7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}