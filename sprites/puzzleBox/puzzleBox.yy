{
    "id": "9fcfd388-9af0-4b4c-bf85-2030ba31da46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "puzzleBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 13,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23ddbe41-5d3a-4a1c-bdef-477ee1dda857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fcfd388-9af0-4b4c-bf85-2030ba31da46",
            "compositeImage": {
                "id": "cdef9ad8-1b37-4e3b-92d5-1bc03081113a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23ddbe41-5d3a-4a1c-bdef-477ee1dda857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e481ccce-d9da-491d-ba57-3a1e7a0f4ded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23ddbe41-5d3a-4a1c-bdef-477ee1dda857",
                    "LayerId": "34e61774-cf7a-4eec-9a6d-c5338b4c4fee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "34e61774-cf7a-4eec-9a6d-c5338b4c4fee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fcfd388-9af0-4b4c-bf85-2030ba31da46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}