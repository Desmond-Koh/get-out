{
    "id": "0d5057f7-4c86-487e-8cb4-851f11cb00df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_visibility",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 699,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f15c0f6-b91f-4376-a7af-c6453b4d97fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d5057f7-4c86-487e-8cb4-851f11cb00df",
            "compositeImage": {
                "id": "3d9a1dcb-da11-4e43-af36-d4be86c394a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f15c0f6-b91f-4376-a7af-c6453b4d97fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a943e26f-3c31-47f1-8c70-8d6514204199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f15c0f6-b91f-4376-a7af-c6453b4d97fb",
                    "LayerId": "65e88f96-d5a7-4557-a72b-852a931a5200"
                }
            ]
        },
        {
            "id": "9dd9e90c-ad46-47e9-9c91-826b1900600f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d5057f7-4c86-487e-8cb4-851f11cb00df",
            "compositeImage": {
                "id": "2f63012e-01d6-4ef5-b5c5-8f7a6cbcb6b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd9e90c-ad46-47e9-9c91-826b1900600f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "385f4728-9fe8-4191-b79a-fc5b29c37723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd9e90c-ad46-47e9-9c91-826b1900600f",
                    "LayerId": "65e88f96-d5a7-4557-a72b-852a931a5200"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 700,
    "layers": [
        {
            "id": "65e88f96-d5a7-4557-a72b-852a931a5200",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d5057f7-4c86-487e-8cb4-851f11cb00df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 350
}