{
    "id": "e16262aa-e648-4e81-9bc6-04a1433ee64a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_puzzleborder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 0,
    "bbox_right": 207,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de28ce25-9377-4a48-8c2f-047ca39a6f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e16262aa-e648-4e81-9bc6-04a1433ee64a",
            "compositeImage": {
                "id": "bb050eb8-5a0a-4d48-9000-ae26c2860f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de28ce25-9377-4a48-8c2f-047ca39a6f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d9eba1-86be-4af9-8ac2-f5a9f33cd1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de28ce25-9377-4a48-8c2f-047ca39a6f8c",
                    "LayerId": "974bd53e-4bbf-4596-8b41-f3f37b5662cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 208,
    "layers": [
        {
            "id": "974bd53e-4bbf-4596-8b41-f3f37b5662cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e16262aa-e648-4e81-9bc6-04a1433ee64a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 4,
    "yorig": 4
}