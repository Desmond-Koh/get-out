{
    "id": "2210bbd4-bcbd-4617-8b34-9113efb7014f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontTiming",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4189d7e-8471-46a6-88a8-17e85aa8135e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2210bbd4-bcbd-4617-8b34-9113efb7014f",
            "compositeImage": {
                "id": "f71711c5-5755-421b-a6dc-dc25c900546f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4189d7e-8471-46a6-88a8-17e85aa8135e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c248cb93-04a2-498a-866a-d82baa2579a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4189d7e-8471-46a6-88a8-17e85aa8135e",
                    "LayerId": "0b9ca1d9-23e2-412f-b375-aacc7da1b617"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "0b9ca1d9-23e2-412f-b375-aacc7da1b617",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2210bbd4-bcbd-4617-8b34-9113efb7014f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 0,
    "yorig": 10
}