{
    "id": "1dc28cc7-a297-4e13-965f-d75e045eb00a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardOpen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 899,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6055248-2884-42e1-9756-5070b3c50284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dc28cc7-a297-4e13-965f-d75e045eb00a",
            "compositeImage": {
                "id": "07c9c3bc-85ca-4050-9304-e8d207fbd2e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6055248-2884-42e1-9756-5070b3c50284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57ce8485-4539-4f92-8b15-aef67e19a519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6055248-2884-42e1-9756-5070b3c50284",
                    "LayerId": "3ef11959-144b-4dfe-a170-eb46c877d44c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "3ef11959-144b-4dfe-a170-eb46c877d44c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dc28cc7-a297-4e13-965f-d75e045eb00a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 450,
    "yorig": 250
}