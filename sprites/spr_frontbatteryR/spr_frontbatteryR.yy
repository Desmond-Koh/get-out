{
    "id": "d292e1f0-bebb-4816-bc05-2ac56020e156",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontbatteryR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce6d5739-7313-4b14-8e0f-0db2f3fe0109",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d292e1f0-bebb-4816-bc05-2ac56020e156",
            "compositeImage": {
                "id": "bb54ac1a-3337-4e7e-b52e-b5456af76445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6d5739-7313-4b14-8e0f-0db2f3fe0109",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a557d4-c49b-44d8-9c3b-971c604278fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6d5739-7313-4b14-8e0f-0db2f3fe0109",
                    "LayerId": "fb2c75ec-0068-460b-baca-2ef2b371b37d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "fb2c75ec-0068-460b-baca-2ef2b371b37d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d292e1f0-bebb-4816-bc05-2ac56020e156",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}