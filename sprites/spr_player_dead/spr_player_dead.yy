{
    "id": "25550b8c-ab27-4f2d-bc5e-9130af4ad076",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 3,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8d73dce-b400-45f7-b316-827b5b43f773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25550b8c-ab27-4f2d-bc5e-9130af4ad076",
            "compositeImage": {
                "id": "05ca67ac-cc50-43ff-981b-a1d86514e5a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d73dce-b400-45f7-b316-827b5b43f773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f96c56-7934-479e-b6d3-48f07ed35a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d73dce-b400-45f7-b316-827b5b43f773",
                    "LayerId": "cbd4679d-03a2-4b97-9668-1296b59212b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "cbd4679d-03a2-4b97-9668-1296b59212b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25550b8c-ab27-4f2d-bc5e-9130af4ad076",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}