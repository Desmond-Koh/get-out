{
    "id": "09c082ae-438f-44fe-a0dc-7fdc38912730",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox_cupboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 3,
    "bbox_right": 149,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb68d730-62b0-4399-a6e9-957f10c1513b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09c082ae-438f-44fe-a0dc-7fdc38912730",
            "compositeImage": {
                "id": "098d59af-be15-4abe-905d-f552724b9e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb68d730-62b0-4399-a6e9-957f10c1513b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa56693-2f37-4771-808f-4f002c160a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb68d730-62b0-4399-a6e9-957f10c1513b",
                    "LayerId": "313ae7ec-3b54-438c-bd9a-7ae2555aca1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "313ae7ec-3b54-438c-bd9a-7ae2555aca1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09c082ae-438f-44fe-a0dc-7fdc38912730",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 78,
    "yorig": 32
}