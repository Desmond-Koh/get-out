{
    "id": "16a340c2-609e-4b55-9c75-92d5bad816d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_keyFragment3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 21,
    "bbox_right": 48,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5959ed8-adf5-47c9-b2a0-6e99453eec17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16a340c2-609e-4b55-9c75-92d5bad816d5",
            "compositeImage": {
                "id": "0ce5efbe-049d-4a71-83ad-fead33615edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5959ed8-adf5-47c9-b2a0-6e99453eec17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d44f704-132a-4bd0-945f-433f0a63d3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5959ed8-adf5-47c9-b2a0-6e99453eec17",
                    "LayerId": "7275b7c6-1d1d-4a8c-8f1b-5c556a28c9dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7275b7c6-1d1d-4a8c-8f1b-5c556a28c9dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16a340c2-609e-4b55-9c75-92d5bad816d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}