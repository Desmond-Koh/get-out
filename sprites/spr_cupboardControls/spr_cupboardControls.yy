{
    "id": "035b97e9-2227-46e0-b9eb-844a23687e6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardControls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ade09d6a-3984-447d-b914-4fa6be3bdd1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035b97e9-2227-46e0-b9eb-844a23687e6b",
            "compositeImage": {
                "id": "9dd04502-a91b-4260-8379-a571cc92e4fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ade09d6a-3984-447d-b914-4fa6be3bdd1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "611eb3cd-2fd5-4f54-887f-a50ea47ac689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ade09d6a-3984-447d-b914-4fa6be3bdd1f",
                    "LayerId": "0e28c0e3-7df6-4b14-9a63-1ad369f17664"
                }
            ]
        },
        {
            "id": "00a14bf3-9501-4dec-83f9-71ea50750b73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035b97e9-2227-46e0-b9eb-844a23687e6b",
            "compositeImage": {
                "id": "32f7800c-b288-4372-b80e-d71ac641b9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00a14bf3-9501-4dec-83f9-71ea50750b73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb401f33-a56d-44bb-9f27-7fa48c61e985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00a14bf3-9501-4dec-83f9-71ea50750b73",
                    "LayerId": "0e28c0e3-7df6-4b14-9a63-1ad369f17664"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "0e28c0e3-7df6-4b14-9a63-1ad369f17664",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "035b97e9-2227-46e0-b9eb-844a23687e6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 40
}