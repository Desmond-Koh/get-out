{
    "id": "24d711fe-f9f7-4cf6-b9ed-c35208362b31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_visibilityPuzzle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1049,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b5ed49e-925d-4472-b715-e459bd746134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d711fe-f9f7-4cf6-b9ed-c35208362b31",
            "compositeImage": {
                "id": "832b6141-d90d-4a1f-bf5d-3987c6afe422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b5ed49e-925d-4472-b715-e459bd746134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b41a2240-d41e-45dc-87c1-54a02343af39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b5ed49e-925d-4472-b715-e459bd746134",
                    "LayerId": "da34e0fb-8592-4c3a-8d5e-aa042eda2a30"
                }
            ]
        },
        {
            "id": "866d3c44-0e8a-4e36-af0c-8c1c8a71ab1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24d711fe-f9f7-4cf6-b9ed-c35208362b31",
            "compositeImage": {
                "id": "065e4096-66d0-41a0-934c-05abbbf5fb1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866d3c44-0e8a-4e36-af0c-8c1c8a71ab1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4096f97f-518d-45c9-9c39-53d49b527576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866d3c44-0e8a-4e36-af0c-8c1c8a71ab1e",
                    "LayerId": "da34e0fb-8592-4c3a-8d5e-aa042eda2a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1050,
    "layers": [
        {
            "id": "da34e0fb-8592-4c3a-8d5e-aa042eda2a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24d711fe-f9f7-4cf6-b9ed-c35208362b31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 525
}