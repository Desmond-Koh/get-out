{
    "id": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_book",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 415,
    "bbox_left": 38,
    "bbox_right": 611,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "538507c3-09e9-4846-8d69-3018e2c88712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
            "compositeImage": {
                "id": "df1cb46a-fc59-43e7-8968-b6397c8fad4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538507c3-09e9-4846-8d69-3018e2c88712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15984519-b23e-43f2-9b53-662b4b386073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538507c3-09e9-4846-8d69-3018e2c88712",
                    "LayerId": "e637a208-8c9b-481f-8e2c-2dcb5b87f4a1"
                }
            ]
        },
        {
            "id": "7819b02d-9a12-439e-84f0-2b2f2629505c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
            "compositeImage": {
                "id": "81f21e9d-073b-4bad-9391-ef13fb35afb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7819b02d-9a12-439e-84f0-2b2f2629505c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9727d521-f6bd-4f87-a9d7-f711dae6bd70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7819b02d-9a12-439e-84f0-2b2f2629505c",
                    "LayerId": "e637a208-8c9b-481f-8e2c-2dcb5b87f4a1"
                }
            ]
        },
        {
            "id": "e5b31813-bcd6-4be4-b3d2-464bdfa1ee01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
            "compositeImage": {
                "id": "a5cb7533-cca0-4d3c-920d-27043287d785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b31813-bcd6-4be4-b3d2-464bdfa1ee01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b96ce18-fc42-4a28-bbdd-d4cf2b3603bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b31813-bcd6-4be4-b3d2-464bdfa1ee01",
                    "LayerId": "e637a208-8c9b-481f-8e2c-2dcb5b87f4a1"
                }
            ]
        },
        {
            "id": "b9a9b0c3-4fea-41ad-acbf-27dee3b05796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
            "compositeImage": {
                "id": "a3ec195e-6979-49f2-b137-579e88e0346d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a9b0c3-4fea-41ad-acbf-27dee3b05796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145e60b1-afd2-4cf7-9cec-fdce162f87d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a9b0c3-4fea-41ad-acbf-27dee3b05796",
                    "LayerId": "e637a208-8c9b-481f-8e2c-2dcb5b87f4a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "e637a208-8c9b-481f-8e2c-2dcb5b87f4a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 650,
    "xorig": 325,
    "yorig": 225
}