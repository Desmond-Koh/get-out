{
    "id": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anna_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90794ca4-1522-4376-ace5-5cd665e86566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
            "compositeImage": {
                "id": "79daf621-914b-48ef-ae43-b83ba35125aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90794ca4-1522-4376-ace5-5cd665e86566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d293f8-6e6e-4d71-b7e0-535237f8dcd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90794ca4-1522-4376-ace5-5cd665e86566",
                    "LayerId": "ad2e0e92-e4ec-4998-a830-0f46ba020f8e"
                }
            ]
        },
        {
            "id": "ccf88dd5-f843-4750-b110-c475466b9f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
            "compositeImage": {
                "id": "d475e402-a62b-429a-94a3-3a1a323d9a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccf88dd5-f843-4750-b110-c475466b9f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32da8cab-48e3-486f-b78c-839c8e8ae470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccf88dd5-f843-4750-b110-c475466b9f1c",
                    "LayerId": "ad2e0e92-e4ec-4998-a830-0f46ba020f8e"
                }
            ]
        },
        {
            "id": "bb209f17-f0e4-46bc-b3a9-bc2006a8ab50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
            "compositeImage": {
                "id": "a1d325f5-e89b-4350-8885-5afacb5a7e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb209f17-f0e4-46bc-b3a9-bc2006a8ab50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bccbdb3-58e7-449c-8bd1-2c3c88a5217e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb209f17-f0e4-46bc-b3a9-bc2006a8ab50",
                    "LayerId": "ad2e0e92-e4ec-4998-a830-0f46ba020f8e"
                }
            ]
        },
        {
            "id": "3209a0fa-5429-46d3-947b-a014f2be4ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
            "compositeImage": {
                "id": "a5bf3e68-c9cc-4582-937c-e47fe09e3dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3209a0fa-5429-46d3-947b-a014f2be4ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42964229-8824-49a5-90d9-c064297e4c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3209a0fa-5429-46d3-947b-a014f2be4ef3",
                    "LayerId": "ad2e0e92-e4ec-4998-a830-0f46ba020f8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "ad2e0e92-e4ec-4998-a830-0f46ba020f8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44646d6a-7590-4fbd-9e29-6c1b180d16f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 46,
    "yorig": 5
}