{
    "id": "c190eaa9-7408-4e90-9863-c5f694d2015b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blackscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8022ec07-31a2-4228-bde6-42d402827890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c190eaa9-7408-4e90-9863-c5f694d2015b",
            "compositeImage": {
                "id": "f220897c-0fca-42fc-be7d-7275acd170f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8022ec07-31a2-4228-bde6-42d402827890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d2d1b5b-cacb-4713-8d91-46eeebaead9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8022ec07-31a2-4228-bde6-42d402827890",
                    "LayerId": "745c638d-cee9-4ad1-b153-6add0bfd0e85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "745c638d-cee9-4ad1-b153-6add0bfd0e85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c190eaa9-7408-4e90-9863-c5f694d2015b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 270
}