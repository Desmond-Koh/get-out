{
    "id": "e006c45e-bb54-46e0-bc93-b2c7ec9ffd92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox_playeritem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 25,
    "bbox_right": 129,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "975551cf-49c5-48b3-ad1e-908e805649fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e006c45e-bb54-46e0-bc93-b2c7ec9ffd92",
            "compositeImage": {
                "id": "aa4e7ba1-f70b-4450-9104-c018765f86f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "975551cf-49c5-48b3-ad1e-908e805649fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23fd8e8d-0c30-411f-bd15-f2eb5eb9beb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "975551cf-49c5-48b3-ad1e-908e805649fd",
                    "LayerId": "7af795a1-bf30-451f-924a-d48d5ee3535e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7af795a1-bf30-451f-924a-d48d5ee3535e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e006c45e-bb54-46e0-bc93-b2c7ec9ffd92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 78,
    "yorig": 32
}