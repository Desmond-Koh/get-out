{
    "id": "5954b4e3-9c26-47ba-825b-b1871ab13075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_void",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8af04439-1a7b-4780-bdef-7c2dc4d135e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5954b4e3-9c26-47ba-825b-b1871ab13075",
            "compositeImage": {
                "id": "c4e42689-e850-4c9b-aba4-914386a932ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af04439-1a7b-4780-bdef-7c2dc4d135e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc68795-1930-40f7-ab3a-10092b20f3d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af04439-1a7b-4780-bdef-7c2dc4d135e3",
                    "LayerId": "fef897f0-bdd2-4e68-b426-77931a0b5056"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fef897f0-bdd2-4e68-b426-77931a0b5056",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5954b4e3-9c26-47ba-825b-b1871ab13075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}