{
    "id": "65fc5d02-124c-4de0-9dcf-b33c5a20ad0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_schooltileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 762,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4152e026-b42a-414f-a8d6-adc37140a324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65fc5d02-124c-4de0-9dcf-b33c5a20ad0f",
            "compositeImage": {
                "id": "740f7314-30f4-4c5c-8ccc-53d341ecf7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4152e026-b42a-414f-a8d6-adc37140a324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1eba76-35aa-4461-9aee-0ad1ea284a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4152e026-b42a-414f-a8d6-adc37140a324",
                    "LayerId": "4242adb2-ec81-4721-9b96-3ca4e0072934"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "4242adb2-ec81-4721-9b96-3ca4e0072934",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65fc5d02-124c-4de0-9dcf-b33c5a20ad0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 0,
    "yorig": 0
}