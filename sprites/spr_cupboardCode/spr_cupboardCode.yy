{
    "id": "95d110c9-2a8f-4b93-b493-d8cca06957de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardCode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aee8386f-92ce-457f-83eb-861361c64a5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95d110c9-2a8f-4b93-b493-d8cca06957de",
            "compositeImage": {
                "id": "9ebdc22d-c39a-42f2-beb6-3896e00e36af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee8386f-92ce-457f-83eb-861361c64a5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b535e244-c737-4d80-b053-c34d4e6bdaba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee8386f-92ce-457f-83eb-861361c64a5c",
                    "LayerId": "b13bffc3-0d63-4818-81db-a53badfcb789"
                }
            ]
        },
        {
            "id": "42b17114-cb78-4caa-b38c-60f2dab84196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95d110c9-2a8f-4b93-b493-d8cca06957de",
            "compositeImage": {
                "id": "1ec3e6cc-5005-4bdd-8da7-02584438c06e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b17114-cb78-4caa-b38c-60f2dab84196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61772c64-adc9-45af-89e1-85079cd357aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b17114-cb78-4caa-b38c-60f2dab84196",
                    "LayerId": "b13bffc3-0d63-4818-81db-a53badfcb789"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b13bffc3-0d63-4818-81db-a53badfcb789",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95d110c9-2a8f-4b93-b493-d8cca06957de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}