{
    "id": "ff694f9b-1b29-4b82-bab0-31d84952bb75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontbatteryY",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a3b3a25-5e9a-4fac-b300-ee9a9d9411f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff694f9b-1b29-4b82-bab0-31d84952bb75",
            "compositeImage": {
                "id": "350a0162-0171-4f61-8bcf-6e42126e841e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3b3a25-5e9a-4fac-b300-ee9a9d9411f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1909f254-9d28-44d6-9b1f-8dcacfe20001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3b3a25-5e9a-4fac-b300-ee9a9d9411f4",
                    "LayerId": "74b8f7fc-f170-43d5-ac17-d30d828bd459"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "74b8f7fc-f170-43d5-ac17-d30d828bd459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff694f9b-1b29-4b82-bab0-31d84952bb75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}