{
    "id": "0d5966c8-d738-437f-9b13-4c720628a890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mainmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c6c95fb-c44d-423e-9f94-cad396e5cdcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d5966c8-d738-437f-9b13-4c720628a890",
            "compositeImage": {
                "id": "8dea4658-df37-4ebe-8b3d-7bbb37acf547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6c95fb-c44d-423e-9f94-cad396e5cdcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3855abf0-8acd-46de-8fdd-dc744779c673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6c95fb-c44d-423e-9f94-cad396e5cdcd",
                    "LayerId": "18774e2c-a2e0-4058-b23d-d083caa25a4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "18774e2c-a2e0-4058-b23d-d083caa25a4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d5966c8-d738-437f-9b13-4c720628a890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}