{
    "id": "eeed901c-a205-46c9-8f27-a9386dbf2640",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardLock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 23,
    "bbox_right": 126,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb01c4f4-1dbf-4879-81ef-80a3e6959d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eeed901c-a205-46c9-8f27-a9386dbf2640",
            "compositeImage": {
                "id": "7ff1863f-9352-4205-89ad-82ec7e21e6a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb01c4f4-1dbf-4879-81ef-80a3e6959d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7d909e-762a-4484-a144-846c89e08a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb01c4f4-1dbf-4879-81ef-80a3e6959d2e",
                    "LayerId": "0cae96b8-02c2-4894-af41-f9e5cf251267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "0cae96b8-02c2-4894-af41-f9e5cf251267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eeed901c-a205-46c9-8f27-a9386dbf2640",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 75
}