{
    "id": "f632a200-8377-4d91-9204-8d176c1e1f26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backstaminaY",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ad36dff-1a9a-48be-b1e6-7a9807ab7965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f632a200-8377-4d91-9204-8d176c1e1f26",
            "compositeImage": {
                "id": "291bdadf-531d-4ccb-a302-5e240926ccb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad36dff-1a9a-48be-b1e6-7a9807ab7965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a186482e-562c-4f11-9a98-4f4c4bb5d73e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad36dff-1a9a-48be-b1e6-7a9807ab7965",
                    "LayerId": "b01a51a5-090e-42b3-bcac-725ea78ff6db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "b01a51a5-090e-42b3-bcac-725ea78ff6db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f632a200-8377-4d91-9204-8d176c1e1f26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}