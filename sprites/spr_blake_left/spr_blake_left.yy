{
    "id": "45003897-bba4-47e1-b626-bea533dbae96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blake_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 2,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b815581c-8f17-4db4-bba0-12ed1143d582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45003897-bba4-47e1-b626-bea533dbae96",
            "compositeImage": {
                "id": "ba939b75-2cfe-4119-81c2-a56ed6eeb4d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b815581c-8f17-4db4-bba0-12ed1143d582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b68b9b-b640-472c-9445-781a7acb387a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b815581c-8f17-4db4-bba0-12ed1143d582",
                    "LayerId": "05e171eb-6241-4a3b-ae46-1427b3c44c99"
                }
            ]
        },
        {
            "id": "2c9926ae-32bf-416b-9fbd-4d24aa7f5324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45003897-bba4-47e1-b626-bea533dbae96",
            "compositeImage": {
                "id": "42949888-25bc-47a0-a387-b43f2ade6d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9926ae-32bf-416b-9fbd-4d24aa7f5324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d48dfc-6fbc-470b-bbaa-3f26f78edca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9926ae-32bf-416b-9fbd-4d24aa7f5324",
                    "LayerId": "05e171eb-6241-4a3b-ae46-1427b3c44c99"
                }
            ]
        },
        {
            "id": "f9cc33e0-c8d9-45ea-b062-6ac61a8116d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45003897-bba4-47e1-b626-bea533dbae96",
            "compositeImage": {
                "id": "321d5e7e-5825-4d26-b5f1-19ec633eecc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9cc33e0-c8d9-45ea-b062-6ac61a8116d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f70f91d-3df4-486b-98ab-977d997e5c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9cc33e0-c8d9-45ea-b062-6ac61a8116d2",
                    "LayerId": "05e171eb-6241-4a3b-ae46-1427b3c44c99"
                }
            ]
        },
        {
            "id": "d7ab392d-5e33-45cf-a1c1-4ddb70a88460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45003897-bba4-47e1-b626-bea533dbae96",
            "compositeImage": {
                "id": "2642497e-8fa9-4afb-802c-92a51c12a631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ab392d-5e33-45cf-a1c1-4ddb70a88460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d5a7ba-66f3-4f94-b822-3ab2bbe69bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ab392d-5e33-45cf-a1c1-4ddb70a88460",
                    "LayerId": "05e171eb-6241-4a3b-ae46-1427b3c44c99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "05e171eb-6241-4a3b-ae46-1427b3c44c99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45003897-bba4-47e1-b626-bea533dbae96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 25
}