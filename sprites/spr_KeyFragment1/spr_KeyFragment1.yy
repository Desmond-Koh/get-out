{
    "id": "a048c6e4-ee03-4fd0-9dad-7c75c3a581ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_keyFragment1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 26,
    "bbox_right": 38,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "427c1f44-9dcc-4104-b6b3-3a9319878fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a048c6e4-ee03-4fd0-9dad-7c75c3a581ef",
            "compositeImage": {
                "id": "d680e91f-f387-4a88-9fbd-4828cca8c12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427c1f44-9dcc-4104-b6b3-3a9319878fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261c8af0-67c2-40c9-8e7a-a494b4af2ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427c1f44-9dcc-4104-b6b3-3a9319878fbb",
                    "LayerId": "8664ce0a-68f1-410d-9c7e-1255568ec063"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8664ce0a-68f1-410d-9c7e-1255568ec063",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a048c6e4-ee03-4fd0-9dad-7c75c3a581ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}