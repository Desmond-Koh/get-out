{
    "id": "e542a3f0-48e7-4d92-a86d-58030e6118b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backTiming",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1df92e0e-658e-4f54-8eb7-a38729374e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e542a3f0-48e7-4d92-a86d-58030e6118b7",
            "compositeImage": {
                "id": "daf7ee05-9bd7-42f3-a21f-d98d71aaef33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df92e0e-658e-4f54-8eb7-a38729374e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e021310-9f5f-4cf4-be37-c1d3f884747a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df92e0e-658e-4f54-8eb7-a38729374e78",
                    "LayerId": "94d98680-ff3e-464d-87bf-ac1f8bc8f471"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "94d98680-ff3e-464d-87bf-ac1f8bc8f471",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e542a3f0-48e7-4d92-a86d-58030e6118b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 0,
    "yorig": 10
}