{
    "id": "94b1d105-7980-46be-981b-329bcb1a413f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_basketballcourtLight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 5,
    "bbox_right": 81,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56450351-7e0e-4046-9d71-964d4af379a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b1d105-7980-46be-981b-329bcb1a413f",
            "compositeImage": {
                "id": "76a563f2-d6ae-447c-8545-3b0db07dec6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56450351-7e0e-4046-9d71-964d4af379a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708c87a8-44c3-4ef0-8ba8-fbb503a10365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56450351-7e0e-4046-9d71-964d4af379a9",
                    "LayerId": "7c3240a9-2996-47e0-b085-dca6b7748b49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 86,
    "layers": [
        {
            "id": "7c3240a9-2996-47e0-b085-dca6b7748b49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b1d105-7980-46be-981b-329bcb1a413f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 86,
    "xorig": 43,
    "yorig": 43
}