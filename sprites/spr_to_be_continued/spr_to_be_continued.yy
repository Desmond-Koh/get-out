{
    "id": "b910b2f0-bf25-4b1b-95ff-4e5f0c34199f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_to_be_continued",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3affe309-bda9-4855-bc70-e450dd87fa23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b910b2f0-bf25-4b1b-95ff-4e5f0c34199f",
            "compositeImage": {
                "id": "f0ff0084-b0da-446f-be95-6e20f694e56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3affe309-bda9-4855-bc70-e450dd87fa23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6cca8f-3843-4fd9-ad8c-a1b91e21fa2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3affe309-bda9-4855-bc70-e450dd87fa23",
                    "LayerId": "6c623b08-6834-4b22-b5c1-6412c63f250b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "6c623b08-6834-4b22-b5c1-6412c63f250b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b910b2f0-bf25-4b1b-95ff-4e5f0c34199f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}