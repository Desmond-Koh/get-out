{
    "id": "adb88cce-4468-487d-bfe2-2c3eae4ba2c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontbattery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fc14fec-f24b-4cf1-9676-ebd92bf8a9e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adb88cce-4468-487d-bfe2-2c3eae4ba2c8",
            "compositeImage": {
                "id": "f7e5fccb-9259-44a4-b949-aa8cefa529eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc14fec-f24b-4cf1-9676-ebd92bf8a9e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20096c50-6956-4b6b-a012-5895b23d7338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc14fec-f24b-4cf1-9676-ebd92bf8a9e8",
                    "LayerId": "9ad39fdd-ee8b-43d6-a968-435863816b6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "9ad39fdd-ee8b-43d6-a968-435863816b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adb88cce-4468-487d-bfe2-2c3eae4ba2c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}