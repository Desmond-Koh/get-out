{
    "id": "63cf19bf-46bf-45c8-a12b-700cfd01f91b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backstaminaR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61bef5c1-3404-4e3a-ae9b-4fd37f6c30d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63cf19bf-46bf-45c8-a12b-700cfd01f91b",
            "compositeImage": {
                "id": "a548e878-9e74-4ac8-be7b-669bb4b8da78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61bef5c1-3404-4e3a-ae9b-4fd37f6c30d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b73f4b-bf3e-4f34-aa4b-030efb5d505e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bef5c1-3404-4e3a-ae9b-4fd37f6c30d2",
                    "LayerId": "03064964-f8bd-4aef-bbed-ad231ffcc81d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "03064964-f8bd-4aef-bbed-ad231ffcc81d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63cf19bf-46bf-45c8-a12b-700cfd01f91b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}