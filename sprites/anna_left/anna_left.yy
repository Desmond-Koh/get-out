{
    "id": "213549c5-449a-448f-8995-bee565f79b46",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "anna_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e8c55fa-ea7c-45db-ac11-344a6fce5c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213549c5-449a-448f-8995-bee565f79b46",
            "compositeImage": {
                "id": "aa95bf5a-8a19-4682-84af-5c2268fe5cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8c55fa-ea7c-45db-ac11-344a6fce5c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45ef0c8-8ec8-41f2-9524-3009e32f056b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8c55fa-ea7c-45db-ac11-344a6fce5c66",
                    "LayerId": "a2b7441c-771d-46af-9a5f-bbfdca2e6adc"
                }
            ]
        },
        {
            "id": "a36d5c1a-2e3e-4380-aaeb-691844b4bc8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213549c5-449a-448f-8995-bee565f79b46",
            "compositeImage": {
                "id": "c4bbd18f-8f06-4fd8-8274-49ae453c6b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36d5c1a-2e3e-4380-aaeb-691844b4bc8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d74eb4-f783-461c-8e43-da5d382d1adb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36d5c1a-2e3e-4380-aaeb-691844b4bc8f",
                    "LayerId": "a2b7441c-771d-46af-9a5f-bbfdca2e6adc"
                }
            ]
        },
        {
            "id": "d027d9eb-0ea5-4996-82b9-44ca101f905f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213549c5-449a-448f-8995-bee565f79b46",
            "compositeImage": {
                "id": "5e5fe7e3-c494-489d-9cec-a01110050080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d027d9eb-0ea5-4996-82b9-44ca101f905f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc594000-bcdb-4b68-acc5-0d67d03d148d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d027d9eb-0ea5-4996-82b9-44ca101f905f",
                    "LayerId": "a2b7441c-771d-46af-9a5f-bbfdca2e6adc"
                }
            ]
        },
        {
            "id": "f70ccd6b-a360-4e0f-9f3a-a462d4bbfdbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "213549c5-449a-448f-8995-bee565f79b46",
            "compositeImage": {
                "id": "990e72da-79cb-43ca-ac5f-2bfe83709152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f70ccd6b-a360-4e0f-9f3a-a462d4bbfdbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c169d8-8124-4f59-99c4-f29795d75357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f70ccd6b-a360-4e0f-9f3a-a462d4bbfdbd",
                    "LayerId": "a2b7441c-771d-46af-9a5f-bbfdca2e6adc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "a2b7441c-771d-46af-9a5f-bbfdca2e6adc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "213549c5-449a-448f-8995-bee565f79b46",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}