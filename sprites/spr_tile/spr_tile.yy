{
    "id": "e3efacda-b49a-4344-ac58-0c9d6275c556",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 10,
    "bbox_right": 767,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a04dbeb9-94f4-4ad9-8475-4583aad6d2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3efacda-b49a-4344-ac58-0c9d6275c556",
            "compositeImage": {
                "id": "d774fb5a-1cb3-497e-ba47-af35cb28afdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04dbeb9-94f4-4ad9-8475-4583aad6d2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900a63b7-8194-49e6-94b1-fc039614cb3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04dbeb9-94f4-4ad9-8475-4583aad6d2c1",
                    "LayerId": "fceeb7e9-f900-4430-9d3f-3a009de27c0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "fceeb7e9-f900-4430-9d3f-3a009de27c0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3efacda-b49a-4344-ac58-0c9d6275c556",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 768,
    "xorig": 587,
    "yorig": 160
}