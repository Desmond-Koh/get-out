{
    "id": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 7,
    "bbox_right": 54,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9faa4280-6816-44aa-b6ec-a2b176c5efb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "6cf59661-40d4-4eb1-baf0-94abea0da819",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9faa4280-6816-44aa-b6ec-a2b176c5efb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d892d6-7d80-4b55-a298-cbffb0c2d71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9faa4280-6816-44aa-b6ec-a2b176c5efb7",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "7c8d8806-2c00-4a36-a333-cb48307f4a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "ada6b715-95b1-46d6-acbe-b4b9a21d678d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8d8806-2c00-4a36-a333-cb48307f4a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb908bb-2d46-4d34-82c5-03baf642f204",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8d8806-2c00-4a36-a333-cb48307f4a72",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "2e474159-eb39-42e9-823f-35c3f9d49213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "4ca6e7aa-b1b4-4311-8d64-b7ab76c54cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e474159-eb39-42e9-823f-35c3f9d49213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da67c9b2-9e15-4866-8071-9c90d4c50738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e474159-eb39-42e9-823f-35c3f9d49213",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "9fa55b57-e9b2-40bf-974f-27ef47d4a44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "03e1df9e-e07f-4b5b-bf7a-771b08a8dfd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa55b57-e9b2-40bf-974f-27ef47d4a44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74a910f-49ee-4cf5-aef0-b881bc625399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa55b57-e9b2-40bf-974f-27ef47d4a44d",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "9c68fa95-d646-4c4f-a47c-d06a46894dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "5d849878-dcc6-4028-aa9f-faacd1c88a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c68fa95-d646-4c4f-a47c-d06a46894dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5d3168f-a52b-4917-aa3b-e6ec1a060325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c68fa95-d646-4c4f-a47c-d06a46894dae",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "bf7a4429-3900-4413-b87f-fd6c6eb42916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "9ed9326e-f27f-42d3-b774-4666dca7b93e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf7a4429-3900-4413-b87f-fd6c6eb42916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21828981-bd76-4afb-a7f8-f3fb42cbfd64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf7a4429-3900-4413-b87f-fd6c6eb42916",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        },
        {
            "id": "4f93eec3-d29a-4a25-a837-b45c563f1180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "compositeImage": {
                "id": "9c5a253b-c054-431f-aafb-1d10b5c8f5f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f93eec3-d29a-4a25-a837-b45c563f1180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e2f8290-654e-44d8-858e-138ad09dd800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f93eec3-d29a-4a25-a837-b45c563f1180",
                    "LayerId": "a19e3018-9406-40ba-a724-48c6af46376a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a19e3018-9406-40ba-a724-48c6af46376a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}