{
    "id": "a23f1284-1250-46b7-ae30-dd3290781aba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_school",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1949,
    "bbox_left": 0,
    "bbox_right": 4499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9532b403-f6dc-4cef-aae5-89ce5efbd1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a23f1284-1250-46b7-ae30-dd3290781aba",
            "compositeImage": {
                "id": "bd8ae10f-fdd3-4b5b-b1b8-3f86fb816947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9532b403-f6dc-4cef-aae5-89ce5efbd1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43039d91-3379-42f3-ac59-a59436b180fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9532b403-f6dc-4cef-aae5-89ce5efbd1a5",
                    "LayerId": "9298acad-e28a-4f62-98c0-9c939a7e9180"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1950,
    "layers": [
        {
            "id": "9298acad-e28a-4f62-98c0-9c939a7e9180",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a23f1284-1250-46b7-ae30-dd3290781aba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4500,
    "xorig": 2250,
    "yorig": 975
}