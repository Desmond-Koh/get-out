{
    "id": "90748a0a-adb9-40ca-8b30-52688aa46435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_timingOutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 108,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e52e74e7-105d-4793-b163-94b13c151b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90748a0a-adb9-40ca-8b30-52688aa46435",
            "compositeImage": {
                "id": "b05cc7ad-cef7-45c0-91d9-db6e29f687be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e52e74e7-105d-4793-b163-94b13c151b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de01c72-16cd-464c-bc33-411ae67a47d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e52e74e7-105d-4793-b163-94b13c151b9c",
                    "LayerId": "81b3ec5f-13a9-4039-a3af-5a35219ce7e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "81b3ec5f-13a9-4039-a3af-5a35219ce7e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90748a0a-adb9-40ca-8b30-52688aa46435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 0,
    "yorig": 10
}