{
    "id": "4b97d0ad-1c6a-4b6b-9914-10d06fce7019",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backstamina",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08f9a1ad-a3fe-4ab6-a1b1-afcacccbbf0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b97d0ad-1c6a-4b6b-9914-10d06fce7019",
            "compositeImage": {
                "id": "5db948d7-9cd6-47fc-8e8a-de9e2e54af71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f9a1ad-a3fe-4ab6-a1b1-afcacccbbf0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3007d291-1228-47de-80ef-b25537711611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f9a1ad-a3fe-4ab6-a1b1-afcacccbbf0b",
                    "LayerId": "31d86508-0186-4b71-9562-30fe345940d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "31d86508-0186-4b71-9562-30fe345940d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b97d0ad-1c6a-4b6b-9914-10d06fce7019",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}