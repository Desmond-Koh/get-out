{
    "id": "f52dd3eb-4124-4c5b-9c2a-72f948638d2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ff4b709-add2-4411-a50f-69c0e5876c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f52dd3eb-4124-4c5b-9c2a-72f948638d2a",
            "compositeImage": {
                "id": "1fb0ab06-df47-47ed-9d81-8590ebe9f72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff4b709-add2-4411-a50f-69c0e5876c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac14bed8-425f-48c5-af9c-68a433922497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff4b709-add2-4411-a50f-69c0e5876c37",
                    "LayerId": "a3c2f447-c8e4-49c5-898b-835ac6a55935"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a3c2f447-c8e4-49c5-898b-835ac6a55935",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f52dd3eb-4124-4c5b-9c2a-72f948638d2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}