{
    "id": "52acf200-c60d-48a6-bebe-83446f3923eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "anna_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bfa0985-7d0a-46a4-96f4-79904f3f1908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
            "compositeImage": {
                "id": "c5238123-81ae-478e-81a7-21dc1d804e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bfa0985-7d0a-46a4-96f4-79904f3f1908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0971d43-e4c5-4001-b385-1044dc8add9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bfa0985-7d0a-46a4-96f4-79904f3f1908",
                    "LayerId": "ef23f0b4-a30c-4ebe-8cd4-26681e7ee87c"
                }
            ]
        },
        {
            "id": "400fcd32-5c57-4263-8afc-7f578daeb115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
            "compositeImage": {
                "id": "03db41d2-721d-4c9d-9cde-09ee2d6487b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400fcd32-5c57-4263-8afc-7f578daeb115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58339ec4-4d19-4d50-8564-70229328b0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400fcd32-5c57-4263-8afc-7f578daeb115",
                    "LayerId": "ef23f0b4-a30c-4ebe-8cd4-26681e7ee87c"
                }
            ]
        },
        {
            "id": "d0c1d785-74f4-4e30-88d3-84c04c0a690d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
            "compositeImage": {
                "id": "edf32caf-279f-4f61-94df-870609883669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c1d785-74f4-4e30-88d3-84c04c0a690d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad29da02-e6c1-43e4-8175-834f761ee948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c1d785-74f4-4e30-88d3-84c04c0a690d",
                    "LayerId": "ef23f0b4-a30c-4ebe-8cd4-26681e7ee87c"
                }
            ]
        },
        {
            "id": "08365e42-6503-4c9a-a672-0840a300ca67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
            "compositeImage": {
                "id": "a4b71232-4618-4ea9-af7f-a6d901ab130c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08365e42-6503-4c9a-a672-0840a300ca67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "083453b1-6592-4cbf-a213-f8420ba17934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08365e42-6503-4c9a-a672-0840a300ca67",
                    "LayerId": "ef23f0b4-a30c-4ebe-8cd4-26681e7ee87c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "ef23f0b4-a30c-4ebe-8cd4-26681e7ee87c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}