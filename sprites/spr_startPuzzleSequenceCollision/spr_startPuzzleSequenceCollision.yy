{
    "id": "b8b02ad3-b795-4550-824c-f165290c1268",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startPuzzleSequenceCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 81,
    "bbox_right": 83,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dfcea0f-876d-414d-be82-f1f819cf84d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8b02ad3-b795-4550-824c-f165290c1268",
            "compositeImage": {
                "id": "fbf27b74-9ada-40b9-a970-84d6c5be5b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfcea0f-876d-414d-be82-f1f819cf84d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159841c1-4949-4d7d-86de-ab6bad15f2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfcea0f-876d-414d-be82-f1f819cf84d7",
                    "LayerId": "9a47d96f-37c2-43ca-87e2-21f19a025708"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "9a47d96f-37c2-43ca-87e2-21f19a025708",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8b02ad3-b795-4550-824c-f165290c1268",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}