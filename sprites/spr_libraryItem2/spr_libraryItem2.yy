{
    "id": "a2c16e06-f9a5-49cc-8368-76d1df022c59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_libraryItem2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 823,
    "bbox_left": 5,
    "bbox_right": 873,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e1bbf7e-f975-4156-a4c5-2037f38be8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2c16e06-f9a5-49cc-8368-76d1df022c59",
            "compositeImage": {
                "id": "ef247d23-55ef-421b-a3c7-31e928047248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1bbf7e-f975-4156-a4c5-2037f38be8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f8458e7-efd2-45fe-a94e-6005b1d99a72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1bbf7e-f975-4156-a4c5-2037f38be8c0",
                    "LayerId": "42417e69-f039-4709-a30a-bdeb71bcb37d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 876,
    "layers": [
        {
            "id": "42417e69-f039-4709-a30a-bdeb71bcb37d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2c16e06-f9a5-49cc-8368-76d1df022c59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}