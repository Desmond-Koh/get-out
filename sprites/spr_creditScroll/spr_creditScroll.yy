{
    "id": "29b445ff-c807-4067-81fc-04fc9524bfde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creditScroll",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1994,
    "bbox_left": 163,
    "bbox_right": 801,
    "bbox_top": 88,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "230f8d1f-6c3b-471b-8fbe-3ae5dcd3c361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29b445ff-c807-4067-81fc-04fc9524bfde",
            "compositeImage": {
                "id": "e67cd910-7b60-46fe-b95c-ef373169ee69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "230f8d1f-6c3b-471b-8fbe-3ae5dcd3c361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e8660c-eb20-4c5f-96cb-aa71706c12c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "230f8d1f-6c3b-471b-8fbe-3ae5dcd3c361",
                    "LayerId": "07604069-1734-4770-911d-427e4a69e6b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3000,
    "layers": [
        {
            "id": "07604069-1734-4770-911d-427e4a69e6b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29b445ff-c807-4067-81fc-04fc9524bfde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 0
}