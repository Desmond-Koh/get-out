{
    "id": "778b1673-8a20-49e4-8b6d-80f3d1f2f349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_libraryItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 594,
    "bbox_left": 5,
    "bbox_right": 623,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42dd070a-3677-4286-a34e-ba96db85f284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "778b1673-8a20-49e4-8b6d-80f3d1f2f349",
            "compositeImage": {
                "id": "a8051603-ef66-47a7-906b-22d1c72feabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42dd070a-3677-4286-a34e-ba96db85f284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c291f278-75cb-485c-b01a-3135e5c74b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42dd070a-3677-4286-a34e-ba96db85f284",
                    "LayerId": "616a7c94-5a6c-47ca-a011-5496b443e021"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 876,
    "layers": [
        {
            "id": "616a7c94-5a6c-47ca-a011-5496b443e021",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "778b1673-8a20-49e4-8b6d-80f3d1f2f349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}