{
    "id": "985f5a0a-ea79-48f5-b6de-9b4b26246ed3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bookControl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 6,
    "bbox_right": 194,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2420b6f1-0e3f-40cb-b84e-bd486215b76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "985f5a0a-ea79-48f5-b6de-9b4b26246ed3",
            "compositeImage": {
                "id": "3831c747-44d9-4fa8-a979-c3b260934adf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2420b6f1-0e3f-40cb-b84e-bd486215b76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7911256f-3229-4c4d-ad8d-be9b07066a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2420b6f1-0e3f-40cb-b84e-bd486215b76e",
                    "LayerId": "c328e719-ec8b-480b-a11d-58393f688a96"
                }
            ]
        },
        {
            "id": "36959913-e853-417e-982e-d70a4e8b1ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "985f5a0a-ea79-48f5-b6de-9b4b26246ed3",
            "compositeImage": {
                "id": "86c2d2cf-cb72-4782-8d65-e4e26a624bca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36959913-e853-417e-982e-d70a4e8b1ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c0b664-e79c-4c40-a620-d6d6b3db5e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36959913-e853-417e-982e-d70a4e8b1ee4",
                    "LayerId": "c328e719-ec8b-480b-a11d-58393f688a96"
                }
            ]
        },
        {
            "id": "61b21e8a-cfad-49c3-b502-b75ffb301d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "985f5a0a-ea79-48f5-b6de-9b4b26246ed3",
            "compositeImage": {
                "id": "b600f21d-2885-4d1f-89e9-11ae2749491e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b21e8a-cfad-49c3-b502-b75ffb301d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac686ee-e723-4ccb-9d99-d610a6b53256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b21e8a-cfad-49c3-b502-b75ffb301d1f",
                    "LayerId": "c328e719-ec8b-480b-a11d-58393f688a96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "c328e719-ec8b-480b-a11d-58393f688a96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "985f5a0a-ea79-48f5-b6de-9b4b26246ed3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}