{
    "id": "b4f1b080-4d72-49d8-bdc2-de35f92ba261",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_code_paper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 264,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dfd7c1d-1da7-4a6b-82c1-52da902cca90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4f1b080-4d72-49d8-bdc2-de35f92ba261",
            "compositeImage": {
                "id": "3ab2601f-6c16-4cf6-979e-c2f78f3b0048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dfd7c1d-1da7-4a6b-82c1-52da902cca90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46a1ba8-e750-4bb0-aed8-7596a73e60c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dfd7c1d-1da7-4a6b-82c1-52da902cca90",
                    "LayerId": "48b405e2-a3a5-4d1f-8c56-7264a1da4ba2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 265,
    "layers": [
        {
            "id": "48b405e2-a3a5-4d1f-8c56-7264a1da4ba2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4f1b080-4d72-49d8-bdc2-de35f92ba261",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 132
}