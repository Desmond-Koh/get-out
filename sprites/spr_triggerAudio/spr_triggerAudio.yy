{
    "id": "cf89f48e-6526-4447-8feb-6eb6496c0dff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_triggerAudio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dccaa06-24de-4ef7-95de-69976c3b75e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf89f48e-6526-4447-8feb-6eb6496c0dff",
            "compositeImage": {
                "id": "9f93a835-11c8-4cb3-8af4-09a52166ee02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dccaa06-24de-4ef7-95de-69976c3b75e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6576f7e5-1d22-4794-8569-9311e1c3c968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dccaa06-24de-4ef7-95de-69976c3b75e3",
                    "LayerId": "311a9799-c6f7-4bba-9597-acabe8237865"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "311a9799-c6f7-4bba-9597-acabe8237865",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf89f48e-6526-4447-8feb-6eb6496c0dff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}