{
    "id": "b09fb8ab-2f68-4c75-b0ba-d822b83ddb5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardCollision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0525bc4-7480-4dfb-8f0f-c7dcf4298954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09fb8ab-2f68-4c75-b0ba-d822b83ddb5c",
            "compositeImage": {
                "id": "445aca29-9254-4ec6-ae10-f9af88ba7cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0525bc4-7480-4dfb-8f0f-c7dcf4298954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65417bc3-1269-4892-9e41-8f00282de795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0525bc4-7480-4dfb-8f0f-c7dcf4298954",
                    "LayerId": "24c6d7ff-cf6f-47ef-a589-57eeb5250bf8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24c6d7ff-cf6f-47ef-a589-57eeb5250bf8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b09fb8ab-2f68-4c75-b0ba-d822b83ddb5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}