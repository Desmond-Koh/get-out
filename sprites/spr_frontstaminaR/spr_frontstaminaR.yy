{
    "id": "5193c0d6-98ee-4948-b1da-1d246a61c403",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_frontstaminaR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8d6dbff-817f-4e9e-aa57-9c7cd442fd67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5193c0d6-98ee-4948-b1da-1d246a61c403",
            "compositeImage": {
                "id": "5dbf20a2-d2e7-4f5c-b7bb-1a75ac583093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8d6dbff-817f-4e9e-aa57-9c7cd442fd67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8056a61-808f-468e-bafd-6247b7669ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8d6dbff-817f-4e9e-aa57-9c7cd442fd67",
                    "LayerId": "e8a83e93-4aa9-42d5-8081-1d3d1f4338ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "e8a83e93-4aa9-42d5-8081-1d3d1f4338ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5193c0d6-98ee-4948-b1da-1d246a61c403",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 0,
    "yorig": 0
}