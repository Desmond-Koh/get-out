{
    "id": "574a1cf2-30e2-443a-b4b2-ff4f65835aa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31ceafcb-3c36-440d-8323-2a5558cc3c5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "574a1cf2-30e2-443a-b4b2-ff4f65835aa8",
            "compositeImage": {
                "id": "f5728d5c-0631-4218-acdd-e316a1eafe74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ceafcb-3c36-440d-8323-2a5558cc3c5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77557d6e-3a10-416c-ad49-5382c283288f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ceafcb-3c36-440d-8323-2a5558cc3c5f",
                    "LayerId": "63b89158-7c26-47ab-89c6-a235e463eba5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "63b89158-7c26-47ab-89c6-a235e463eba5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "574a1cf2-30e2-443a-b4b2-ff4f65835aa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}