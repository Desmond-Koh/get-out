{
    "id": "5e19d955-6b0d-414a-ac70-41008e0f489e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_youDied",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 467,
    "bbox_left": 324,
    "bbox_right": 635,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f9ce4d0-0b55-40b4-990d-8d16abfcd506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e19d955-6b0d-414a-ac70-41008e0f489e",
            "compositeImage": {
                "id": "551dd390-f9d7-4b9f-884b-c048002e1bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f9ce4d0-0b55-40b4-990d-8d16abfcd506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c169ec7-b009-4081-9286-09e447471e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f9ce4d0-0b55-40b4-990d-8d16abfcd506",
                    "LayerId": "69ee94ab-9f9d-4005-b32f-cba5dfac03d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "69ee94ab-9f9d-4005-b32f-cba5dfac03d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e19d955-6b0d-414a-ac70-41008e0f489e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 270
}