{
    "id": "fe308fc5-1686-4ebd-a1a2-64cc9ee006fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cupboardControl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "087d44ab-21a5-4a9c-a05c-6acabb756625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe308fc5-1686-4ebd-a1a2-64cc9ee006fd",
            "compositeImage": {
                "id": "ba1f93cf-3d40-48d0-bc42-c3ffffc76c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "087d44ab-21a5-4a9c-a05c-6acabb756625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d846847-0804-46a6-987f-8246c7b8ed01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "087d44ab-21a5-4a9c-a05c-6acabb756625",
                    "LayerId": "680995b1-10d3-4763-a52f-20e73d3a4905"
                }
            ]
        },
        {
            "id": "e7c31656-7659-4552-878b-153178114c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe308fc5-1686-4ebd-a1a2-64cc9ee006fd",
            "compositeImage": {
                "id": "ff41b8cc-d6a5-4413-9ed6-687b2d97b5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c31656-7659-4552-878b-153178114c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e516d60-cefc-4bf1-af6d-fb7ff88c8afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c31656-7659-4552-878b-153178114c3c",
                    "LayerId": "680995b1-10d3-4763-a52f-20e73d3a4905"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "680995b1-10d3-4763-a52f-20e73d3a4905",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe308fc5-1686-4ebd-a1a2-64cc9ee006fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}