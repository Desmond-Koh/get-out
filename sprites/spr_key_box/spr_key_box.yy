{
    "id": "0ae30ae3-aed1-4b44-bd5c-f5aa3775639b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 0,
    "bbox_right": 167,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf82f69e-a1ef-4de0-99de-f19ac26e83b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ae30ae3-aed1-4b44-bd5c-f5aa3775639b",
            "compositeImage": {
                "id": "ef8d5ed3-5460-45f5-aa6b-767d3f62c6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf82f69e-a1ef-4de0-99de-f19ac26e83b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b342062-6bdb-4bf1-bdf7-03f9fce6dd89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf82f69e-a1ef-4de0-99de-f19ac26e83b9",
                    "LayerId": "f381f87b-2de8-43e5-852f-66117836ffd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 137,
    "layers": [
        {
            "id": "f381f87b-2de8-43e5-852f-66117836ffd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ae30ae3-aed1-4b44-bd5c-f5aa3775639b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 68
}