/// @desc Previous Page

if(currPage > 0 && global.isEscKeyPressed == false){
	currPage--;
	audio_play_sound(snd_bookPageFlip, 10, false);
}