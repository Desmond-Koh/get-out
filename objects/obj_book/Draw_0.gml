/// @desc Book

draw_sprite_ext(spr_book, currPage, x, y-20, 1, 1, 0, c_white, 1);

if(currPage == 0){
	draw_sprite_ext(spr_bookControls, 0, x+330, y-200, 1, 1, 0, c_white, 1);
}
else if(currPage == 3){
	draw_sprite_ext(spr_bookControls, 2, x+330, y-200, 1, 1, 0, c_white, 1);
}
else{
	draw_sprite_ext(spr_bookControls, 1, x+330, y-200, 1, 1, 0, c_white, 1);
}
