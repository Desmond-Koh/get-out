{
    "id": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_book",
    "eventList": [
        {
            "id": "31727ef9-8e74-4aa2-b01a-4c9f4ea52f30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "00da11d6-7a82-4e66-80f7-58b49775e15f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "e021ac27-0a1c-4364-86bf-8ceadfd1e893",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 9,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "51d972a4-65ae-4cc8-80e9-b8e083e988e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "b1c452f4-dd61-4104-b316-5cff41905e1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 9,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "706e5283-0628-4c26-a942-207f8753459e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "6fc93828-eb4b-4ef3-aa21-3a23b94af55d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        },
        {
            "id": "325a2f3c-ef71-47f2-99f6-282f24e7917b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 69,
            "eventtype": 9,
            "m_owner": "f7264ec3-f156-4f8c-a1bf-ea56a79e7a6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "933f37a3-8be3-42ce-a54f-b38edf2d8ca5",
    "visible": true
}