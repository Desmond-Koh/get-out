{
    "id": "a25eb04a-0963-4c17-abba-4a446ca495f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_showTextOnLight",
    "eventList": [
        {
            "id": "3413fb2e-88f7-4c2a-8dae-095d89a9e034",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a25eb04a-0963-4c17-abba-4a446ca495f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95d110c9-2a8f-4b93-b493-d8cca06957de",
    "visible": true
}