x += (xTo - x) / 25;
y += (yTo - y) / 25;

if (follow != noone)
{
	xTo = follow.x;
	yTo = follow.y;
}

//found blake's dialogue
var vm = matrix_build_lookat(x,y,-10,x,y,0,0,1,0);
camera_set_view_mat(camera, vm);

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_ghostEncounterDialogue, false, false)){
	global.gameState = 1;
	instance_activate_object(obj_dialoguebox);
	alarm[1] = 60;
	alarm[0] = 300;
}

if(global.gameState = 1 && alarm[0] == 0)
{
	global.gameState = 2;
	instance_activate_object(obj_dialoguebox);
}

//found door dialogue
if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_foundDoor, false, false)){
	global.gameState = 3;
	instance_activate_object(obj_dialoguebox);
	alarm[2] = 60;
	alarm[0] = 300;	
}

if(alarm[2] > 1) //while camera is not on player, player cannot move
{
	obj_player.sprite_index = spr_player_up;
	global.moveState = false;	
}

if(global.gameState = 3 && alarm[0] == 0)
{
	global.gameState = 4;
	instance_activate_object(obj_dialoguebox);
}