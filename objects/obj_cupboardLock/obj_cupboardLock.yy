{
    "id": "3d9c3ce3-6777-4ee4-9901-f2e6e2506f74",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cupboardLock",
    "eventList": [
        {
            "id": "2032c8d1-1742-4900-80cc-ea8351227690",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 9,
            "m_owner": "3d9c3ce3-6777-4ee4-9901-f2e6e2506f74"
        },
        {
            "id": "a92af294-05e9-4943-983e-716ef15534ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 9,
            "m_owner": "3d9c3ce3-6777-4ee4-9901-f2e6e2506f74"
        },
        {
            "id": "ebc936c0-f0ae-466e-835b-263f49132325",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d9c3ce3-6777-4ee4-9901-f2e6e2506f74"
        },
        {
            "id": "5a243168-67d6-43d4-84fd-49149c88a837",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 9,
            "m_owner": "3d9c3ce3-6777-4ee4-9901-f2e6e2506f74"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eeed901c-a205-46c9-8f27-a9386dbf2640",
    "visible": true
}