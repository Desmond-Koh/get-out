/// @desc Attempt Unlock on a cupboard
if(global.isEscKeyPressed == false){
	if!(currNum == lockNumPos1 || currNum == lockNumNeg1 || currNum == lockNumPos2 || currNum == lockNumNeg2){
		lockCorrectAttempt = 0;
		audio_play_sound(snd_cupboardLockFail, 10, false);
	}

	//Check Cupboard Lock 1 Number
	if(currNum == lockNumPos1 || currNum == lockNumNeg1){
		audio_play_sound(snd_cupboardLockFail, 10, false);
		if(lockCorrectAttempt == 0){
			lockCorrectAttempt += 1;
		}
		else{
			lockCorrectAttempt = 0;
		}
	}

	//Check Cupboard Lock 2 Number
	if(currNum == lockNumPos2 || currNum == lockNumNeg2){
		audio_play_sound(snd_cupboardLockFail, 10, false);
		if(lockCorrectAttempt == 1){
			lockCorrectAttempt += 1;
		}
		else{
			lockCorrectAttempt = 0;
		}
	}

	if(lockCorrectAttempt == 2){
		audio_play_sound(snd_cupboardLockSuccess, 10, false);
		instance_destroy();
		instance_destroy(global.CupboardId);			
		global.CupboardOpenId = instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemOverlay", obj_cupboardOpen)
	}
}