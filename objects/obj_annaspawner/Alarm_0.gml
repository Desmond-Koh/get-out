playerX = obj_player.x;
playerY = obj_player.y;

spawnRandomizer = irandom_range(1,4);
giggleRandomizer = irandom_range(1,3);

if(global.annaSpawned == false && global.moveState == true && global.gameState >= 4 && global.PlayerDie == false && global.inSickbay = false)
{
	//spawn anna in 4 random directions
	if(spawnRandomizer == 1) 
	{
		if(collision_rectangle(playerX-20, playerY-180,playerX-20, playerY-180, obj_void, false, false)){	
			global.annaId = instance_create_layer(playerX-20, playerY+120,"Inst_Ghost",obj_anna); //spawn below of player
			obj_anna.sprite_index = spr_anna_up;
		}
		else{
			global.annaId = instance_create_layer(playerX-20, playerY-180,"Inst_Ghost",obj_anna); //spawn above of player
			obj_anna.sprite_index = spr_anna_down;
		}	
	}
	else if(spawnRandomizer == 2)
	{	
		if(collision_rectangle(playerX-20, playerY+120,playerX-20, playerY+120, obj_void, false, false)){
			global.annaId = instance_create_layer(playerX-20, playerY-180,"Inst_Ghost",obj_anna); //spawn above of player
			obj_anna.sprite_index = spr_anna_down;
		}
		else{
			global.annaId = instance_create_layer(playerX-20, playerY+120,"Inst_Ghost",obj_anna); //spawn below of player
			obj_anna.sprite_index = spr_anna_up;
		}
	}
	else if(spawnRandomizer == 3)
	{
		if(collision_rectangle(playerX-160, playerY-35,playerX-160, playerY-35, obj_void, false, false)){	
			global.annaId = instance_create_layer(playerX+120, playerY-35,"Inst_Ghost",obj_anna);
			obj_anna.sprite_index = spr_anna_left;
		}
		else
		{
			global.annaId = instance_create_layer(playerX-160, playerY-35,"Inst_Ghost",obj_anna); //spawn left of player
			obj_anna.sprite_index = spr_anna_right;
		}	
	}
	else if(spawnRandomizer == 4)
	{
		if(collision_rectangle(playerX+120, playerY-35,playerX+120, playerY-35, obj_void, false, false)){
			global.annaId = instance_create_layer(playerX-160, playerY-35,"Inst_Ghost",obj_anna); //spawn left of player
			obj_anna.sprite_index = spr_anna_right;
		}
		else{
			global.annaId = instance_create_layer(playerX+120, playerY-35,"Inst_Ghost",obj_anna); //spawn right of player
			obj_anna.sprite_index = spr_anna_left;
		}
	}	
	
	//play random spawn noise
	if(giggleRandomizer == 1)
	{
		if(!audio_is_playing(snd_anna_giggle_1))
		{
			audio_play_sound(snd_anna_giggle_1, 20, false);
		}
	}
	else if(giggleRandomizer == 2)
	{
		if(!audio_is_playing(snd_anna_giggle_2))
		{
			audio_play_sound(snd_anna_giggle_2, 20, false);
		}
	}
	else if(giggleRandomizer == 3)
	{
		if(!audio_is_playing(snd_anna_giggle_3))
		{
			audio_play_sound(snd_anna_giggle_3, 20, false);
		}
	}	

}

alarm[0] = spawnRate;