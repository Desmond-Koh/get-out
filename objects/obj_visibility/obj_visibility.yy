{
    "id": "507e8f06-ada8-4276-8614-aab3883f9ce3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_visibility",
    "eventList": [
        {
            "id": "eac0b4db-69b3-4ae8-ae98-5c88efd9fd1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        },
        {
            "id": "3d5f1b45-be1e-4a80-ac59-a8f6c1eb2211",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        },
        {
            "id": "30e398a6-f7e7-4cec-a41f-05170e95201c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        },
        {
            "id": "8b20344c-6722-42a9-b7ec-d66bf1766c41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        },
        {
            "id": "efd7af2a-3410-490d-abab-94c1b12dfd5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        },
        {
            "id": "e18f0569-e8ae-4fa8-83a3-d63b9c6ffb8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "507e8f06-ada8-4276-8614-aab3883f9ce3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}