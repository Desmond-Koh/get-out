x += (xTo - x) / 25;
y += (yTo - y) / 25;

if (follow != noone)
{
	xTo = follow.x;
	yTo = follow.y;
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_ghostEncounterDialogue, false, false)){
	alarm[1] = 60;
	alarm[0] = 300;
	instance_destroy(obj_ghostEncounterDialogue);
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_foundDoor, false, false)){
	alarm[2] = 60;
	alarm[0] = 300;
	instance_destroy(obj_foundDoor);
}

