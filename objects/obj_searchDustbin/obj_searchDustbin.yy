{
    "id": "6e50e0d3-0067-4508-99bf-4966dd79726b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_searchDustbin",
    "eventList": [
        {
            "id": "a41a1874-25e7-49c0-ad1f-0b528c5e0569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e50e0d3-0067-4508-99bf-4966dd79726b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b09fb8ab-2f68-4c75-b0ba-d822b83ddb5c",
    "visible": false
}