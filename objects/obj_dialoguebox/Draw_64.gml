/// @desc Dialogue Box
GameState = "";
if(global.gameState == 0)
{
	GameState = initialDialogue;
}
else if(global.gameState == 1)
{
	GameState = encounterGhostDialogue;
}
else if(global.gameState == 2)
{
	GameState = aftEncounterGhostDialogue;
}
else if(global.gameState == 3)
{
	GameState = foundDoorDialogue;
}
else if(global.gameState == 4)
{
	GameState = aftFoundDoorDialogue;
}
else if(global.gameState == 5)
{
	GameState = noKeyDoorDialogue;
}
else if(global.gameState == 6)
{
	GameState = basketballCourtDialogue;
}
else if(global.gameState == 7)
{
	GameState = foundAllKeyFragsDialogue;
}
else if(global.gameState == 8)
{
	GameState = playerRevive;
}
else if(global.gameState == 9)
{
	GameState = annaSpawned;
}

var c = dialoguebox_col;

draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(-1);
draw_sprite_ext(dialoguebox, 0, box_x, box_y, 1, 1, 0, c_white, 1);
draw_text_color(text_x, text_y, GameState[dialogueVar],c,c,c,c, 1);