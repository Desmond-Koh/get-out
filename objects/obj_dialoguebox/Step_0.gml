GameState = "";
if(global.gameState == 0)
{
	GameState = initialDialogue;
}
else if(global.gameState == 1)
{
	GameState = encounterGhostDialogue;
}
else if(global.gameState == 2)
{
	GameState = aftEncounterGhostDialogue;
}
else if(global.gameState == 3)
{
	GameState = foundDoorDialogue;
}
else if(global.gameState == 4)
{
	GameState = aftFoundDoorDialogue;
}
else if(global.gameState == 5)
{
	GameState = noKeyDoorDialogue;
}
else if(global.gameState == 6)
{
	GameState = basketballCourtDialogue;
}
else if(global.gameState == 7)
{
	GameState = foundAllKeyFragsDialogue;
}
else if(global.gameState == 8)
{
	GameState = playerRevive;
}
else if(global.gameState == 9)
{
	GameState = annaSpawned;
}

var dialogueArray = array_length_1d(GameState) - 1;
if(keyboard_check_pressed(interact_key) && global.isEscKeyPressed == false){
	if(page < dialogueArray){
		audio_play_sound(snd_dialogue, 10, false);
		dialogueVar++;
		page++;		
	}
	else{
		audio_play_sound(snd_dialogue, 10, false);
		page = 0;
		dialogueVar = 0;
		
		// Don't set move state to true after dialogue "encounter ghost dialogue' end.
		if(global.gameState != 1){
			global.moveState = true;
		}
		instance_deactivate_object(obj_dialoguebox);
	}
}