/// @desc Initialize 
dialoguebox = spr_dialoguebox;

box_x = 370;
box_y = 570;

x_buffer = 110;
y_buffer = 30;
text_x = box_x + x_buffer;
text_y = box_y + y_buffer;

//all dialogues
dialogueVar = 0;

//dialogues when starting game
initialDialogue[0] = "Huh? I overslept..";
initialDialogue[1] = "Where am I.. how did I end up \nhere?";
initialDialogue[2] = "It's late.. I need to get home.";

//dialogues when player exits classroom
encounterGhostDialogue[0] = "Eeeek! What's that?";
aftEncounterGhostDialogue[0] = "Wh-... what is that thing? That's no human!";
aftEncounterGhostDialogue[1] = "I need to get out of here!";

//dialogues when the player found an exit door
foundDoorDialogue[0] = "Hmm.. a door, i wonder where does it lead \nto.";
aftFoundDoorDialogue[0] = "Let's exit through that door.";

//dialogues when the player tries to open door without key
noKeyDoorDialogue[0] = "The door won't budge.";
noKeyDoorDialogue[1] = "Let's look around for a key.";

//dialogues when player triggers basketball court puzzle
basketballCourtDialogue[0] = "Di-.. did the lights just shut?!"
basketballCourtDialogue[1] = "I better avoid the dark.."
basketballCourtDialogue[2] = "I should try to fix the lights at the \nelectrical box."

//dialogues when player found all 3 key fragments
foundAllKeyFragsDialogue[0] = "It looks like all the key fragments are \nhere."
foundAllKeyFragsDialogue[1] = "I should be able to open the door now.."

//dialogues when player revives
playerRevive[0] = "Wh-.. What happened?! I don't remember \nanything..."
playerRevive[1] = "Am i in the sick bay?"

//dialogues when player revives
annaSpawned[0] = "Strange.. I can feel someone is watching me."

//initialize to start the first page
page = 0;
interact_key = ord("E");
dialoguebox_col = c_white;
