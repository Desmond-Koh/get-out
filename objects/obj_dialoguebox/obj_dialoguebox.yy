{
    "id": "40a71bd8-982a-425a-bdeb-34fb5c7f1df2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialoguebox",
    "eventList": [
        {
            "id": "3c351c1a-c9a7-4439-8330-41a217387631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40a71bd8-982a-425a-bdeb-34fb5c7f1df2"
        },
        {
            "id": "f674838e-4d4c-45f6-a0b8-6d6c266eae5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "40a71bd8-982a-425a-bdeb-34fb5c7f1df2"
        },
        {
            "id": "aef50915-600f-491c-bc88-432065bf312d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40a71bd8-982a-425a-bdeb-34fb5c7f1df2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}