{
    "id": "6941b20b-178f-4397-91e7-eb350ec6dbd2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main_menu",
    "eventList": [
        {
            "id": "70b51a5d-3488-4d56-8329-c54e9beef4ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6941b20b-178f-4397-91e7-eb350ec6dbd2"
        },
        {
            "id": "98e9e318-b2b7-446d-80d9-a2786fe63dc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6941b20b-178f-4397-91e7-eb350ec6dbd2"
        },
        {
            "id": "277a63fb-35c7-4c64-914a-7023463c83ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6941b20b-178f-4397-91e7-eb350ec6dbd2"
        },
        {
            "id": "214599cf-cf82-4089-b86a-ad781df5cd2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "6941b20b-178f-4397-91e7-eb350ec6dbd2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}