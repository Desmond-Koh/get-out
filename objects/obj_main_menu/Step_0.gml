menu_move = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);
if((menu_index+menu_move) == 1 && !file_exists("save")){
	menu_index += menu_move*2;
}
else{
	menu_index += menu_move
}


if(menu_index < 0){
	menu_index = buttons - 1;	
}

if(menu_index > buttons - 1){
	menu_index = 0;
}

if(menu_index != last_selected){
	audio_play_sound(snd_menuSelect,1,false);
}

last_selected = menu_index;

// Main menu music
if(audio_is_playing(snd_mainMenu) == false) 
{
	audio_play_sound(snd_mainMenu, 10, true);
}	