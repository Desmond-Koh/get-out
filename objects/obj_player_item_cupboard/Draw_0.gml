if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard1_instance, false, false) && global.isBookPickedUp == false && global.isCupboardOpen == true){
	draw_sprite_ext(spr_player_item, 0, obj_player.x-125, obj_player.y+60, 2, 1.4, 0, c_white, 1);
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard2_instance, false, false) && global.isEMFReaderPickedUp == false && global.isCupboardOpen == true){
	draw_sprite_ext(spr_player_item, 2, obj_player.x+125, obj_player.y-100, 1.5, 1, 0, c_white, 1);
}

