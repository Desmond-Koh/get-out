{
    "id": "1e36fdb2-91b8-42f9-8b3a-845f5e7b4ad4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_item_cupboard",
    "eventList": [
        {
            "id": "ab8b31f9-52db-44e1-bbff-ab3a9428b51c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e36fdb2-91b8-42f9-8b3a-845f5e7b4ad4"
        },
        {
            "id": "32242c20-f738-4cf8-98f5-d7066ff6d006",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1e36fdb2-91b8-42f9-8b3a-845f5e7b4ad4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56e7d09c-06bc-4851-91cd-76fe8f2d8460",
    "visible": true
}