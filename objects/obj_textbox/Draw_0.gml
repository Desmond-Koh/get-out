/// @description Textbox

if(global.textbox == "playeritem" || global.textbox == "fixLight"){	
	draw_sprite_ext(spr_textbox_playeritem, 0, x, y-1, 1, 1, 0, c_white, 1);
}
else{
	draw_sprite_ext(spr_textbox_cupboard, 0, x, y-1, 1, 1, 0, c_white, 1);
}

draw_set_font(fnt_text);
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text_color(x, y, text, c_white, c_white, c_white, c_white, 1);