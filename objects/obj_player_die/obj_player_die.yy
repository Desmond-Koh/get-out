{
    "id": "53732384-1d1b-41d6-b50d-635b3fc7e5f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_die",
    "eventList": [
        {
            "id": "54a8ba8b-c83e-488f-8318-fe808c43923d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53732384-1d1b-41d6-b50d-635b3fc7e5f1"
        },
        {
            "id": "63c6f619-9351-442c-989a-63dfa24dcb80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "53732384-1d1b-41d6-b50d-635b3fc7e5f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5e19d955-6b0d-414a-ac70-41008e0f489e",
    "visible": true
}