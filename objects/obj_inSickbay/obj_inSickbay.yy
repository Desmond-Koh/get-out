{
    "id": "913889c3-5487-4d95-9af5-0ef3fa5f6e92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inSickbay",
    "eventList": [
        {
            "id": "3003e7bb-1e09-4c5f-87c7-a9f2217a9a67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "913889c3-5487-4d95-9af5-0ef3fa5f6e92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c01ac20-90bb-43b9-a263-c28c0722bc77",
    "visible": false
}