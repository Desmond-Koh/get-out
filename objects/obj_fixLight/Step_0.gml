if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, obj_fixLight, false, false)){
	if(fixLight == noone){
		global.textbox = "fixLight";
		fixLight = instance_create_layer(x, y, "Inst_Text", obj_textbox)
	}
	
	if(keyboard_check_pressed(ord("E")) && timingId == noone){
		timingId = instance_create_layer(obj_player.x, obj_player.y, "Inst_Timing", obj_fixLightTiming)
		if(fixLight != noone){
			instance_destroy(fixLight);
		}	
	}
}
else{
	if(fixLight != noone)
	{
		instance_destroy(fixLight);
		fixLight = noone;
	}
	
	instance_destroy(timingId)
	timingId = noone;
}