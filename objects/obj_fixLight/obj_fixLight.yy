{
    "id": "eb14200d-fc56-4ba6-98c3-6757ee65513b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fixLight",
    "eventList": [
        {
            "id": "f91b567e-63ac-47c2-a830-043e38bdfa40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eb14200d-fc56-4ba6-98c3-6757ee65513b"
        },
        {
            "id": "96064ae7-3d07-4d5a-8908-b56c0b0f7b3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb14200d-fc56-4ba6-98c3-6757ee65513b"
        },
        {
            "id": "41b1c10a-cc58-41c5-8aa5-f1e26e06a240",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eb14200d-fc56-4ba6-98c3-6757ee65513b"
        }
    ],
    "maskSpriteId": "17e1a879-bf02-461e-96d9-fa6c309efef5",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}