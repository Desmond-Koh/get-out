{
    "id": "50cf51eb-0e09-4641-9439-680499a294a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_resetVisibility",
    "eventList": [
        {
            "id": "48fe6329-a2ae-4971-a827-0516ad1bee36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "50cf51eb-0e09-4641-9439-680499a294a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "17e1a879-bf02-461e-96d9-fa6c309efef5",
    "visible": false
}