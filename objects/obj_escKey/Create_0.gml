global.moveState = false;
global.isEscKeyPressed = true;
audio_play_sound(snd_menuSelect,1,false);
audio_pause_sound(snd_InGameBGM);

startX = (view_wport[0] / 2) - (sprite_get_width(spr_player_item) * 2.5) + 160;
startY = view_hport[0] - (sprite_get_height(spr_player_item) / 2) - 350;

button_h = 48;

indentation = "       ";

button[0] = indentation + "RETURN";
button[1] = indentation + "SAVE";
button[2] = indentation + "MAIN MENU";
button[3] = indentation + "EXIT";
buttons = array_length_1d(button);

menu_index = 0;
last_selected = 0;


