{
    "id": "daa50092-15e4-479e-9d1b-2708432666b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_escKey",
    "eventList": [
        {
            "id": "9d51ce18-9917-471a-b0ac-7372b15a82e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "daa50092-15e4-479e-9d1b-2708432666b2"
        },
        {
            "id": "e0b8bd07-53e2-4b1f-b9f5-ab803fe0b740",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "daa50092-15e4-479e-9d1b-2708432666b2"
        },
        {
            "id": "5ca0b8d3-c4ff-4eef-896b-e3695ba5bd77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "daa50092-15e4-479e-9d1b-2708432666b2"
        },
        {
            "id": "d046a464-e069-467d-ab55-e0c695c00a68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "daa50092-15e4-479e-9d1b-2708432666b2"
        },
        {
            "id": "08252265-beda-42ad-af5c-0a4567156f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "daa50092-15e4-479e-9d1b-2708432666b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}