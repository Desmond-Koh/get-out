audio_stop_all();
instance_destroy();
	
if(menu_index == 0){
	instance_destroy();
}
else if(menu_index == 1){
	game_save("save");
	instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_gameSaveNotification)
}
else if(menu_index == 2){
	room_goto(rm_main);
}
else if(menu_index == 3){
	game_end();
}