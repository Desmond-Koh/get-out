draw_sprite_ext(spr_escKey, 0, startX, startY+10,1.4,1.4,0,c_white, 1)

var i = 0;
repeat(buttons)
{
	draw_set_font(fnt_escKey);
	draw_set_halign(fa_center);
	draw_set_color(c_ltgray);
	draw_set_alpha(0.7)
	if(menu_index == i)
	{
		draw_set_color(c_red);
	}
	
	draw_text(startX-28, startY-60 + button_h * i, button[i]);
	i++;
}
