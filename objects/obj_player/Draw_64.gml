/// @desc HUD

if(global.PlayerDie == false){
	startInvX = (view_wport[0] / 2) - (sprite_get_width(spr_player_item) * 2.5)
	startInvY = view_hport[0] - (sprite_get_height(spr_player_item) / 2);

	//Inventory HUD
	for (var inv = 0; inv < maxInvSlots; inv++)
	{
		itemX = startInvX + (inv * sprite_get_width(spr_player_item)) -30;
		itemY = startInvY + 30;

		if(inv == inv_selected){
			draw_sprite_ext(spr_inv_box, 1, itemX-60, itemY, 1, 1, 0, c_white, 1);
		}
		else{
			draw_sprite_ext(spr_inv_box, 0, itemX-60, itemY, 1, 1, 0, c_white, 1);
		}
		
		item = global.array_inv[inv];
		if(item > -1)
		{
			draw_sprite_ext(spr_player_item, item,  itemX+30, itemY-50, 1, 1, 0, c_white, 1);
		}
		
		draw_set_font(-1)
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		draw_text_color(itemX+45, itemY-78, inv+1, c_gray,c_gray,c_gray,c_gray,1)
	}
	
	//Stamina HUD
	draw_sprite_ext(spr_staminabarOutline, 1, itemX+432, itemY-77, 1, 1, 0, c_white, 1);
	if(global.stamina >= 80)
	{
		draw_sprite_ext(spr_backstamina,1,itemX+298, itemY-63,
		 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_frontstamina,1,itemX+298, itemY-63,
		max(0,global.stamina/100),1,0,c_white,1);
	}
	else if(global.stamina >= 30)
	{
		draw_sprite_ext(spr_backstamina,1,itemX+298, itemY-63,
		1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_frontstaminaY,1,itemX+298, itemY-63,
		max(0,global.stamina/100),1,0,c_white,1);
	}
	else
	{
		draw_sprite_ext(spr_backstaminaR,1,itemX+298, itemY-63,
		 1, 1, 0, c_white, 1);
		draw_sprite_ext(spr_frontstaminaR,1,itemX+298, itemY-63,
		max(0,global.stamina/100),1,0,c_white,1);
	}
	
	//Torchlight HUD
	if(global.isTorchlightPickedUp == true)
	{
		draw_sprite_ext(spr_torchBatteryBarOutline, 1, itemX+253, itemY-122, 1, 1, 0, c_white, 1);
		if(global.battery >= 80)
		{
			draw_sprite_ext(spr_backbattery,1,itemX+298, itemY-118,
			 1, 1, 0, c_white, 1);
			draw_sprite_ext(spr_frontbattery,1,itemX+298, itemY-118,
			max(0,global.battery/100),1,0,c_white,1);
		}
		else if(global.battery >= 30)
		{
			draw_sprite_ext(spr_backbattery,1,itemX+298, itemY-118,
			1, 1, 0, c_white, 1);
			draw_sprite_ext(spr_frontbatteryY,1,itemX+298, itemY-118,
			max(0,global.battery/100),1,0,c_white,1);
		}
		else
		{
			draw_sprite_ext(spr_backbatteryR,1,itemX+298, itemY-118,
			 1, 1, 0, c_white, 1);
			draw_sprite_ext(spr_frontbatteryR,1,itemX+298, itemY-118,
			max(0,global.battery/100),1,0,c_white,1);
		}
	}
	
	
	//Key fragment
	if(global.isKeyFragment1PickedUp == true){
		draw_sprite_ext(spr_keyFragment1, 0, itemX-650, itemY-90, 1.5,1.5,0, c_white, 1)
	}
	
	if(global.isKeyFragment2PickedUp == true){
		draw_sprite_ext(spr_keyFragment2, 0, itemX-632, itemY-90, 1.5,1.5,0, c_white, 1)
	}
	
	if(global.isKeyFragment3PickedUp == true){
		draw_sprite_ext(spr_keyFragment3, 0, itemX-619, itemY-52, 1.5,1.5,0, c_white, 1)
	}
}