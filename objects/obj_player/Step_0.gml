/// @desc Every Step

if(global.torchlightValue == 1){
	instance_activate_layer("Inst_ShowTextOnLight");
}
else{
	instance_deactivate_layer("Inst_ShowTextOnLight");
}


if(global.PlayerDie == false && Book == noone && global.isCupboardOpen == false && global.isEscKeyPressed == false){
	move_get_input();
	script_execute(state);

	//Pickup Item
	x1 = x;
	y1 = y;
	x2 = x + sprite_width;
	y2 = y + sprite_height;

	if(collision_rectangle(x1, y1, x2, y2, obj_player_item, false, false)){	
		if(TextboxPlayerItem == noone){
			global.textbox = "playeritem";
			TextboxPlayerItem = instance_create_layer(x, y, "Inst_Text", obj_textbox)
		}
	
		if(keyboard_check(ord("E"))){
			itemInstanceID = collision_rectangle(x1, y1, x2, y2, obj_player_item, false, false);
			item = itemInstanceID.image_index;	
			pickup(item, itemInstanceID)
			if(audio_is_playing(snd_lootItem) == false)
			{ 
				audio_play_sound(snd_lootItem, 10, false);
			}
		}
	}
	else{
		if(TextboxPlayerItem != noone){
			instance_destroy(TextboxPlayerItem);
			TextboxPlayerItem = noone;
		}
	}
	
	//Walking Sound Effect
	if((keyboard_check(ord("W")) || keyboard_check(ord("A")) || keyboard_check(ord("S")) || keyboard_check(ord("D")))){
		charMovement = irandom_range(1,3);
		if(audio_is_playing(snd_charfootstep1) == false && audio_is_playing(snd_charfootstep2) == false
		&& audio_is_playing(snd_charfootstep3) == false && global.moveState == true){
			if(charMovement = 1){
				footstepIndex = audio_play_sound(snd_charfootstep1, 10, false);
			}
			if(charMovement = 2){
				footstepIndex = audio_play_sound(snd_charfootstep2, 10, false);
			}
			if(charMovement = 3){
				footstepIndex = audio_play_sound(snd_charfootstep3, 10, false);
			}
			audio_sound_pitch(footstepIndex, 0.75);
		}
	}

	if(audio_is_playing(snd_InGameBGM) == false) {
		audio_play_sound(snd_InGameBGM, 10, true);
	}	

	//Sprint and Stamina
	if(Book == noone){
		if(keyboard_check(vk_lshift) && global.stamina > 0 && ((keyboard_check(ord("W")) || keyboard_check(ord("A")) || keyboard_check(ord("S")) || keyboard_check(ord("D")))) && global.moveState == true){
			global.stamina -= 0.35;
			spd = sprint_spd;
			image_speed = 0.36;
			audio_sound_pitch(footstepIndex, 1.15);	
		}
		else{
			spd = walk_spd;
		}
	}
	else{
		 spd = 0;
	}

	if!((keyboard_check(vk_lshift)) && (keyboard_check(ord("W")) || keyboard_check(ord("A")) || keyboard_check(ord("S")) || keyboard_check(ord("D")))){
		global.stamina += 0.25;
	}

	if(global.stamina > 100){
		global.stamina = 100;
	}
	else if (global.stamina < 0){
		global.stamina = 0;	
	}
	
	/// torch battery
	if(global.torchlightValue == 1)
	{
		global.battery -= 0.02;	
	}
	
	if(global.battery <= 0)
	{
		global.battery = 0;
		global.torchlightValue = 0;
	}

	//EMF Beeping Sound
	if(global.emfReaderValue == 0){
		if(audio_is_playing(snd_emfBeep)){
			audio_stop_sound(snd_emfBeep);
		}
	}
	else if(global.emfReaderValue == 1){
		if(place_meeting(obj_player.x, obj_player.y, obj_emfRadius) && audio_is_playing(snd_emfBeep) == false){
			audio_play_sound(snd_emfBeep, 10, true);
		}
		else if(!place_meeting(obj_player.x, obj_player.y, obj_emfRadius) && audio_is_playing(snd_emfBeep) == true){
			audio_stop_sound(snd_emfBeep);
		}
	}

	//Ghost encounter dialogue
	if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_ghostEncounterDialogue, false, false)){
		audio_play_sound(snd_ghostHowl, 10, false);
		global.moveState = false;
		sprite_index = spr_player_left;
	}

	//Found door dialogue
	if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_foundDoor, false, false)){
		global.moveState = false;
		sprite_index = spr_player_up;
	}
	
	//Anna spawned dialogue
	if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_annaSpawnedDialogue, false, false) && global.isAnnaSpawned == false){
		global.gameState = 9;
		global.isAnnaSpawned = true;
		global.moveState = false;
		audio_play_sound(snd_annaspawned, 10, false);
		instance_activate_object(obj_dialoguebox);
		
	}
	

	//Play basketball bounce audio
	if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, inst_triggerBasketballBounce, false, false)){
		audio_play_sound(snd_basketballBounce, 20, false);
		audio_play_sound(snd_ghostHowl, 10, false);
		instance_destroy(inst_triggerBasketballBounce);
	}

	//Attempt Unlock Door
	if(place_meeting(obj_player.x, obj_player.y, obj_unlockDoor)){
		if(TextboxDoor == noone){
			global.textbox = "door";
			TextboxDoor = instance_create_layer(x, y, "Inst_Text", obj_textbox)
		}
		
		if(keyboard_check_pressed(ord("E"))){
			if(global.isKeyFragment1PickedUp == true && global.isKeyFragment2PickedUp == true && global.isKeyFragment3PickedUp == true){
				//Unlock Door
				room_goto(rm_tbc1);
			}
			else
			{
				if(!audio_is_playing(snd_doorLock)){
					audio_play_sound(snd_doorLock, 20, false);
				}
				global.gameState = 5;
				instance_activate_object(obj_dialoguebox);
			}
		}
	}
	else
	{
		if(TextboxDoor != noone)
		{
			instance_destroy(TextboxDoor);
			TextboxDoor = noone;
		}
	}

	if(place_meeting(obj_player.x, obj_player.y, obj_searchDustbin)){
		if(TextboxSearchDustbin == noone){
			global.textbox = "searchdustbin";
			TextboxSearchDustbin = instance_create_layer(x, y, "Inst_Text", obj_textbox)
		}
	
		if(keyboard_check_pressed(ord("E")) && timingId == noone){
			timingId = instance_create_layer(obj_player.x, obj_player.y, "Inst_Timing", obj_searchDustbinTiming)
			if(TextboxSearchDustbin != noone){
				instance_destroy(TextboxSearchDustbin);
			}	
		}
	}
	else{
		if(TextboxSearchDustbin != noone){
			instance_destroy(TextboxSearchDustbin);
			TextboxSearchDustbin = noone;
		}
	
		instance_destroy(timingId)
		timingId = noone;
	}

	//Found all key fragments
	if(global.isKeyFragment1PickedUp == true && global.isKeyFragment2PickedUp == true && global.isKeyFragment3PickedUp == true && global.foundAllKeysDialogueTriggered == false)
	{
			global.gameState = 7;
			instance_activate_object(obj_dialoguebox);
			global.moveState = false;
			global.foundAllKeysDialogueTriggered = true;
	}
	
	//Spawn Blake 2
	if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, inst_emfblake2, false, false) && global.isBlake2Spawned == false){
		instance_create_layer(2890, 100, "Inst_Ghost", obj_blake)
		global.isBlake2Spawned = true;
	}	
}

//Open Cupboard
if(place_meeting(obj_player.x, obj_player.y, obj_cupboardCollision)){
	//Display controls to open cupboard
	if(TextboxCupboard == noone){
		global.textbox = "cupboard";
		TextboxCupboard = instance_create_layer(x, y, "Inst_Text", obj_textbox)
	}
	if(keyboard_check_pressed(ord("E"))){
		set_lock_number();
		if((place_meeting(obj_player.x, obj_player.y, cupboard1_instance) && global.isCupboard1Unlock == false) || 
		(place_meeting(obj_player.x, obj_player.y, cupboard2_instance) && global.isCupboard2Unlock == false))
		{
			//Open cupboardLock if cupboard is not unlocked
			if(global.isCupboardOpen == false){
				CupboardLockId = instance_create_layer(x, y+60, "Inst_ItemOverlay", obj_cupboardLock);
				global.CupboardId = instance_create_layer(x, y, "Inst_ItemOverlay", obj_cupboard);
			}
			else{
				instance_destroy(global.CupboardId);
				instance_destroy(CupboardLockId);
				instance_destroy(global.CupboardOpenId);			
			}
		}
		else
		{
			//Open cupboard if cupboard is unlocked
			if(global.isCupboardOpen == false){
				global.CupboardOpenId = instance_create_layer(x, y, "Inst_ItemOverlay", obj_cupboardOpen);
			}
			else{
				instance_destroy(global.CupboardOpenId);
			}			
		}
		
	}
}
else
{
	if(TextboxCupboard != noone)
	{
		instance_destroy(TextboxCupboard);
		TextboxCupboard = noone;
	}
}

