/// @desc Initizalize

//Use this to change walk and sprint speed
global.walk_spd = 1.5;

sprint_spd = 2.5;

//player cannot move until moveState change to true
global.moveState = false;

//Set game state to trigger dialogues when certain point of the game is reached
global.gameState = 0;

global.spd = walk_spd;
hspd = 0;
vspd = 0;
len = 0;
dir = 0;
global.stamina = 100;
global.battery = 100;

state = move_state;

// Sprite Variables
face = 0;

#macro RIGHT spr_player_right;
#macro LEFT spr_player_left;
#macro DOWN spr_player_down;
#macro UP spr_player_up;

// Items
maxInvSlots = 6
for (var inv = 0; inv < maxInvSlots; inv ++){
	global.array_inv[inv] = -1;
}

TextboxPlayerItem = noone;
TextboxCupboard = noone;
TextboxSearchDustbin = noone;
TextboxDoor = noone;
inv_selected = 0;
timingId = noone;
global.isEscKeyPressed = false;
global.Book = noone;
global.CodePaper = noone;
global.CodeLibrary = noone;
global.torchlightValue = 0;
global.emfReaderValue = 0;
global.pickUp1TextDisplayed = false;
global.keyFragmentCounter = 0;
global.isKeyFragment1PickedUp = false;
global.isKeyFragment2PickedUp = false;
global.isKeyFragment3PickedUp = false;
global.isTorchlightPickedUp = false;
global.isBookPickedUp = false;
global.isEMFReaderPickedUp = false;
global.isHolyNecklacePickedUp = false;
global.HolyNecklaceInvNum = noone;
global.isCupboardOpen = false;
global.isCupboard1Unlock = false;
global.isCupboard2Unlock = false;
global.CupboardOpenId = noone;
global.textbox = noone;
global.isCameraPanning = false;
global.isBasketballCourtTriggered = false;
global.isPuzzleTriggered = false;
global.puzzleId = noone;
global.sequencePuzzleId = noone;
global.PlayerDie = false;
global.isPuzzleOpened = false;
global.isCodePaperPickup = false;
global.itemNotificationNum = noone;
global.foundAllKeysDialogueTriggered = false;
global.isBlake2Spawned = false;
global.isAnnaSpawned = false;
global.inSickbay = false;
//Lock Number
global.lockNumPos1 = noone;
global.lockNumNeg1 = noone;
global.lockNumPos2 = noone;
global.lockNumNeg2 = noone;

//check anna spawned
global.annaSpawned = false;
if(global.annaSpawned == true)
{
	instance_deactivate_object(obj_annaspawner);
}

global.Torchlight = instance_create_layer(x, y, "Inst_Visibility", obj_visibility);