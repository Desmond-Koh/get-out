{
    "id": "238e2ed0-86f6-4565-b989-604422be9162",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "72fccf73-e243-48db-8bce-c76e0da9f7c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "5caa3b88-c14d-4965-8f6d-a3a45ceddb62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "6e362b43-efbb-4a23-a546-51a00fd6caa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "7e83e165-e72a-4b6c-92d8-d57b10658d75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 49,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "81fd2cb6-e62c-41fd-b05c-ab64b2ade2e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "dd07d073-158d-42ed-acc3-d04eab073417",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 51,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "666676f7-7ddd-4def-9ee9-15bd5b8bf0b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 52,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "fbe7d9ac-65b7-40a1-84eb-8fd28f7513d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "9e937d5d-14d7-43c6-ad34-3c4de9b6a0e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "f0ab9fdb-e648-4a86-9822-833943341a9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        },
        {
            "id": "0922351b-c22e-4876-aba9-3b44f50e87b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 9,
            "m_owner": "238e2ed0-86f6-4565-b989-604422be9162"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "0216bf70-3030-4928-af31-641da682de72",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "6ac8244a-6854-4f75-93df-e29863560049",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "002c8a87-b362-4aad-b958-4c4ee6f36492",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "c71c32d1-6224-458f-acec-21687fd4b50c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "757bbcf4-063e-4eb2-8b71-a340c0838ddb",
    "visible": true
}