{
    "id": "a9c95228-a83a-4b77-ae67-a80f89595484",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_keyFragment2",
    "eventList": [
        {
            "id": "503f5998-757a-4666-8c62-ae381a4dedcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9c95228-a83a-4b77-ae67-a80f89595484"
        },
        {
            "id": "3de5ef91-d044-4962-817c-168215ee3261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9c95228-a83a-4b77-ae67-a80f89595484"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf619180-e5aa-43c1-bf62-86beef386709",
    "visible": true
}