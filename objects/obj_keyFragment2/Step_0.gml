if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_keyFragment2, false, false)){	
	if(TextboxPlayerItem == noone){
		global.textbox = "playeritem";
		TextboxPlayerItem = instance_create_layer(x, y, "Inst_Text", obj_textbox)
	}
	
	if(keyboard_check(ord("E"))){
		global.itemNotificationNum = "foundKey";		
		instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_itemNotification)
		
		global.isKeyFragment2PickedUp = true;
		if(audio_is_playing(snd_keyPickup) == false)
		{ 
			audio_play_sound(snd_keyPickup, 10, false);
		}
		instance_destroy(inst_keyFragment2);
		instance_destroy(TextboxPlayerItem);
		TextboxPlayerItem = noone;
	}
}
else{
	if(TextboxPlayerItem != noone){
		instance_destroy(TextboxPlayerItem);
		TextboxPlayerItem = noone;
	}
}
	
