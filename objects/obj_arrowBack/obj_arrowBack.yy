{
    "id": "f0357a23-52a5-4a07-be6a-c200f3101c00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_arrowBack",
    "eventList": [
        {
            "id": "f76f7132-3258-4ae2-8086-6df3096ff2a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "f0357a23-52a5-4a07-be6a-c200f3101c00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9e1d1e7f-e4ff-4709-9d69-32218fbd6356",
    "visible": true
}