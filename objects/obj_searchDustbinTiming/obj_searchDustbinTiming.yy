{
    "id": "a05ecea8-143d-42fb-9ed9-2907febdc6c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_searchDustbinTiming",
    "eventList": [
        {
            "id": "acf0c424-4bbd-404d-b7c2-2d8e8211dd15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a05ecea8-143d-42fb-9ed9-2907febdc6c3"
        },
        {
            "id": "fa578a43-1a60-45b4-8a11-f1bafad4e5d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a05ecea8-143d-42fb-9ed9-2907febdc6c3"
        },
        {
            "id": "12337cc8-64c9-481e-a803-099145c8b2d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a05ecea8-143d-42fb-9ed9-2907febdc6c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}