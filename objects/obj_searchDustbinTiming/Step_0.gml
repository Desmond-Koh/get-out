if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, obj_searchDustbin, false, false)){
	if(keyboard_check(ord("E"))){	
		if(searchDustbin < 1){
			global.moveState = false;
			searchDustbin += 0.008;	
		}
		else{
			searchDustbin = 1;
			global.moveState = true;
			if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, inst_code, false, false) && global.isCodePaperPickup == false){	
				pickup(4, noone)
				if(audio_is_playing(snd_lootItem) == false)
				{ 
					audio_play_sound(snd_lootItem, 10, false);
				}	
				global.isCodePaperPickup = true;
				global.itemNotificationNum = "foundCode";
				instance_destroy(inst_code);
				instance_activate_object(inst_12345);
			}
			else if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, inst_battery, false, false)){	
				if(global.battery < 50){
					global.battery += 50
				}
				else{
					global.battery = 100;
				}
				
				if(audio_is_playing(snd_lootItem) == false)
				{ 
					audio_play_sound(snd_lootItem, 10, false);
				}	
				
				global.itemNotificationNum = "foundBattery";
				instance_destroy(inst_battery);
			}
			else
			{
				global.itemNotificationNum = "foundNothing";
			}
			
			instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_itemNotification)
			instance_destroy();
		}
	}
	else{
		global.moveState = true;
	}
}