{
    "id": "72c9784c-c67c-4bd2-afca-6616bf1e03ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_puzzleTile",
    "eventList": [
        {
            "id": "c0715b20-2df9-42d4-97ae-f9ee60e2ff79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "72c9784c-c67c-4bd2-afca-6616bf1e03ea"
        },
        {
            "id": "4d99c4ab-1b63-44d6-8a57-190a7a973e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "72c9784c-c67c-4bd2-afca-6616bf1e03ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "708ea4fe-2b5d-40f4-ab80-35b6b5456390",
    "visible": true
}