startX = (view_wport[0] / 2) - (sprite_get_width(spr_player_item) * 2.5) + 160;
startY = view_hport[0] - (sprite_get_height(spr_player_item) / 2) - 350;

alpha = 0.8;
text = "";

if(global.itemNotificationNum == "foundCode"){
	text = "You found a paper!"
}
else if(global.itemNotificationNum == "foundNothing"){
	text = "You found nothing!"
}
else if(global.itemNotificationNum == "foundBattery"){
	text = "You found a battery!"
}
else if(global.itemNotificationNum == "foundHolynecklace"){
	text = "You found a holy necklace!"
}
else if(global.itemNotificationNum == "foundTorchlight"){
	text = "You found a torchlight!"
}
else if(global.itemNotificationNum == "foundBook"){
	text = "You found a book!"
}
else if(global.itemNotificationNum == "foundEMFReader"){
	text = "You found an EMF reader!"
}
else if(global.itemNotificationNum == "foundKey"){
	if(global.keyFragmentCounter != 2){
		global.keyFragmentCounter++
		text = "You found a key fragment!"
	}
}

if(alpha < 0)
{
	instance_destroy(obj_itemNotification)
}
