{
    "id": "ddf2fd68-15e1-430c-b0cd-376e51c3ff8d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_triggerBasketballCourt",
    "eventList": [
        {
            "id": "1220dfbb-d8cb-430c-a0ee-0cfb6b12e65c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddf2fd68-15e1-430c-b0cd-376e51c3ff8d"
        },
        {
            "id": "bd59e49d-0471-413c-aab2-654bb3fdfa02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddf2fd68-15e1-430c-b0cd-376e51c3ff8d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6cb23b11-abc3-4b45-bc0d-b0ed49efd27e",
    "visible": false
}