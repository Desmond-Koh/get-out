//Trigger Basketball Court
if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, obj_triggerBasketballCourt, false, false)){
	if(global.isBasketballCourtTriggered == false){
		audio_play_sound(snd_heavyLightBreak, 10, false);
		global.gameState = 6;
		global.moveState = false;
		instance_activate_object(obj_dialoguebox);
		
		global.sequencePuzzleId = instance_create_layer(x, y, "Inst_VisibilityPuzzle", obj_startPuzzleSequence);
		global.isBasketballCourtTriggered = true;
	}
	else{
		if(instance_activate_layer(global.sequencePuzzleId) == false){
				instance_activate_object(global.sequencePuzzleId )
		}
	}
}