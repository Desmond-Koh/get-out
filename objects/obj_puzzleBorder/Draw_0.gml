
// puzzle win condition - answers: 15,11,10,6,5,1,2,5,7,3,4,8
var winningCondition = [2,	5,	4, 8,
						1,	7,	3,	noone,
						9,	6,	10,	12,
						13,	14,	11,	15];

var value = 0;
var complete = true;

for(var j=0; j<4; ++j)
{
	for(var i=0; i<4; ++i)
	{
		var ins = collision_point(x + (i * sprite_get_width(spr_puzzletile)),
											 y + (j * sprite_get_height(spr_puzzletile)),
											 obj_puzzleTile, false, false);
		var tile_value = noone;
		
	    if(ins != noone)
		{
			tile_value = ins.value;	
		}
		
		if(tile_value != winningCondition[value])
		{
			complete = false;
		}
		++value;
	}	
}

draw_self();


if(complete == true)
{
	global.isKeyFragment3PickedUp = true;
	global.itemNotificationNum = "foundKey";		
	instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_itemNotification)
	if(audio_is_playing(snd_keyPickup) == false)
	{
		audio_play_sound(snd_keyPickup, 10, false);
	}
	global.moveState = true;
	instance_destroy(obj_puzzleBorder);
	instance_destroy(obj_puzzleTile);
	instance_destroy();
	
}