{
    "id": "595c8c54-452c-45b3-92f8-7c8a26df52c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_puzzleBorder",
    "eventList": [
        {
            "id": "7557e732-2ea7-4606-8ef2-abcfea9c6e3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "595c8c54-452c-45b3-92f8-7c8a26df52c1"
        },
        {
            "id": "0f48d05e-8da9-409b-b3df-de787cb5230b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "595c8c54-452c-45b3-92f8-7c8a26df52c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e16262aa-e648-4e81-9bc6-04a1433ee64a",
    "visible": true
}