{
    "id": "c736780c-a6bb-4e62-b4c3-9da61dea33bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fixLightTiming",
    "eventList": [
        {
            "id": "ff456a8d-6d8d-47ee-b47c-c0cd41b92c43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c736780c-a6bb-4e62-b4c3-9da61dea33bd"
        },
        {
            "id": "98c26d7d-ec53-41b9-bbd7-b0e343bc5083",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c736780c-a6bb-4e62-b4c3-9da61dea33bd"
        },
        {
            "id": "08ad1883-2bf8-4061-93f2-b93b8d0ef095",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c736780c-a6bb-4e62-b4c3-9da61dea33bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}