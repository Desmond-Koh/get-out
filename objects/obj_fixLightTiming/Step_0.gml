if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, obj_fixLight, false, false)){
	if(keyboard_check(ord("E"))){
		if(fixLight < 1){
			global.moveState = false;
			fixLight += 0.002;
		}
		else{
			fixLight = 1;
			global.moveState = true;
			instance_activate_layer("Inst_KeyFragment2");
			audio_play_sound(snd_heavyLight, 10, false);
			instance_destroy(obj_startPuzzleSequence);
			instance_destroy(inst_fixLight);
			instance_destroy();
		}
	}
	else{
		global.moveState = true;
	}
}