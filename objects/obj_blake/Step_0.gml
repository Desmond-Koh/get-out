/// @desc

globalvar torchlightValue;
globalvar walk_spd;

playerX = obj_player.x;
playerY = obj_player.y;

//Get Direction
switch(direction div 90){
	case 0:
		sprite_index = spr_blake_up;
		playerY -= 2000;
		break;
	case 1:
		sprite_index = spr_blake_left;
		playerX -= 2000;
		break;
	case 2:
		sprite_index = spr_blake_down;
		playerY += 2000;
		break;
	case 3:
		sprite_index = spr_blake_right;
		playerX += 2000;
		break;
}

if(distance_to_object(obj_player) < 200){
	if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, inst_emfblake2, false, false)){
		instance_destroy(inst_emfblake2);
	}
	else
	{
		instance_destroy(inst_emfblake);
	}

	
	path_end();
	if(audio_is_playing(snd_heartBeat) == false) 
	{
		heartbeatId = audio_play_sound(snd_heartBeat, 10, false);
	}	
	
	if (torchlightValue == 0)
	{
		image_speed = 0.2;
		keyPressed = 0;
		move_towards_point(obj_player.x, obj_player.y, walk_spd - 0.15);
		if(!alarm[0]){
			alarm[1] = -1;
			alarm[0] = 240;
		}		
	}
	else if (torchlightValue == 1 && !alarm[1])
	{	
		image_speed = 0.5;
		move_towards_point(playerX, playerY, walk_spd + 0.3);
		alarm[0] = -1;
		alarm[1] = 420;	
	}
}
else
{
	image_speed = 0.2;
	move_towards_point(obj_player.x, obj_player.y, 0.1);	
	if(audio_is_playing(snd_heartBeat) == true) 
	{
		audio_stop_sound(heartbeatId);
	}	
	alarm[0] = -1;
	alarm[1] = -1;
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, obj_ghostEncounterDialogue, false, false)){
	alarm[2] = 400;
}