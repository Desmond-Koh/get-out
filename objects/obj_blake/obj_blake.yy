{
    "id": "f943bfe6-d272-4896-a783-27ade68407f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blake",
    "eventList": [
        {
            "id": "ce02c847-88fb-4827-97db-882fa94f8fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        },
        {
            "id": "46a5091c-e22d-4258-938e-4e1b1bb4aa3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        },
        {
            "id": "461e503f-7f29-49a4-b607-6e461cc01f81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        },
        {
            "id": "ddc84a2b-0d37-4f6f-846d-8c2a062a82db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        },
        {
            "id": "f891a391-8958-4d8b-ae43-0ba489090f1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        },
        {
            "id": "46b9f9f0-cbfc-41e4-829f-b918a9c2c626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f943bfe6-d272-4896-a783-27ade68407f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "f3c2cdd7-8210-457f-9634-a25a7456d05a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "6a973e85-d871-414a-9704-04f2831f76a6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "ed2629aa-8a8c-4bf3-9140-19d18b54d2d2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "ff933b1d-1368-4f5e-b65b-7fe7a8e6834f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff92f3bc-ea09-4dee-bd27-41ee800af3e4",
    "visible": true
}