/// @desc Player Dies

instance_destroy();
audio_stop_sound(heartbeatId);
audio_stop_sound(snd_emfBeep);
player_die();