{
    "id": "75df3643-3848-4349-b071-3b2e85c88eb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cupboard",
    "eventList": [
        {
            "id": "476ee57e-bf34-4ebd-8da4-9b17a394fe99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75df3643-3848-4349-b071-3b2e85c88eb7"
        },
        {
            "id": "8ea36e2d-870b-4909-9734-de17d373d97a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75df3643-3848-4349-b071-3b2e85c88eb7"
        },
        {
            "id": "6b4f3095-d9c2-47dc-9671-4cef99415bd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "75df3643-3848-4349-b071-3b2e85c88eb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd0cb952-c54f-4fbe-bcb2-bf8212d9f10f",
    "visible": true
}