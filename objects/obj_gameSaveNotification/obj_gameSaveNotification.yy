{
    "id": "3fd359d3-9cbc-45fb-a0b7-63213c14814c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameSaveNotification",
    "eventList": [
        {
            "id": "03109e53-1e35-47ab-b55f-cc434eeb64b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3fd359d3-9cbc-45fb-a0b7-63213c14814c"
        },
        {
            "id": "ce40c9e9-056e-41b2-ae80-54adad6065b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3fd359d3-9cbc-45fb-a0b7-63213c14814c"
        },
        {
            "id": "ba924318-4170-4906-8c90-8d99f3b350fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3fd359d3-9cbc-45fb-a0b7-63213c14814c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}