if(collision_rectangle(obj_player.x, obj_player.y, obj_player.x, obj_player.y, obj_puzzleText, false, false)){	
	if(puzzleTextId == noone && global.isKeyFragment3PickedUp == false){
		global.textbox = "puzzle";
		puzzleTextId = instance_create_layer(x, y, "Inst_Text", obj_textbox)
	}
	
	if(keyboard_check_pressed(ord("E"))){
	
		if(global.isPuzzleOpened == false){
			if(global.isPuzzleTriggered == false){	
				global.puzzleId = instance_create_layer(x-88, y-88, "Inst_ItemOverlay", obj_puzzleBorder);
				global.isPuzzleOpened = true;
				global.moveState = false;
				global.isPuzzleTriggered = true
				if(puzzleTextId  != noone){
					instance_destroy(puzzleTextId );
					puzzleTextId  = noone;
				}
			}
			else
			{
				global.isPuzzleOpened = true;
				global.moveState = false;
				if(puzzleTextId  != noone){
					instance_destroy(puzzleTextId );
					puzzleTextId  = noone;
				}
				if(instance_activate_object(global.puzzleId) == false){
					instance_activate_object(global.puzzleId)
					instance_activate_object(obj_puzzleTile)
				}
			}
		}
		else{
			if(puzzleTextId == noone && global.isKeyFragment3PickedUp == false){
				global.textbox = "puzzle";
				puzzleTextId = instance_create_layer(x, y, "Inst_Text", obj_textbox)
			}
			instance_deactivate_object(obj_puzzleBorder);
			instance_deactivate_object(obj_puzzleTile);
			global.isPuzzleOpened = false
			global.moveState = true;
		}
				
	}

}
else{
	if(puzzleTextId != noone){
		instance_destroy(puzzleTextId );
		puzzleTextId  = noone;
	}
}