{
    "id": "04261717-7440-494f-8b67-01a6b6dc9cd9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_puzzleText",
    "eventList": [
        {
            "id": "6dd99bdd-e15e-494d-9f68-89e887b61c71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "04261717-7440-494f-8b67-01a6b6dc9cd9"
        },
        {
            "id": "5e38885a-8353-426b-ad0e-a5ba29a84888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "04261717-7440-494f-8b67-01a6b6dc9cd9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "17e1a879-bf02-461e-96d9-fa6c309efef5",
    "visible": false
}