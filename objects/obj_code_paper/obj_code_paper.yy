{
    "id": "764c7ad2-f37e-4f7a-a456-b360facaa82b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_code_paper",
    "eventList": [
        {
            "id": "03631e1c-a7d5-49f0-b239-2d6326b7f052",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "764c7ad2-f37e-4f7a-a456-b360facaa82b"
        },
        {
            "id": "017c244c-6538-4996-92e2-349f79833ce1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "764c7ad2-f37e-4f7a-a456-b360facaa82b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b4f1b080-4d72-49d8-bdc2-de35f92ba261",
    "visible": true
}