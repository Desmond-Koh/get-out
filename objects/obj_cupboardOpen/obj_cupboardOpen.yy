{
    "id": "d12884af-97c6-4b84-9858-37eb61f3c7e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cupboardOpen",
    "eventList": [
        {
            "id": "f74ac652-b9ba-4060-88e9-b4fe31031bf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d12884af-97c6-4b84-9858-37eb61f3c7e0"
        },
        {
            "id": "2a71ed7c-19d3-451e-a07a-5e40319c8f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d12884af-97c6-4b84-9858-37eb61f3c7e0"
        },
        {
            "id": "5bdad160-5154-4d62-9176-a54d453cf976",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d12884af-97c6-4b84-9858-37eb61f3c7e0"
        },
        {
            "id": "c945fb91-a6bb-41f1-8221-c44c630e2a09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d12884af-97c6-4b84-9858-37eb61f3c7e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1dc28cc7-a297-4e13-965f-d75e045eb00a",
    "visible": true
}