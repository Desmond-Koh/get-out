/// @desc
if(alpha < 1){
	alpha += 0.01;
}

if(yAxis > y-200){
	yAxis -= 0.1;
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard1_instance, false, false)){
	if(collision_rectangle(mouse_x, mouse_y, mouse_x, mouse_y, bookItemId, false, false) && mouse_check_button_released(mb_left)){
		itemInstanceID = collision_rectangle(mouse_x, mouse_y, mouse_x, mouse_y, bookItemId, false, false);
		item = 0
		pickup(item, itemInstanceID)
		if(audio_is_playing(snd_lootItem) == false)
		{ 
			audio_play_sound(snd_lootItem, 10, false);
		}
	}
}

if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard2_instance, false, false)){
	if(collision_rectangle(mouse_x, mouse_y, mouse_x, mouse_y, emfReaderId, false, false) && mouse_check_button_released(mb_left)){
		itemInstanceID = collision_rectangle(mouse_x, mouse_y, mouse_x, mouse_y, obj_player_item_cupboard, false, false);
		item = 2;	
		pickup(item, itemInstanceID)
		if(audio_is_playing(snd_lootItem) == false)
		{ 
			audio_play_sound(snd_lootItem, 10, false);
		}
	}
}







