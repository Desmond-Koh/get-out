/// @desc

alpha = 0.1;
yAxis = y-180;
bookItemId = noone;
emfReaderId = noone;


global.isCupboardOpen = true;
global.moveState = false;
audio_play_sound(snd_cupboard, 10, false);

//Set cupboard unlocked to true
if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard1_instance, false, false)){
	global.isCupboard1Unlock = true;
	
	//Add Book
	if(global.isBookPickedUp == false){
		bookItemId = instance_create_layer(obj_cupboardOpen.x-125, obj_cupboardOpen.y+60, "Inst_PlayerItemCupboard", obj_player_item_cupboard);
	}
	
	if(global.isKeyFragment1PickedUp == false){
		global.itemNotificationNum = "foundKey";		
		instance_create_layer(obj_player.x, obj_player.y, "Inst_ItemNotification", obj_itemNotification)
		global.isKeyFragment1PickedUp = true;
		audio_play_sound(snd_keyPickup, 20, false);
		alarm[0] = 200;
	}
}
else if(collision_rectangle(obj_player.x-20, obj_player.y-20, obj_player.x+20, obj_player.y+20, cupboard2_instance, false, false)){
	global.isCupboard2Unlock = true;
	
	//EMF Reader
	if(global.isEMFReaderPickedUp == false){
		emfReaderId = instance_create_layer(obj_cupboardOpen.x+125, obj_cupboardOpen.y-100, "Inst_PlayerItemCupboard", obj_player_item_cupboard);
	}
}