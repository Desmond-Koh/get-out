{
    "id": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credit",
    "eventList": [
        {
            "id": "8922fe6f-dd55-4159-b710-6ee5208ea2c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 61,
            "eventtype": 6,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "bd281cf0-7dc6-4d23-9dbc-33f8ec7d2d42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "3287b954-a7d5-4023-a78a-615818b978f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "2b95ed32-a645-4b8f-b266-b024be584280",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "54e6fa83-be94-4136-a819-c3c1933d995d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 9,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "7f833bb5-dae9-45c2-aac7-63b8f6173af8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 9,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        },
        {
            "id": "08970f2d-c742-4756-a5a3-c5780207561a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "84ec16a3-69bc-4b79-94ac-9aa35bb94ee4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}