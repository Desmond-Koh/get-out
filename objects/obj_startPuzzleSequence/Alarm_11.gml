/// @desc Turn off light

instance_destroy(obj_startPuzzleSequenceMiddle);

audio_play_sound(snd_heavyLightOff, 10, false);
lightSpawn0 = false;
lightSpawn1 = false;
lightSpawn2 = false;
lightSpawn3 = false;
lightSpawn4 = false;
lightSpawn5 = false;
lightSpawn6 = false;
lightSpawn7 = false;
lightSpawn8 = false;
lightSpawn9 = false;
lightSpawn10 = false;

