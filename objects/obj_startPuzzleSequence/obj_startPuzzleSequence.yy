{
    "id": "c0cd3741-fc64-477e-8523-559dd5d4f961",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startPuzzleSequence",
    "eventList": [
        {
            "id": "c8a5e9a5-75bc-465b-acba-7193fb9a84f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "54464ac0-ab8d-4e97-a3d9-3c2afa5ffd22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "2f744a44-af69-47c4-a7ae-6a9f039f95c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "f03214e2-7f15-4951-a961-83c2263b872d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "0e3272b5-a106-443d-ae16-cd469b7df4ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "536b4ed6-3fba-4705-a247-9314ee99509c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "4eb0b8f7-be73-45ea-9d4c-5026aa385971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "18934a42-10f7-4901-baf1-4cc405ffd7bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "d4fa3e0a-7dea-4f09-9a27-e7777fd95863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "36859010-729b-45b5-ab2a-ddd3c20c4934",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "12a00228-db25-4f9f-b3ae-f52fc7adf337",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "24f9c8c2-0888-4ae3-943d-0e89cf35ebfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "6e472a18-384a-4606-9ccb-255ec49c0037",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 9,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "a499f920-1c47-40bd-9a12-ed5bfc75e4c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        },
        {
            "id": "d4dfe381-801c-4f7a-b968-79b589ddd32b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0cd3741-fc64-477e-8523-559dd5d4f961"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}