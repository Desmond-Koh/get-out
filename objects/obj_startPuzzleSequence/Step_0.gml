instance_create_layer(lightX, lightY, "Inst_TriggerBasketballCourt", obj_startPuzzleSequenceMiddle)

if(!collision_rectangle(obj_player.x-50, obj_player.y-50, obj_player.x+50, obj_player.y+50, obj_startPuzzleSequenceMiddle, false, false) &&
!collision_rectangle(obj_fixLight.x-50, obj_fixLight.y-70, obj_fixLight.x+70, obj_fixLight.y+50, obj_player, false, false) && 
global.moveState == true){
	inDarknessTiming += 0.012
	if(audio_is_playing(snd_heartBeatPuzzle1) == false){
		audio_play_sound(snd_heartBeatPuzzle1, 10, false);
	}	
	
	if(inDarknessTiming >= 3){
		
		if(global.PlayerDie == false){
			if(global.isHolyNecklacePickedUp == true){
				inDarknessTiming = 0;
				instance_deactivate_object(global.sequencePuzzleId);
			}
			else{
				audio_stop_sound(heartbeatId);
				instance_destroy(obj_startPuzzleSequence);
				instance_destroy(inst_fixLight);
			}
			player_die();
		}	
	}
}
else{
	inDarknessTiming = 0;
}
