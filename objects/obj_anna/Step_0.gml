
playerX = obj_player.x;
playerY = obj_player.y;

move_towards_point(obj_player.x,obj_player.y,0);

if(distance_to_object(obj_player) == 0){
	if(!audio_is_playing(snd_anna_chased_off))
	{
		audio_play_sound(snd_anna_chased_off, 20, false);
	}
	global.annaSpawned = false;
	instance_destroy(obj_anna);
}