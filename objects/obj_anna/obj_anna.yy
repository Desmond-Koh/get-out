{
    "id": "7356f716-3a9b-40c6-bf64-91c85bac2bcd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anna",
    "eventList": [
        {
            "id": "c1360f7f-df60-40ff-ad39-ca376bbf3e3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7356f716-3a9b-40c6-bf64-91c85bac2bcd"
        },
        {
            "id": "631b5999-a43a-4c6e-921a-80b7c466e9a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7356f716-3a9b-40c6-bf64-91c85bac2bcd"
        },
        {
            "id": "369f8321-47ee-4e91-adaa-a876a39b3bf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7356f716-3a9b-40c6-bf64-91c85bac2bcd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52acf200-c60d-48a6-bebe-83446f3923eb",
    "visible": true
}