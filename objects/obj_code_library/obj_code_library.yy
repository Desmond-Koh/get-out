{
    "id": "7a93c7f1-7c07-4af6-bbf9-693b827774da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_code_library",
    "eventList": [
        {
            "id": "2d3081d1-f6d0-43de-a344-e2abbca085c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a93c7f1-7c07-4af6-bbf9-693b827774da"
        },
        {
            "id": "39ddd33f-0c7a-4a1e-87b1-306782319c5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7a93c7f1-7c07-4af6-bbf9-693b827774da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8cdf501f-08a4-4ffa-8a9c-4cc6c8c1b452",
    "visible": true
}